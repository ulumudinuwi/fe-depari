<?php

defined('BASEPATH') or exit('No direct script access allowed');

// Data Mode
define('DATA_MODE_VIEW', 0);
define('DATA_MODE_ADD', 1);
define('DATA_MODE_EDIT', 2);
define('DATA_MODE_DELETE', 3);

// Jenis Kelamin
define('JENIS_KELAMIN_LAKI_LAKI', 1);
define('JENIS_KELAMIN_PEREMPUAN', 2);

// Jenis Wilayah
define('WILAYAH_PROVINSI', 1);
define('WILAYAH_KABUPATEN', 2);
define('WILAYAH_KOTA', 3);
define('WILAYAH_KECAMATAN', 4);
define('WILAYAH_KELURAHAN', 5);
define('WILAYAH_DESA', 6);

// Jenis Identitas
define('JENIS_INDENTITAS_KTP', 1);
define('JENIS_INDENTITAS_SIM', 2);
define('JENIS_INDENTITAS_PASPOR', 3);
define('JENIS_INDENTITAS_KTM', 4);

// Jenis Telepon
define('JENIS_TELEPON_HP', 1);
define('JENIS_TELEPON_TELEPON_RUMAH', 2);
define('JENIS_TELEPON_TELEPON_KANTOR', 3);

// Golongan Darah
define('GOLONGAN_DARAH_O', 1);
define('GOLONGAN_DARAH_A', 2);
define('GOLONGAN_DARAH_B', 3);
define('GOLONGAN_DARAH_AB', 4);

// Agama
define('AGAMA_ISLAM', 1);
define('AGAMA_HINDU', 2);
define('AGAMA_BUDHA', 3);
define('AGAMA_KATOLIK', 4);
define('AGAMA_PROTESTAN', 5);
define('AGAMA_KONGHUCU', 6);
define('AGAMA_LAIN_LAIN', 7);

// Status Kawin
define('STATUS_KAWIN_KAWIN', 1);
define('STATUS_KAWIN_TIDAK_KAWIN', 2);
define('STATUS_KAWIN_DUDA', 3);
define('STATUS_KAWIN_JANDA', 4);
define('STATUS_KAWIN_DIBAWAH_UMUR', 5);

// Rujukan dari
define('BUKAN_RUJUKAN', 0);
define('RUJUKAN_FKTP', 1);
define('RUJUKAN_FKRTL', 2);

// Cara Pembayaran
define('CARA_BAYAR_UMUM', 1);
define('CARA_BAYAR_BPJS', 2);
define('CARA_BAYAR_JAMKESDA', 3);
define('CARA_BAYAR_ASURANSI', 4);
define('CARA_BAYAR_PERUSAHAAN', 5);
define('CARA_BAYAR_INTERNAL', 6);

// Hubungan Penanggung Jawab dengan Pasien
define('PJ_HUBUNGAN_AYAH', 1);
define('PJ_HUBUNGAN_IBU', 2);
define('PJ_HUBUNGAN_ANAK', 3);
define('PJ_HUBUNGAN_ADIK', 4);
define('PJ_HUBUNGAN_KAKAK', 5);
define('PJ_HUBUNGAN_SUAMI', 6);
define('PJ_HUBUNGAN_ISTRI', 7);
define('PJ_HUBUNGAN_WALI', 8);

// Jenis Komponen Tarif
define('JENIS_KOMPONEN_TARIF_JASA_DOKTER', 1);
define('JENIS_KOMPONEN_TARIF_RUMAH_SAKIT', 2);
define('JENIS_KOMPONEN_TARIF_INSENTIF_KARYAWAN', 3);
define('JENIS_KOMPONEN_TARIF_LAIN_LAIN', 99);

// Jenis Kelas
define('KELAS_VVIP', 1);
define('KELAS_VIP', 2);
define('KELAS_UTAMA', 3);
define('KELAS_I', 4);
define('KELAS_II', 5);
define('KELAS_III', 6);

// Golongan Operasi
define('GOLONGAN_OPERASI_KECIL', 1);
define('GOLONGAN_OPERASI_SEDANG', 2);
define('GOLONGAN_OPERASI_BESAR', 3);
define('GOLONGAN_OPERASI_KHUSUS', 4);

// Jenis Unit
define('UNIT_LAYANAN_RAWAT_JALAN', 1);
define('UNIT_LAYANAN_IGD', 2);
define('UNIT_LAYANAN_RAWAT_INAP', 3);
define('UNIT_LAYANAN_PENUNJANG_MEDIS', 4);
define('UNIT_LAYANAN_AMBULANCE', 5);
define('UNIT_LAYANAN_LAIN_LAIN', 6);

// Jenis ODS/ODC
define('ODS', 1);
define('ODC', 2);

// JENIS PENUNJANG MEDIS
define('PENUNJANG_MEDIS_LABORATORIUM', 1);
define('PENUNJANG_MEDIS_RADIOLOGI', 2);
define('PENUNJANG_MEDIS_OK', 3);

define('JENIS_TINDAKAN_POLIKLINIK_PENDAFTARAN', 1);
define('JENIS_TINDAKAN_POLIKLINIK_KONSUL_DOKTER', 2);
define('JENIS_TINDAKAN_POLIKLINIK_TINDAKAN', 3);
define('JENIS_TINDAKAN_POLIKLINIK_OBAT', 4);
define('JENIS_TINDAKAN_POLIKLINIK_OBAT_RACIKAN', 5);
define('JENIS_TINDAKAN_POLIKLINIK_BHP', 6);
define('JENIS_TINDAKAN_RAWAT_DARURAT_PENDAFTARAN', 7);
define('JENIS_TINDAKAN_RAWAT_DARURAT_TINDAKAN', 8);
define('JENIS_TINDAKAN_RAWAT_DARURAT_OBAT', 9);
define('JENIS_TINDAKAN_RAWAT_DARURAT_OBAT_RACIKAN', 10);
define('JENIS_TINDAKAN_RAWAT_DARURAT_BHP', 11);
define('JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN', 12);
define('JENIS_TINDAKAN_RAWAT_INAP_VISITE_DOKTER', 13);
define('JENIS_TINDAKAN_RAWAT_INAP_OBAT', 14);
define('JENIS_TINDAKAN_RAWAT_INAP_OBAT_RACIKAN', 15);
define('JENIS_TINDAKAN_RAWAT_INAP_BHP', 16);
define('JENIS_TINDAKAN_LABORATORIUM', 17);
define('JENIS_TINDAKAN_LABORATORIUM_BHP', 18);
define('JENIS_TINDAKAN_RADIOLOGI', 19);
define('JENIS_TINDAKAN_RADIOLOGI_BHP', 20);
define('JENIS_TINDAKAN_BEDAH_TINDAKAN', 21);
define('JENIS_TINDAKAN_BEDAH_OBAT', 22);
define('JENIS_TINDAKAN_BEDAH_OBAT_RACIKAN', 23);
define('JENIS_TINDAKAN_BEDAH_BHP', 24);
define('JENIS_TINDAKAN_APOTEK_OBAT', 25);
define('JENIS_TINDAKAN_APOTEK_OBAT_RACIKAN', 26);
define('JENIS_TINDAKAN_APOTEK_BHP', 27);
define('JENIS_TINDAKAN_RAWAT_INAP_SEWA_KAMAR', 28);
define('JENIS_TINDAKAN_CUSTOM', 99);

// JENIS RUANG
define('JENIS_RUANG_RAWAT_INAP', 1);
define('JENIS_RUANG_OPERASI', 2);

// STATUS BED
define('STATUS_BED_KOSONG', 1);
define('STATUS_BED_ISI', 2);

// Kategori Perkiraan
define('KATEGORI_PERKIRAAN_AKTIVA', 1);
define('KATEGORI_PERKIRAAN_UTANG', 2);
define('KATEGORI_PERKIRAAN_MODAL', 3);
define('KATEGORI_PERKIRAAN_PENDAPATAN', 4);
define('KATEGORI_PERKIRAAN_BIAYA', 5);
define('KATEGORI_PERKIRAAN_SEMENTARA', 6);

// Jenis Perkiraan
define('JENIS_PERKIRAAN_AKTIVA', 1);
define('JENIS_PERKIRAAN_AKTIVA_LANCAR', 2);
define('JENIS_PERKIRAAN_KAS_KECIL', 3);
define('JENIS_PERKIRAAN_KAS', 4);
define('JENIS_PERKIRAAN_BANK', 5);
define('JENIS_PERKIRAAN_PIUTANG_DAGANG', 6);
define('JENIS_PERKIRAAN_PERSEDIAAN', 7);
define('JENIS_PERKIRAAN_AKTIVA_TETAP', 8);
define('JENIS_PERKIRAAN_PEROLEHAN', 9);
define('JENIS_PERKIRAAN_PENYUSUTAN', 10);
define('JENIS_PERKIRAAN_AKTIVA_LAIN_LAIN', 11);
define('JENIS_PERKIRAAN_UTANG', 12);
define('JENIS_PERKIRAAN_UTANG_DAGANG', 13);
define('JENIS_PERKIRAAN_UTANG_LANCAR', 14);
define('JENIS_PERKIRAAN_UTANG_JANGKA_PANJANG', 15);
define('JENIS_PERKIRAAN_UTANG_LAIN_LAIN', 16);
define('JENIS_PERKIRAAN_MODAL', 17);
define('JENIS_PERKIRAAN_LABA_RUGI_BERJALAN', 18);
define('JENIS_PERKIRAAN_PENDAPATAN', 19);
define('JENIS_PERKIRAAN_BIAYA', 20);
define('JENIS_PERKIRAAN_HARGA_POKOK_PENJUALAN', 21);
define('JENIS_PERKIRAAN_BIAYA_ADMINISTRASI_DAN_UMUM', 22);
define('JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN', 23);
define('JENIS_PERKIRAAN_BIAYA_LAIN_LAIN', 24);
define('JENIS_PERKIRAAN_PAJAK', 25);
define('JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI', 26);

// Kelompok/Rincian Perkiraan
define('KR_KELOMPOK', 1);
define('KR_RINCIAN', 2);
define('KR_SUB_RINCIAN', 3);

// Jenis Laporan
define('JENIS_LAPORAN_NERACA', 1);
define('JENIS_LAPORAN_LABA_RUGI', 2);

// Saldo Normal
define('SALDO_NORMAL_DEBIT', 1);
define('SALDO_NORMAL_KREDIT', 2);

// UNIT who can see STATUS MEDIS (Pemeriksaan and Assesment)
define('STATUS_MEDIS_UNIT_RAWAT_JALAN', 'rawat_jalan');
define('STATUS_MEDIS_UNIT_IGD', 'igd');
define('STATUS_MEDIS_UNIT_RAWAT_INAP', 'rawat_inap');
define('STATUS_MEDIS_UNIT_OK', 'ok');

// FORM CPPT
define('CPPT_VISITE_FORM', 'visite');
define('CPPT_LAPORAN_OK_FORM', 'laporan_ok');
$config['form_cppt'] = array(
    CPPT_VISITE_FORM => 'Visite',
    CPPT_LAPORAN_OK_FORM => 'Laporan OK',
);

// FORM KEPERAWATAN RAWAT INAP
$config['form_keperawatan_rawat_inap'] = array(
    1 => 'Pengkajian Awal Keperawatan Dewasa/Lansia',
    2 => 'Pengkajian Pasien Anak',
    3 => 'Pengkajian Pasien Obstetric Dan Ginekologi',
    4 => 'Pengkajian Pasien Neonatus (untuk usia <= 28 hari)',
    5 => 'Pengkajian Risiko Jatuh Pada Pasien Neonatus',
    6 => 'Pengkajian Risiko Jatuh Pada Pasien Anak',
    7 => 'Pengkajian Risiko Jatuh Pada Pasien Dewasa',
    8 => 'Penilaian Status Fungsional (berdasarkan penilaian barthel index)',
    9 => 'Pengkajian Integritas Kulit Pada Pasien Dewasa',
    10 => 'Pengkajian Integritas Kulit Pada Pasien Pediatric',
    11 => 'Pengkajian Integritas Kulit Pada Pasien Neonatus',
    12 => 'Diagnose Dan Perencanaan Asuhan Keperawatan Kode A Sampai Kode H Sebanyak 5 Lembar',
);

/*
|--------------------------------------------------------------------------
| Jenis Kelamin
|--------------------------------------------------------------------------
*/

$config['jenis_kelamin'] = array(
    JENIS_KELAMIN_LAKI_LAKI => 'Laki-laki',
    JENIS_KELAMIN_PEREMPUAN => 'Perempuan',
);

/*
|--------------------------------------------------------------------------
| Jenis Identitas
|--------------------------------------------------------------------------
*/

$config['jenis_identitas'] = array(
    JENIS_INDENTITAS_KTP => 'KTP',
    JENIS_INDENTITAS_SIM => 'SIM',
    JENIS_INDENTITAS_PASPOR => 'Paspor',
    JENIS_INDENTITAS_KTM => 'KTM',
);

/*
|--------------------------------------------------------------------------
| Jenis Telepon
|--------------------------------------------------------------------------
*/

$config['jenis_telepon'] = array(
    JENIS_TELEPON_HP => 'HP',
    JENIS_TELEPON_TELEPON_RUMAH => 'Telepon Rumah',
    JENIS_TELEPON_TELEPON_KANTOR => 'Telepon Kantor',
);

/*
|--------------------------------------------------------------------------
| Golongan Darah
|--------------------------------------------------------------------------
*/

$config['golongan_darah'] = array(
    GOLONGAN_DARAH_O => 'O',
    GOLONGAN_DARAH_A => 'A',
    GOLONGAN_DARAH_B => 'B',
    GOLONGAN_DARAH_AB => 'AB',
);

/*
|--------------------------------------------------------------------------
| Agama
|--------------------------------------------------------------------------
*/

$config['agama'] = array(
    AGAMA_ISLAM => 'Islam',
    AGAMA_HINDU => 'Hindu',
    AGAMA_BUDHA => 'Budha',
    AGAMA_KATOLIK => 'Katolik',
    AGAMA_PROTESTAN => 'Protestan',
    AGAMA_KONGHUCU => 'Konghucu',
    AGAMA_LAIN_LAIN => 'Lain-lain',
);

/*
|--------------------------------------------------------------------------
| Status Kawin
|--------------------------------------------------------------------------
*/

$config['status_kawin'] = array(
    STATUS_KAWIN_KAWIN => 'Kawin',
    STATUS_KAWIN_TIDAK_KAWIN => 'Tidak Kawin',
    STATUS_KAWIN_DUDA => 'Duda',
    STATUS_KAWIN_JANDA => 'Janda',
    STATUS_KAWIN_DIBAWAH_UMUR => 'Dibawah Umur',
);

/*
|--------------------------------------------------------------------------
| Rujukan dari
|--------------------------------------------------------------------------
*/

$config['rujukan_dari'] = array(
    BUKAN_RUJUKAN => 'Pasien Datang Sendiri',
    RUJUKAN_FKTP => 'FKTP',
    RUJUKAN_FKRTL => 'FKRTL',
);

/*
|--------------------------------------------------------------------------
| Cara Pembayaran
|--------------------------------------------------------------------------
*/

$config['cara_bayar'] = array(
    CARA_BAYAR_UMUM => 'Umum',
    CARA_BAYAR_BPJS => 'BPJS',
    CARA_BAYAR_JAMKESDA => 'Jamkesda',
    CARA_BAYAR_ASURANSI => 'Asuransi',
    CARA_BAYAR_PERUSAHAAN => 'Perusahaan',
    CARA_BAYAR_INTERNAL => 'Internal',
);

/*
|--------------------------------------------------------------------------
| // Hubungan Penanggung Jawab dengan Pasien
|--------------------------------------------------------------------------
*/

$config['pj_hubungan'] = array(
    PJ_HUBUNGAN_AYAH => 'Ayah',
    PJ_HUBUNGAN_IBU => 'Ibu',
    PJ_HUBUNGAN_ANAK => 'Anak',
    PJ_HUBUNGAN_ADIK => 'Adik',
    PJ_HUBUNGAN_KAKAK => 'Kakak',
    PJ_HUBUNGAN_SUAMI => 'Suami',
    PJ_HUBUNGAN_ISTRI => 'Istri',
    PJ_HUBUNGAN_WALI => 'Wali',
);

/*
|--------------------------------------------------------------------------
| Jenis Komponen Tarif
|--------------------------------------------------------------------------
*/

$config['jenis_komponen_tarif'] = array(
    JENIS_KOMPONEN_TARIF_JASA_DOKTER => 'Jasa Dokter',
    JENIS_KOMPONEN_TARIF_RUMAH_SAKIT => 'Rumah Sakit',
    JENIS_KOMPONEN_TARIF_INSENTIF_KARYAWAN => 'Insentif Karyawan',
    JENIS_KOMPONEN_TARIF_LAIN_LAIN => 'Lain-lain',
);

/*
|--------------------------------------------------------------------------
| Jenis Kelas
|--------------------------------------------------------------------------
*/

$config['jenis_kelas'] = array(
    KELAS_VVIP => 'Kelas VVIP',
    KELAS_VIP => 'Kelas VIP',
    KELAS_UTAMA => 'Kelas UTAMA',
    KELAS_I => 'Kelas I',
    KELAS_II => 'Kelas II',
    KELAS_III => 'Kelas III',
);

/*
|--------------------------------------------------------------------------
| Status Barang
|--------------------------------------------------------------------------
*/

$config['status_barang'] = array(
    0 => '(Pilih)',
    1 => 'Baik',
    2 => 'Rusak',
    3 => 'Di Service',
);
/*
|--------------------------------------------------------------------------
| Status Permintaan
|--------------------------------------------------------------------------
*/

$config['status_permintaan'] = array(
    0 => 'Permintaan',
    1 => 'Diterima',
);

/*
|--------------------------------------------------------------------------
| Golongan Operasi
|--------------------------------------------------------------------------
*/

$config['golongan_operasi'] = array(
    GOLONGAN_OPERASI_KECIL => 'Kecil',
    GOLONGAN_OPERASI_SEDANG => 'Sedang',
    GOLONGAN_OPERASI_BESAR => 'Besar',
    GOLONGAN_OPERASI_KHUSUS => 'Khusus',
);

/*
|--------------------------------------------------------------------------
| Jenis Unit
|--------------------------------------------------------------------------
*/

$config['jenis_unit'] = array(
    UNIT_LAYANAN_RAWAT_JALAN => 'Rawat Jalan',
    UNIT_LAYANAN_IGD => 'IGD',
    UNIT_LAYANAN_RAWAT_INAP => 'Rawat Inap',
    UNIT_LAYANAN_PENUNJANG_MEDIS => 'Penunjang Medis',
    UNIT_LAYANAN_AMBULANCE => 'Ambulance',
    UNIT_LAYANAN_LAIN_LAIN => 'Lain-lain',
);

/*
|--------------------------------------------------------------------------
| Jenis ODS/ODC
|--------------------------------------------------------------------------
*/
$config['jenis_ods_odc'] = array(
    ODS => 'ODS',
    ODC => 'ODC',
);

/*
|--------------------------------------------------------------------------
| Jenis Penunjang Medis
|--------------------------------------------------------------------------
*/

$config['jenis_penunjang_medis'] = array(
    PENUNJANG_MEDIS_LABORATORIUM => 'Laboratorium',
    PENUNJANG_MEDIS_RADIOLOGI => 'Radiologi',
    PENUNJANG_MEDIS_OK => 'OK',
);

/*
|--------------------------------------------------------------------------
| Jenis Ruang
|--------------------------------------------------------------------------
*/

$config['jenis_ruang'] = array(
    JENIS_RUANG_RAWAT_INAP => 'Ruang Rawat Inap',
    JENIS_RUANG_OPERASI => 'Ruang Operasi'
);

/*
|--------------------------------------------------------------------------
| Status Bed
|--------------------------------------------------------------------------
*/

$config['status_bed'] = array(
    STATUS_BED_KOSONG => 'Kosong',
    STATUS_BED_ISI => 'Isi'
);

/*
|--------------------------------------------------------------------------
| Kategori Perkiraan
|--------------------------------------------------------------------------
*/

$config['kategori_perkiraan'] = array(
    KATEGORI_PERKIRAAN_AKTIVA => 'Aktiva',
    KATEGORI_PERKIRAAN_UTANG => 'Utang',
    KATEGORI_PERKIRAAN_MODAL => 'Modal',
    KATEGORI_PERKIRAAN_PENDAPATAN => 'Pendapatan',
    KATEGORI_PERKIRAAN_BIAYA => 'Biaya',
);

/*
|--------------------------------------------------------------------------
| Jenis Perkiraan
|--------------------------------------------------------------------------
*/

$config['jenis_perkiraan'] = array(
    JENIS_PERKIRAAN_AKTIVA => 'Aktiva',
    JENIS_PERKIRAAN_AKTIVA_LANCAR => 'Aktiva Lancar',
    JENIS_PERKIRAAN_KAS_KECIL => 'Kas Kecil',
    JENIS_PERKIRAAN_KAS => 'Kas',
    JENIS_PERKIRAAN_BANK => 'Bank',
    JENIS_PERKIRAAN_PIUTANG_DAGANG => 'Piutang Dagang',
    JENIS_PERKIRAAN_PERSEDIAAN => 'Persediaan',
    JENIS_PERKIRAAN_AKTIVA_TETAP => 'Aktiva Tetap',
    JENIS_PERKIRAAN_PEROLEHAN => 'Perolehan',
    JENIS_PERKIRAAN_PENYUSUTAN => 'Penyusutan',
    JENIS_PERKIRAAN_AKTIVA_LAIN_LAIN => 'Aktiva Lain-lain',
    JENIS_PERKIRAAN_UTANG => 'Utang',
    JENIS_PERKIRAAN_UTANG_DAGANG => 'Utang Usaha',
    JENIS_PERKIRAAN_UTANG_LANCAR => 'Utang Lancar',
    JENIS_PERKIRAAN_UTANG_JANGKA_PANJANG => 'Kewajiban Jangka Panjang',
    JENIS_PERKIRAAN_UTANG_LAIN_LAIN => 'Kewajiban Lain-lain',
    JENIS_PERKIRAAN_MODAL => 'Modal',
	JENIS_PERKIRAAN_LABA_RUGI_BERJALAN => 'Laba/Rugi berjalan',
    JENIS_PERKIRAAN_PENDAPATAN => 'Pendapatan',
    JENIS_PERKIRAAN_BIAYA => 'Biaya',
    JENIS_PERKIRAAN_HARGA_POKOK_PENJUALAN => 'Harga Pokok Penjualan',
    JENIS_PERKIRAAN_BIAYA_ADMINISTRASI_DAN_UMUM => 'Biaya Administrasi dan Umum',
    JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN => 'Pendapatan Lain-lain',
    JENIS_PERKIRAAN_BIAYA_LAIN_LAIN => 'Biaya Lain-lain',
    JENIS_PERKIRAAN_PAJAK => 'Pajak',
    JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI => 'Ikhtisar Laba/Rugi',
);

// TIPE KARTU STOCK
$config['tipe_kartu_stock_penerimaan'] = 1;
$config['tipe_kartu_stock_retur'] = 2;
$config['tipe_kartu_stock_penerimaan_retur'] = 3;
$config['tipe_kartu_stock_pengeluaran'] = 4;
$config['tipe_kartu_stock_mutasi'] = 5;
$config['tipe_kartu_stock_penerimaan_mutasi'] = 6;
$config['tipe_kartu_stock_stock_opname'] = 7;
$config['tipe_kartu_stock_penjualan_non_racikan'] = 8;
$config['tipe_kartu_stock_penjualan_racikan'] = 9;
$config['tipe_kartu_stock_pengeluaran_layanan'] = 10;
$config['tipe_kartu_stock_retur_penjualan'] = 11;
$config['tipe_kartu_stock'] = array(
    $config['tipe_kartu_stock_penerimaan'] => 'Penerimaan',
    $config['tipe_kartu_stock_retur'] => 'Retur',
    $config['tipe_kartu_stock_penerimaan_retur'] => 'Penerimaan Retur',
    $config['tipe_kartu_stock_pengeluaran'] => 'Pengeluaran',
    $config['tipe_kartu_stock_mutasi'] => 'Mutasi',
    $config['tipe_kartu_stock_penerimaan_mutasi'] => 'Penerimaan Mutasi',
    $config['tipe_kartu_stock_stock_opname'] => 'Stock Opname',
    $config['tipe_kartu_stock_penjualan_non_racikan'] => 'Penjualan Non Racikan',
    $config['tipe_kartu_stock_penjualan_racikan'] => 'Penjualan Racikan',
    $config['tipe_kartu_stock_pengeluaran_layanan'] => 'Pengeluaran dari Layanan',
    $config['tipe_kartu_stock_retur_penjualan'] => 'Retur Penjualan',
);

// STATUS PURCHASE ORDER
$config['status_po_waiting_for_approval'] = 1;
$config['status_po_purchasing'] = 2;
$config['status_po_waiting_for_delivery'] = 3;
$config['status_po_partial_received'] = 4;
$config['status_po_closed'] = 5;
$config['status_po_rejected'] = 6;
$config['status_po_diverted'] = 7;
$config['status_po'] = array(
    $config['status_po_waiting_for_approval'] => 'Menunggu Approval Manager',
    $config['status_po_purchasing'] => 'Purchasing',
    $config['status_po_waiting_for_delivery'] => 'Menunggu Pengiriman',
    $config['status_po_partial_received'] => 'Diterima Sebagian',
    $config['status_po_closed'] => 'Diterima Semua',
    $config['status_po_rejected'] => 'Ditolak',
    $config['status_po_diverted'] => 'Dialihkan',
);

// SIFAT PURCHASE ORDER
$config['sifat_po'] = array(
    'cito' => 'Cito',
    'rutin' => 'Rutin',
    'biasa' => 'Biasa',
);

$config['status_partial'] = 1;
$config['status_closed'] = 2;
$config['status_done'] = 3;

// STATUS PENERIMAAN
$config['status_penerimaan'] = array(
    $config['status_partial'] => 'Diterima Sebagian',
    $config['status_closed'] => 'Diterima',
);

// STATUS PENGELUARAN
$config['status_pengeluaran'] = array(
    $config['status_partial'] => 'Dikirim Sebagian',
    $config['status_closed'] => 'Dikirim',
    $config['status_done'] => 'Diterima oleh Unit',
);

// STATUS REQUEST
$config['status_request_unprocessed'] = 1;
$config['status_request_partial'] = 2;
$config['status_request_closed'] = 3;
$config['status_request'] = array(
    $config['status_request_unprocessed'] => 'Belum Diproses',
    $config['status_request_partial'] => 'Dikirim Sebagian',
    $config['status_request_closed'] => 'Dikirim',
);

// STATUS MUTASI
$config['status_mutasi_unprocessed'] = 1;
$config['status_mutasi_processed'] = 2;
$config['status_mutasi'] = array(
    $config['status_mutasi_unprocessed'] => 'Belum Diproses',
    $config['status_mutasi_processed'] => 'Berhasil',
);

// STATUS STOCK OPNAME
$config['status_so_pemeriksaan_pertama'] = 1;
$config['status_so_pemeriksaan_kedua'] = 2;

// KELOMPOK SIGNA
define('KELOMPOK_SIGNA_CARA_MAKAN', 'CARA MAKAN');
define('KELOMPOK_SIGNA_DOSIS', 'DOSIS');
define('KELOMPOK_SIGNA_LAINNYA', 'LAINNYA');
$config['kelompok_signa'] = array(
    KELOMPOK_SIGNA_CARA_MAKAN => 'CARA MAKAN',
    KELOMPOK_SIGNA_DOSIS => 'DOSIS',
    KELOMPOK_SIGNA_LAINNYA => 'LAINNYA',
);

// KATEGORI DOKTER
define('KATEGORI_DOKTER_UMUM', 'umum');
define('KATEGORI_DOKTER_BEDAH', 'bedah');
define('KATEGORI_DOKTER_SYARAF', 'syaraf');
define('KATEGORI_DOKTER_BIDAN', 'bidan');
$config['kategori_dokter'] = array(
	KATEGORI_DOKTER_UMUM => 'Dokter Umum',
	KATEGORI_DOKTER_BEDAH => 'Dokter Bedah',
	KATEGORI_DOKTER_SYARAF => 'Dokter Syaraf',
	KATEGORI_DOKTER_BIDAN => 'Bidan',
);

// UNIT TENAGA KESEHATAN
define('UNIT_TENAGA_KESEHATAN_FARMASI', 'farmasi');
define('UNIT_TENAGA_KESEHATAN_LOGISTIK', 'logistik');
define('UNIT_TENAGA_KESEHATAN_PERAWAT', 'perawat');
define('UNIT_TENAGA_KESEHATAN_OK', 'ok');
define('UNIT_TENAGA_KESEHATAN_LABORATORIUM', 'laboratorium');
define('UNIT_TENAGA_KESEHATAN_RADIOLOGI', 'radiologi');
$config['unit_tenaga_kesehatan'] = array(
	UNIT_TENAGA_KESEHATAN_FARMASI => 'Farmasi',
	UNIT_TENAGA_KESEHATAN_LOGISTIK => 'Logistik',
	UNIT_TENAGA_KESEHATAN_PERAWAT => 'Perawat',
    UNIT_TENAGA_KESEHATAN_OK => 'OK',
    UNIT_TENAGA_KESEHATAN_LABORATORIUM => 'Laboratorium',
	UNIT_TENAGA_KESEHATAN_RADIOLOGI => 'Radiologi',
);
