<?php

/**
 * Main Navigation.
 * Primarily being used in views/layouts/admin.php.
 */
$config['navigation'] = array(
    'dashboard' => array(
        'uri' => 'home',
        'title' => 'Dashboard',
        'icon' => 'fa fa-dashboard',
    ),
    // 'master' => array(
    //     'title' => 'Master',
    //     'icon' => 'fa fa-hdd-o',
    //     'children' => array(
    //         'master_dasar' => array(
    //             'title' => 'Data Dasar',
    //             'children' => array(
    //                 'agama' => array(
    //                     'title' => 'Agama',
    //                     'uri' => 'master/agama',
    //                 ),
    //                 'jabatan' => array(
    //                     'title' => 'Jabatan',
    //                     'uri' => 'master/jabatan',
    //                 ),
    //                 'title' => array(
    //                     'title' => 'Title',
    //                     'uri' => 'master/title',
    //                 ),
    //                 'pekerjaan' => array(
    //                     'title' => 'Pekerjaan',
    //                     'uri' => 'master/pekerjaan',
    //                 ),
    //                 'pendidikan' => array(
    //                     'title' => 'Pendidikan',
    //                     'uri' => 'master/pendidikan',
    //                 ),
    //                 'pegawai' => array(
    //                     'title' => 'Pegawai',
    //                     'uri' => 'master/pegawai',
    //                 ),
    //                 'wilayah' => array(
    //                     'title' => 'Wilayah',
    //                     'uri' => 'master/wilayah',
    //                 ),
    //             ),
    //         ),
    //     )
    // ),
    'user' => array(
        'uri' => 'user',
        'title' => 'User',
        'icon' => 'fa fa-users',
    ),
    'owner' => array(
        'uri' => 'owner',
        'title' => 'Owner',
        'icon' => 'fa fa-user',
    ),
    'supplier' => array(
        'uri' => 'supplier',
        'title' => 'Merchant',
        'icon' => 'fa fa-user',
    ),
    'kategori' => array(
        'uri' => 'kategori',
        'title' => 'Kategori',
        'icon' => 'fa fa-navicon',
    ),
    'produk' => array(
        'uri' => 'produk',
        'title' => 'Produk',
        'icon' => 'fa fa-shopping-cart',
    ),
    'order' => array(
        'uri' => 'order',
        'title' => 'Order',
        'icon' => 'fa fa-dropbox',
    ),
    // 'user-management' => array(
    //     'uri' => 'auth/user',
    //     'title' => 'User Management',
    //     'icon' => 'fa fa-user',
    // ),
    'acl' => array(
        'title' => 'ACL',
        'icon' => 'fa fa-unlock-alt',
        'children' => array(
            'rules' => array(
                'uri' => 'acl/rule',
                'title' => 'Rules',
            ),
            'roles' => array(
                'uri' => 'acl/role',
                'title' => 'Roles',
            ),
            'resources' => array(
                'uri' => 'acl/resource',
                'title' => 'Resources',
            ),
        ),
    ),
    'utils' => array(
        'title' => 'Utils',
        'icon' => 'fa fa-wrench',
        'children' => array(
            'style_guides' => array(
                'uri' => 'utils/style_guides',
                'title' => 'Style Guides',
            ),
            'system_logs' => array(
                'uri' => 'utils/logs/system',
                'title' => 'System Logs',
            ),
            'deploy_logs' => array(
                'uri' => 'utils/logs/deploy',
                'title' => 'Deploy Logs',
            ),
            'info' => array(
                'uri' => 'utils/info',
                'title' => 'Info',
            ),
        ),
    ),
);
