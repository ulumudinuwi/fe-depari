<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends Admin_Controller 
{
	protected $page_title = '<i class="icon-dropbox"></i> Produk';
	protected $def_uri = 'produk';
	protected $jenis_barang;

	public function __construct()
	{
		parent::__construct();

		// $this->load->model('produk/Agama_model');
	}

	public function index()
	{
		$this->data['page_icons'] = '<a href="'. site_url("produk/edit") .'" class="btn btn-primary btn-labeled btn_add"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_script($this->def_uri . '/script-index')
			->build($this->def_uri . '/index', $this->data);
	}
	
	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.status")
			 ->from($this->table_def.' a');
		echo $this->datatables->generate();
	}

	public function edit()
	{
		$uid = $this->input->get('id') ? $this->input->get('id') : '';
		
		if ($uid === "") {
			$this->data['page_title'] = '<i class="icon-dropbox"></i> Tambah Produk';
			$this->data['uid'] = $uid;
		}
		else {
			$this->data['page_title'] = '<i class="icon-dropbox"></i> Edit Produk';
			$this->data['uid'] = $uid;
		}
		
		$this->template
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
            ->set_js('plugins/editors/wysihtml5/wysihtml5.min', FALSE)
            ->set_js('plugins/editors/wysihtml5/toolbar', FALSE)
            ->set_js('plugins/editors/wysihtml5/parsers', FALSE)
            ->set_js('plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
            ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_script($this->def_uri . '/script-edit')
			->build($this->def_uri . '/edit', $this->data);
	}

}