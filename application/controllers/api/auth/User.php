<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class User extends Admin_Controller 
{
	protected $table_def_pegawai = "m_pegawai";

	function __construct()
	{
		parent::__construct();
	}

	public function index() 
	{
		echo $this->user_model->datatable();
	}

	public function load_unit_kerja() {

		$unit_usaha_id = $this->input->get('unit_usaha_id');

		$unit_kerja = $this->Unit_kerja_model->dropdown_options(array('unit_usaha_id' => $unit_usaha_id));

		echo json_encode($unit_kerja);
	}

	public function verifikasi() {
		$password = json_decode(base64_decode($this->input->post('password')));
		$username = $_POST['username'];
		$pegawai_id = $_POST['pegawai_id'];
		$type = $_POST['type'];

		$user = $this->user_model->get_by_username($username);

        if ($user && $this->user_model->check_password($password, $user->password)) {	        
	        switch ($type) {
				case 'peracik':
					if($user->pegawai_id != $pegawai_id) {
		        		$this->output->set_status_header(504)
		            		->set_output(json_encode(['message' => 'Akun tersebut bukan milik user peracik.']));
		            	return;	
		        	}
					break;
				default:
					if($user->id != $this->auth->userid()) {
		        		$this->output->set_status_header(504)
		            		->set_output(json_encode(['message' => 'Akun tersebut tidak sama dengan akun login saat ini.']));
		            	return;	
		        	}
					break;
			}

	        $this->output->set_status_header(200)
	            ->set_output(json_encode(['message' => 'success']));
	        return;
        }

		$this->output->set_status_header(504)
		    	->set_output(json_encode(['message' => 'Akun tersebut tidak tersedia, hubungi Administrator untuk keterangan lebih lanjut.']));
	}

  	// LOAD MODAL PEGAWAI
  	public function load_data_pegawai() {
    	$this->datatables->select("id, nip, nama_depan, tanggal_lahir")
    		->where('status', 1)
        	->from($this->table_def_pegawai);
    	echo $this->datatables->generate();
  	}
}