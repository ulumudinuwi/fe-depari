<?php defined('BASEPATH') OR exit('No direct script access allowed');

//include APPPATH.'/libraries/KeyInForeignTable.php';

class Jabatan extends CI_Controller 
{
	//use KeyInForeignTable;
	
	protected $table_def = "m_jabatan";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Jabatan_model');
	}

	public function get_all() {
		if (! $this->input->is_ajax_request())
				exit();

		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def}.deleted = 0";
		$aWheres[] = "{$this->table_def}.status = 1";
		if(count($aWheres) > 0) 
			$sWhere = implode(' AND ', $aWheres);
		if(!empty($sWhere)) 
			$sWhere = "WHERE ".$sWhere;

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.nama",
		);
		$result = $this->Jabatan_model->get_all(0, 0, $sWhere, "ORDER BY {$this->table_def}.nama ASC", $aSelect)['data'];
		echo json_encode(['list' => $result]);
	}

	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.status")
			->from($this->table_def.' a')
			->where('a.deleted', 0);
		
		$response = $this->datatables->generate();
		$json = json_decode($response);
        
		$response = json_encode($json);
		echo $response;
	}
	
	public function simpan() {

		if (!$this->input->is_ajax_request())
			exit();

		$this->Jabatan_model->save();
		echo json_encode(['action' => 'simpan']);
	}
	
	public function hapus() {

		if (!$this->input->is_ajax_request())
			exit();

	  	$uid = $this->input->get('uid');
		$this->Jabatan_model->delete_kelas($uid);
		echo json_encode(['action' => 'hapus']);
	}
	
	public function get_kelas_all() {

		if (!$this->input->is_ajax_request())
			exit();

		$kelas_list = $this->Jabatan_model->get_all(0, 0, "", "ORDER BY m_jabatan.jenis ASC");
		echo json_encode(['action' => 'get_kelas_all', 'kelas_list' => $kelas_list["data"]]);
	}

	public function edit_status() {
	    if (!$this->input->is_ajax_request())
	      exit();

	  $uid = $this->input->post('uid');
	  $deskripsi = $this->input->post('deskripsi');
	  $status = $this->input->post('status');

	  $result = $this->Jabatan_model->update_status($uid, $status);
	  echo json_encode($result);
	}	
	
}