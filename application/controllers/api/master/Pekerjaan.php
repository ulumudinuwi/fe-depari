<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include APPPATH.'/libraries/KeyInForeignTable.php';

class Pekerjaan extends CI_Controller 
{
	//use KeyInForeignTable;
	
	protected $table_def = "m_pekerjaan";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('master/Pekerjaan_model');
	}
	
	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.status")
			 ->from($this->table_def.' a')
			 ->where('a.deleted', 0);
		
		$response = $this->datatables->generate();
		$json = json_decode($response);
		
		$response = json_encode($json);
		echo $response;
	}
	
	public function get_all() {

		if (!$this->input->is_ajax_request())
			exit();
		
		$pekerjaanList = $this->Pekerjaan_model->get_all(0, 0, "", "ORDER BY {$this->table_def}.ordering ASC");
		$output['pekerjaan_list'] = $pekerjaanList["data"];
		echo json_encode($output);
	}
	
	public function simpan() {

		if (!$this->input->is_ajax_request())
			exit();

		$this->Pekerjaan_model->save();
		echo json_encode(['action' => 'simpan']);
	}

	public function hapus() {

		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->get('uid');
		$this->Pekerjaan_model->delete_pekerjaan($uid);
		echo json_encode(['action' => 'hapus']);
	}

	public function edit_status() {
	    if (!$this->input->is_ajax_request())
	      exit();

	  $uid = $this->input->post('uid');
	  $status = $this->input->post('status');

	  $result = $this->Pekerjaan_model->update_status($uid, $status);
	  echo json_encode($result);
	}

}