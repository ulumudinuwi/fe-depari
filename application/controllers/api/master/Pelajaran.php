<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pelajaran extends CI_Controller 
{
	protected $table_def = "m_pelajaran";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Pelajaran_model');
	}

	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.status, b.nama as kategori_pelajaran")
            ->join('m_kategori_pelajaran as b', 'a.kategori_pelajaran_id = b.id', 'left')
			->from($this->table_def.' a')
			->where('a.deleted', 0);
			
		$response = $this->datatables->generate();
		$json = json_decode($response);
		$response = json_encode($json);
		echo $response;
	}

	public function fetch_all() {
        if (! $this->input->is_ajax_request())
            exit();

        $result = $this->db->select('id, nama, kategori_pelajaran_id, jenis_kelamin')
        	->where('status', 1)
        	->where('deleted', 0)
            ->order_by('id', 'asc')
            ->get($this->table_def)->result();

        echo json_encode(['data' => $result]);
    }

	public function simpan() {

		if (!$this->input->is_ajax_request())
			exit();

		$this->Pelajaran_model->save();
		echo json_encode(['action' => 'simpan']);
	}

	public function hapus() {

		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->get('uid');
		$this->Pelajaran_model->delete_title($uid);
		echo json_encode(['action' => 'hapus']);
	}

	public function edit_status() {
	    if (!$this->input->is_ajax_request())
	      exit();

	  $uid = $this->input->post('uid');
	  $status = $this->input->post('status');

	  $result = $this->Pelajaran_model->update_status($uid, $status);
	  echo json_encode($result);
	}
}