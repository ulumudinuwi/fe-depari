<?php defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'/libraries/KeyInForeignTable.php';

class Perusahaan extends CI_Controller 
{
	use KeyInForeignTable;
	
	protected $table_def = "m_perusahaan";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Perusahaan_model');
	}

	public function load_data() {
		$jenis = $this->input->post('jenis');
		$response = $this->datatables->select("a.id, a.uid, a.kode, a.nama, status")
			->from($this->table_def.' a')
			->where('a.deleted', 0);
		if ((int) $jenis != 0) {
			$response->where('a.jenis', $jenis);
		}
		
		$response = $this->datatables->generate();
		$json = json_decode($response);
        foreach ($json->data as $i => $data) {
            $relationCount = $this->key_in_foreign_table($data->id, [
                't_pelayanan' => 'perusahaan_id'
            ]);
            $json->data[$i]->deleted = (int) $relationCount->counts === 0 ? true : false;
        }
		$response = json_encode($json);
		echo $response;
	}

	public function fetch_by_jenis() {
        if (! $this->input->is_ajax_request())
            exit();

        $jenis = $this->input->get('q') ? $this->input->get('q') : "";
        if(base64_decode($jenis, true)) $jenis = base64_decode($jenis);

        $result = $this->db->select('id, nama')
        	->where('status', 1)
        	->where('deleted', 0)
            ->where('jenis', $jenis)
            ->order_by('nama', 'asc')
            ->get($this->table_def)
            ->result();

        echo json_encode(['data' => $result]);
    }

	public function simpan() {

		if (!$this->input->is_ajax_request())
			exit();

		$this->Perusahaan_model->save();
		echo json_encode(['action' => 'simpan']);
	}
	
	public function hapus() {

		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->get('uid');
		$this->Perusahaan_model->delete_perusahaan($uid);
		echo json_encode(['action' => 'hapus']);
	}
	
	public function get_perusahaan_by_jenis() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$jenis = $this->input->get('jenis');
		
		$perusahaanList = $this->Perusahaan_model->get_all(0, 0, "WHERE (".$this->table_def.".jenis = ".$jenis.")", "ORDER BY ".$this->table_def.".kode ASC");
		echo json_encode(['action' => 'get_perusahaan_by_jenis', 'perusahaan_list' => $perusahaanList["data"]]);
	}

	public function edit_status() {
	    if (!$this->input->is_ajax_request())
	      exit();

	  $uid = $this->input->post('uid');
	  $status = $this->input->post('status');

	  $result = $this->Perusahaan_model->update_status($uid, $status);
	  echo json_encode($result);
	}	
}