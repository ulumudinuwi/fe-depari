<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Title extends CI_Controller 
{
	protected $table_def = "m_title";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Title_model');
	}

	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.singkatan, a.status")
			->from($this->table_def.' a')
			 ->where('a.deleted', 0);
			
		$response = $this->datatables->generate();
		$json = json_decode($response);
		$response = json_encode($json);
		echo $response;
	}

	public function fetch_all() {
        if (! $this->input->is_ajax_request())
            exit();

        $result = $this->db->select('id, nama, singkatan, jenis_kelamin')
        	->where('status', 1)
        	->where('deleted', 0)
            ->order_by('id', 'asc')
            ->get($this->table_def)->result();

        echo json_encode(['data' => $result]);
    }

	public function simpan() {

		if (!$this->input->is_ajax_request())
			exit();

		$this->Title_model->save();
		echo json_encode(['action' => 'simpan']);
	}

	public function hapus() {

		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->get('uid');
		$this->Title_model->delete_title($uid);
		echo json_encode(['action' => 'hapus']);
	}

	public function edit_status() {
	    if (!$this->input->is_ajax_request())
	      exit();

	  $uid = $this->input->post('uid');
	  $status = $this->input->post('status');

	  $result = $this->Title_model->update_status($uid, $status);
	  echo json_encode($result);
	}
}