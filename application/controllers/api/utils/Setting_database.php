<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_database extends Admin_Controller 
{
    protected $table_def_settings = "m_settings";
    protected $table_def_unit_usaha = "m_unitusaha";

    function __construct()
    {
        parent::__construct();

        $this->load->model('master/Settings_model');
    }

    public function load_data() 
    {
        $query = $this->db->order_by('kode', 'asc')->get($this->table_def_unit_usaha);
        $unit_usaha = array();
        foreach ($query->result() as $row) {
            $unit_usaha[$row->kode] = $row;
        }

        $aColumns = array('icon', 'unit', 'hostname', 'status', 'action');

        /* 
         * Paging
         */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $iLimit = intval( $_GET['iDisplayLength'] );
            $iOffset = intval( $_GET['iDisplayStart'] );
        }

        /*
         * Ordering
         */
        $sOrder = "ORDER BY ".$this->table_def_settings.".key ASC";

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = $this->table_def_settings.".key LIKE 'db_%'";
        $aWheres[] = $this->table_def_settings.".type = '". $this->Settings_model::TYPE_PENGADAAN ."'";

        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->Settings_model->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $unit = array_key_exists(substr($obj->key, 3), $unit_usaha) ? $unit_usaha[substr($obj->key, 3)] : null;

            $obj->unit = $unit ? $unit->nama : strtoupper(substr($obj->key, 3));
            $obj->status = 0;
            $obj->connected = false;
            $obj->label = '';

            // Check Connection
            $x = 0;
            while (! $obj->connected && $x < count($obj->value)) {
                $setting = $obj->value[$x];
                if (check_database_connection($setting)) {
                    $obj->connected = true;
                    $obj->label = property_exists($setting, 'label') ? $setting->label : '';
                }
                $x++;
            }

            $rows[] = get_object_vars($obj);
        }
        $output['aaData'] = $rows;

        echo json_encode($output);
    }

    public function get_data($uid) {
        if (! $this->input->is_ajax_request()) 
            exit();

        $setting = $this->Settings_model->get_by("WHERE ".$this->table_def_settings.".uid = '".$uid."'");

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $setting]));
    }

    public function check_connection() {
        if (! $this->input->is_ajax_request())
            exit();

        $setting = new stdClass();
        $setting->driver = $this->input->post('driver') == 'mysqli' ? 'mysql' : $this->input->post('driver');
        $setting->hostname = $this->input->post('hostname');
        $setting->port = $this->input->post('port') ? $this->input->post('port') : '3306';
        $setting->username = $this->input->post('username');
        $setting->password = $this->input->post('password');
        $setting->database = $this->input->post('database');

        $connected = check_database_connection($setting);

        if ($connected) {
            $this->output->set_status_header(200)
                    ->set_output(json_encode(['message' => 'Connected to database.']));
        } else {
            $this->output->set_status_header(503)
                    ->set_output(json_encode(['code' => $ex->getCode(), 'message' => $ex->getMessage()]));
        }
    }

    public function save()
    {
        if (! $this->input->is_ajax_request())
            exit();

        $obj = $this->_getDataObject();

        if (! isset($obj->uid) && empty($obj->uid)) {
            $this->output->set_status_header(404);
        } else {
            $uid = $obj->uid;

            unset($obj->id);
            unset($obj->uid);

            $data = [
                'value' => $obj->value
            ];

            if (! ($object = $this->Settings_model->updateByUid($uid, $data)) ) {
                $this->output->set_status_header(500)
                    ->set_output(json_encode(['message' => 'Terjadi kesalahan ketika menyimpan ke database']));
            } else {
                $this->output->set_status_header(200)
                    ->set_output(json_encode($object));
            }
        }
    }

    private function _getDataObject()
    {
        $obj = new stdClass();
        $obj->id = $this->input->post('id') && ($this->input->post('id') != "0") ? $this->input->post('id') : 0;
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "0") ? $this->input->post('uid') : 0;

        $databases = array();
        if (array_key_exists('database_driver', $_POST)) {
            for ($i = 0, $n = count($_POST['database_driver']); $i < $n; $i++) {
                $database = new stdClass();
                $database->label = $_POST['database_label'][$i];
                $database->driver = $_POST['database_driver'][$i];
                $database->hostname = $_POST['database_hostname'][$i];
                $database->port = $_POST['database_port'][$i];
                $database->username = $_POST['database_username'][$i];
                $database->password = $_POST['database_password'][$i];
                $database->dbname = $_POST['database_database'][$i];

                $databases[] = $database;
            }
        }
        $obj->value = json_encode($databases);

        return $obj;
    }

}