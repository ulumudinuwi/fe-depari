<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Login controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Login extends CI_Controller {
    
    function index() {
        // user is already logged in
        if ($this->auth->loggedin()) {
            redirect($this->config->item('dashboard_uri', 'template'));
        }
        
        $this->load->language('auth');
        $email = $this->input->post('email', FALSE);
        $password = $this->input->post('password', FALSE);
        $remember = $this->input->post('remember') ? TRUE : FALSE;
        $token = $this->input->post('token') ? TRUE : FALSE;
        $redirect = $this->input->get_post('redirect');
        
        $data = array();
        if ($email)
            $data['email'] = $email;
        if ($remember)
            $data['remember'] = $remember;
        
        // show login form
        // $this->load->helper('form');
        $this->template->set_layout('login')
        ->build('auth/login', $data);
    }

    function login_success() {

        // user is already logged in
        if ($this->auth->loggedin()) {
            redirect($this->config->item('dashboard_uri', 'template'));
        }
        
        $this->load->language('auth');

        $role = $this->input->post('role');
        $token = $this->input->post('token');
        $email = $this->input->post('email');
        $sup_id = $this->input->post('sup_id');
        $username = '';
        $password = 'admin';

        switch ($role) {
            case 'Operator':
                $username = 'operator';
                break;

            case 'Owner':
                $username = 'owner';
                break;
            
            default:
                $username = 'admin';
                break;
        }

        $redirect = $this->input->get_post('redirect');
        
        // form submitted
        if ($username && $password) {
            // get user from database
            $user = $this->user_model->get_by_username($username);
            if ($user && $this->user_model->check_password($password, $user->password)) {
                
                   $this->auth->login($user->id, $remember);
                    
                    // Add session data
                    $this->session->set_userdata(array(
                        'lang' => $user->lang,
                        'role_id' => $user->role_id,
                        'role_name' => $user->role_name,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'token' => $token,
                        'sup_id' => $sup_id,
                    ));

                    if ($redirect)
                        redirect($redirect);
                    else
                       echo true;
                }
                


            } else
                $this->template->add_message('warning', lang('login_attempt_failed'));
        
        $data = array();
        if ($username)
            $data['username'] = $username;
        if ($remember)
            $data['remember'] = $remember;
        
        echo true;
    }
    
}