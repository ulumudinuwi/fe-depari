<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin_Controller 
{
    protected $page_title = '<i class="icon-cogs"></i> Settings';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 
     */
    public function index()
    {
        $this->data['page_title'] = '<i class="icon-cogs"></i> Settings';
        $this->template
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/buttons/spin.min', FALSE)
            ->set_js('plugins/buttons/ladda.min', FALSE)
            ->set_script('utils/settings/script-index')
            ->build('utils/settings/index', $this->data);
    }

    public function form_database($uid) {
        $btnSave = '<button type="button" class="btn btn-success btn-labeled submit-button">';
        $btnSave .= '<b><i class="icon-floppy-disk"></i></b> ';
        $btnSave .= 'Simpan';
        $btnSave .= '</button>';

        $btnAdd = '<button type="button" class="btn btn-primary btn-labeled add-button">';
        $btnAdd .= '<b><i class="icon-database-add"></i></b> ';
        $btnAdd .= 'Tambah';
        $btnAdd .= '</button>';

        $btnKembali = '<button type="button" class="btn btn-default cancel-button">Kembali</button>';

        $this->data['uid'] = $uid;
        $this->data['page_icons'] = $btnSave.'&nbsp;'.$btnAdd.'&nbsp;'.$btnKembali;
        $this->data['page_title'] = '<i class="icon-database"></i> Settings Database';
        $this->template
            ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
            ->set_js('core/libraries/jquery_ui/touch.min', TRUE)
            ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
            ->set_js('plugins/forms/validation/validate.min.js', TRUE)
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
            ->set_js('plugins/trees/fancytree_all.min', TRUE)
            ->set_js('plugins/trees/fancytree_childcounter', TRUE)
            ->set_js('plugins/editors/wysihtml5/wysihtml5.min', FALSE)
            ->set_js('plugins/editors/wysihtml5/toolbar', FALSE)
            ->set_js('plugins/editors/wysihtml5/parsers', FALSE)
            ->set_js('plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA', FALSE)
            ->set_js('plugins/forms/styling/uniform.min', TRUE)
            ->set_js('plugins/forms/styling/switchery.min', TRUE)
            ->set_js('plugins/forms/styling/switch.min', TRUE)
            ->set_script('utils/settings/script-form-db')
            ->build('utils/settings/form-db', $this->data);
    }
}