<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Controller for authenticate controllers.
 * 
 * @package CI-Beam
 * @category Controller
 * @author Ardi Soebrata
 */
class API_Controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		
		// Redirect unlogged users to login page.
		if (!$this->auth->loggedin()) {
			$this->session->set_userdata('role_name', 'Guest');
			return $this->failed('Maaf user harus login');
		}
		
		// Get current user id.
		$id = $this->auth->userid();

		// Get user from database
		$user = $this->user_model->get_by_id($id);
		$user_data = array(
			'id'			=> $user->id,
			'first_name'	=> $user->first_name,
			'last_name'		=> $user->last_name,
			'username'		=> $user->username,
			'email'			=> $user->email,
			'lang'			=> $user->lang,
			'roles'			=> $user->roles,
			'role_id'		=> $user->role_id,
			'role_name'		=> $user->role_name,
			// 'pegawai_id' => $user->pegawai_id
		);
		$this->load->vars('auth_user', $user_data);
		$this->session->set_userdata($user_data);

		# Unit Usaha & Variable user yang akan digunakan
		$this->user = $user;
		
		// Check ACL
		$this->acl->build();
		$allowed = $this->acl->is_allowed($this->uri->uri_string());
		if (!$allowed) show_error(lang('error_401'), 401, lang('error_401_title'));
		
		$this->template->set_layout('admin');
	}

	protected function check_method($method){
	    if($this->input->method() == $method || $this->input->method(TRUE) == $method){
	        return true;
        }
        return false;
    }

    protected function is_affected($db){
        if($db->affected_rows() != 1) {
            throw new Exception('Gagal Simpan/Update');
            return;
        }
    }

	protected function success($data, $message = 'success', $status = 1) {
		$response = [
			'status'  => $status,
			'data'    => $data,
			'message' => $message 
		];

		return json_encode($response, 200);
	}

	protected function failed($data, $message = 'error', $status = 0) {
		$response = [
			'status'  => $status,
			'data'    => $data,
			'message' => $message 
		];

		return json_encode($response, 200);
	}
}