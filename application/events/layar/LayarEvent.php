<?php

use Sse\Event;
use Sse\SSE;

class LayarEvent implements Event {
    private $CI;
    private $last_checksum = null;

    public function __construct() {
        $this->CI =& get_instance();
    }

    public function check() {
        $row = $this->CI->db->select('(IFNULL(SUM(update_at), 0) + IFNULL(SUM(status), 0)) as checksum')
                ->where_in('status', [1,3])
                ->get('t_loket')
                ->row();
        
        $checksum = $row->checksum;
        if ($checksum != $this->last_checksum) {
            $this->last_checksum = $checksum;
            return true;
        }

        return false;
    }

    public function update() {
        return json_encode(['data' => true, 'timestamp' => time()]);
    }
}