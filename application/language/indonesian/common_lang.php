<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['field_id'] = 'ID';
$lang['field_uid'] = 'UID';
$lang['field_kode'] = 'Kode';
$lang['field_nama'] = 'Nama';
$lang['field_keterangan'] = 'Keterangan';
$lang['field_deskripsi'] = 'Deskripsi';
$lang['field_jenis'] = 'Jenis';
$lang['field_kategori'] = 'Kategori';
$lang['field_pilih'] = 'Pilih';
$lang['field_status'] = 'Status';
$lang['field_all'] = '- Semua -';

$lang['field_filter_range_tanggal'] = 'Rentang Waktu';
$lang['field_tanggal'] = 'Tanggal';
$lang['field_cara_bayar'] = 'Jaminan';
$lang['field_perusahaan'] = 'Perusahaan';
$lang['field_asal_pasien'] = 'Asal Pasien';
$lang['field_kelas'] = 'Kelas';
$lang['field_ruang'] = 'Ruangan';
$lang['field_bed'] = 'No. Bed';
$lang['field_dokter_perujuk'] = 'Dokter Perujuk';
$lang['field_dokter_lab'] = 'Dokter Lab';
$lang['field_dokter_rad'] = 'Dokter Radiologi';
$lang['field_diagnosa_klinis'] = 'Diagnosa Klinis';
$lang['field_diagnosa'] = 'Diagnosa';
$lang['field_layanan'] = 'Layanan';
$lang['field_poli'] = 'Poli';
$lang['field_no_register'] = 'No. Register';
$lang['field_dokter'] = 'Dokter';

// PASIEN
$lang['field_no_rekam_medis'] = 'No. RM';
$lang['field_nama'] = 'Nama';
$lang['field_jenis_kelamin'] = 'Jenis Kelamin';
$lang['field_alamat'] = 'Alamat';
$lang['field_tanggal_lahir'] = 'Tgl. Lahir';
$lang['field_umur'] = 'Usia';
$lang['field_telepon'] = 'No. Telepon';
$lang['select_dokter'] = '- Pilih Dokter -';


$lang['status_active'] = 'Aktif';
$lang['status_inactive'] = 'Non Aktif';

$lang['btn_save'] = 'Simpan';
$lang['btn_cancel'] = 'Batal';
$lang['btn_close'] = 'Tutup';
$lang['btn_back'] = 'Kembali';
$lang['btn_add'] = 'Tambah';
$lang['btn_delete'] = 'Hapus';
$lang['btn_restore'] = 'Restore';
$lang['btn_edit'] = 'Edit';
$lang['btn_reject'] = 'Tolak';
$lang['btn_reset'] = 'Reset';
$lang['btn_search'] = 'Cari';
$lang['pendaftaran_btn_add'] = 'Daftar Baru';

$lang['modal_search_result_pasien'] = 'Hasil Pencarian Pasien';