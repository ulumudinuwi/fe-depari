<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 12/5/16
 * Time: 14:33
 */

use Zend\Barcode\Barcode;

class CIBarcode
{
    public function generate($text)
    {
        $barcode = Barcode::render(
            'code128',
            'image',
            [
                'text' => $text,
                'font' => 1,
            ]
        );

        return imagejpeg($barcode);
    }

    public function barcode_image($text)
    {
        $barcode_options = [
            'text' => $text
        ];

        $render_options = [];
        $renderer = Barcode::factory(
            'code39',
            'image',
            $barcode_options,
            $render_options
        );
        return $renderer;
    }

}