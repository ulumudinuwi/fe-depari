<?php

trait KeyInForeignTable {

    public function key_in_foreign_table($id, $tables, $stdkey = '') {

        if (!is_array($tables))
            $tables = array($tables);

        $sqls = array();
        foreach ($tables as $tbl => $key) {
            if (is_numeric($tbl)) {
                $tbl = $key;
                $key = $stdkey;
            }
            $sqls[] = "(SELECT COUNT(*) AS cnt FROM `$tbl` WHERE `$key` = $id)\n";
        }

        $sql = "SELECT sum(cnt) AS counts FROM (". implode(' UNION ', $sqls).") AS counts";
		$query = $this->db->query($sql);
        $result = $query->result();

        return $result[0];
    }

}