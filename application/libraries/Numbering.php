<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 10/13/16
 * Time: 18:48
 */
class Numbering
{
//CREATE DEFINER=`root`@`localhost` FUNCTION `fn_rm`() RETURNS varchar(12) CHARSET latin1
//BEGIN
//declare punit varchar(2);
//declare ptahun varchar(2);
//declare pnomor varchar(6);
//declare phasil varchar(12);
//set punit = (select kode from setting where uid = 'cdf77e38-6868-11e6-b317-047d7bda3be5');
//set ptahun = date_format(curdate(), '%y');
//set pnomor = (SELECT LPAD((select count(*) from m_pasien) + 1,6,'0'));
//set phasil = (select concat_ws('.',`punit`,`ptahun`,`pnomor`));
//RETURN phasil;
//END ;;
//DELIMITER ;

private $db;

public function __construct()
{
    $this->CI = & get_instance();

    // Ensure our database is loaded and ready
    $this->CI->load->database();
    $this->CI->load->dbforge();
    $this->db = $this->CI->db;
}

/**
 * @Deprecated
 * Pake SELECT func_get_no_rm(KODE_UNIT);
 */
public function no_rm(){
    $ptahun = date('y');
    $pnomor =  strval($this->db->query('select count(*) as urutan from m_pasien')->result()[0]->urutan + 1);
    $punit = $this->db->query("select kode from setting where uid = 'cdf77e38-6868-11e6-b317-047d7bda3be5'")->result()[0]->kode;
    return $punit . '.' . $ptahun . '.' . $pnomor;

}

/**
 * @Deprecated
 * Pake SELECT func_get_no_rm(KODE_UNIT);
 */
public function no_rm_igd(){
    $ptahun = date('y');
    $pnomor =  strval($this->db->query('select count(*) as urutan from m_pasien')->result()[0]->urutan + 1);
    $punit = $this->db->query("select kode from setting where uid = 'cdf77e38-6868-11e6-b317-047d7bda3be5'")->result()[0]->kode;
    return $punit . '.' . $ptahun . '.' . $pnomor;
}

/**
 * @Deprecated
 * Pake SELECT func_get_no_rm(KODE_UNIT);
 */
public function no_rm_ods_odc(){
    $ptahun = date('y');
    $pnomor =  strval($this->db->query('select count(*) as urutan from m_pasien')->result()[0]->urutan + 1);
    $punit = $this->db->query("select kode from setting where uid = 'cdf77e38-6868-11e6-b317-047d7bda3be5'")->result()[0]->kode;
    return $punit . '.' . $ptahun . '.' . $pnomor;
}



}