<?php

/**
 * Part of CI PHPUnit Test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 * 
 * @property Faker\Faker $faker
 * 
 */
class Seeder {

	private $CI;
	protected $db;
	protected $dbforge;
	protected $faker;

	public function __construct()
	{
		$this->CI = & get_instance();

		// Ensure our database is loaded and ready
		$this->CI->load->database();
		$this->CI->load->dbforge();

		// Setup some convenience variables
		$this->db = $this->CI->db;
		$this->dbforge = $this->CI->dbforge;

		$this->faker = Faker\Factory::create('id_ID');
		$this->faker->seed(1645985237);
	}

	/**
	 * Runs the database seeds. This is where the magic happens.
	 * This method MUST be overridden by the child classes.
	 */
	public function run()
	{
		
	}

	/**
	 * Run another seeder
	 * 
	 * @param string $class Seeder classname
	 */
	public function call($class)
	{
		if (empty($class))
		{
			show_error('No Seeder was specified.');
		}

		$path = APPPATH . 'database/seeds/' . str_ireplace('.php', '', $class) . '.php';
		if (!is_file($path))
		{
			show_error("Unable to find the Seed class: {$class}");
		}

		try
		{
			echo "Seeding $class ... ";
			require $path;
			$seeder = new $class();
			$seeder->run();
			unset($seeder);
			echo " Done\n";
		}
		catch (\Exception $e)
		{
			show_error($e->getMessage(), $e->getCode());
		}

		return TRUE;
	}

	public function __get($property)
	{
		return $this->CI->$property;
	}

}
