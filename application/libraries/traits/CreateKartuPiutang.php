<?php

trait CreateKartuPiutang {

    public function createKartuPiutang($transaksi, $posting = true) {
		if ($posting) {
			$aData = array();
			$aData['customer_id']			= $transaksi['customer_id'];
			$aData['transaksi_id']			= $transaksi['id'];
			$aData['transaksi_detail_id']	= $transaksi['detail_id'];
			$aData['tanggal']				= $transaksi['tanggal'];
			$aData['no_bukti']				= $transaksi['no_bukti'];
			$aData['uraian']				= $transaksi['uraian'];
			$aData['jumlah']				= $transaksi['jumlah'];
            $aData['tanggal_lunas']         = $transaksi['tanggal_lunas'];
            $aData['status']                = $transaksi['status'];
            $aData['keterangan']            = $transaksi['keterangan'];
			$aData['jenis']					= $transaksi['jenis'];
			$this->db->insert('piutang', $aData);
		}
		else {
			$this->db->where('customer_id', $transaksi['customer_id']);
			$this->db->where('id', $transaksi['transaksi_id']);
			$this->db->delete('piutang');
		}
	}

}