<style type="text/css">
	.page-container {
		/*background: url("<?= base_url('assets/img/login_cover.jpg'); ?>");*/
		background-color: #184d47;
		background-size: cover;
	}

	@media (max-width: 1024px) {
		.login-logo {
			text-align: center;
		}
	}
</style>

<form role="form" method="post" id="formlogin">
	<div class="panel panel-body login-form">
		<div class="text-center">
			<h4 class="content-group" style="color: #10575e;"><span style="border-bottom: 2px solid #2fb7c5;">Login</span></h4>
			<p class="content-group text-muted"><span>Login to access application</span></p>
		</div>
		
		<?php echo messages(); ?>

		<div class="form-group has-feedback has-feedback-left">
			<div class="form-control-feedback">
				<i class="icon-user text-muted" style="color: #2fb7c5;"></i>
			</div>
			<input type="text" class="form-control input input-rounded login-input" style="text-transform: lowercase !important;" placeholder="<?php echo lang('email'); ?>" name="email" id="email" autofocus value="<?php echo (isset($email)) ? $email : ''; ?>">
		</div>
		
		<div class="form-group has-feedback has-feedback-left mt-20">
			<div class="form-control-feedback">
				<i class="icon-lock2 text-muted" style="color: #2fb7c5;"></i>
			</div>
			<input type="password" class="form-control input-rounded login-input" style="text-transform: lowercase !important;" placeholder="<?php echo lang('password'); ?>" name="password" id="password"  value="" />
		</div>

		<div class="form-group">
			<select class="select form-control  input-rounded login-input"  id="userRole">
					<option value="Admin">Admin</option>
					<option value="Owner">Owner</option>
					<option value="Operator">Operator</option>
			</select>
		</div>

		<div class="form-group" style="margin-top: 10px;">
			<button  name="login-button" type="button" class="btn btn-rounded btn-block login-input login-button" style="background-color: #f58634; color: white;"><i class="icon-key position-left"></i><?php echo lang('login'); ?> </button>
		</div>

	</div>
</form>

<script>
//Halaman Login
var apiDataUrl = 'https://deparifresh.com/';
$('.login-button').click(function(){    
	$('#formlogin').submit();
});

$('#formlogin').validate({
	rules: {
		email : {
			required: true,
			email: true,
		},
		password : {
			required : true,
			maxlength : 100
		},
	},
	messages : {
		email : {
			required: "Email Tidak Boleh Kosong",
		},
		password : {
			required : "Error! Password cannot be empty",
			maxlength : "Error! max 100 character"
		},
	},
	submitHandler: function(form) {
        var email = $('#email').val();
        var password = $('#password').val();
        var role = $('#userRole').val();
        if (role == 'Admin') {
            var dataconto = {
                        "email": email,
                        "password": password,
                        "client_id": "mAolqyKtgC_lF2bvRtWoCSYGqbCubLu2dl2vZhhR7KU",
                        "client_secret": "Wywd4Sd5EGbE_VZ4r9mwTo3a3n1eapUhuz1I0GH4nO4",
                        "grant_type": "password",
                        "scope": "admin"
                        };
        } else {
            var dataconto = {
                        "email": email,
                        "password": password,
                        "client_id": "mAolqyKtgC_lF2bvRtWoCSYGqbCubLu2dl2vZhhR7KU",
                        "client_secret": "Wywd4Sd5EGbE_VZ4r9mwTo3a3n1eapUhuz1I0GH4nO4",
                        "grant_type": "password",
                        };
        }
		$.ajax({
            headers:{
                "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
                "Content-Type":"application/json"
            },
            type: "POST",
            crossDomain:true,
			url : apiDataUrl+"oauth/token",
            dataType:"json",
			data: JSON.stringify(dataconto),
			cache: false,
            processData: false,
			success: function(data) {
                console.log(data);
                var savetoken = {
                        "email": data.email,
                        "role": data.role,
                        "token": data.access_token,
                        "sup_id": data.supplier ? data.supplier.id : ''
                        };
                swal("Success!", "Login Succesfully", {
                    icon : "success",
                    buttons: {
                        confirm: {
                            className : 'btn btn-success'
                        }
                    },
                }).then(function(){
                    $('.modal').modal('hide');
                });
                // $.ajax({
                //     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                //      'Accept':"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"},
                //     type: "POST",
                //     crossDomain:true,
                //     url : "login/login_success",
                //     contentType: 'application/json',
                //     dataType:"json",
                //     data: JSON.stringify(savetoken),
                //     cache: false,
                //     processData: false,
                //     success: function(data) {
                //             setTimeout(function() {
                //                 // window.location.href = "<?php echo base_url()?>";
                //             }, 1000);
                //     }
                // });
                $.ajax('login/login_success', {
                    type: 'POST',  // http method
                    data: savetoken,  // data to submit
                    success: function (data, status, xhr) {
                        window.location.href = "<?php echo base_url()?>";
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        console.log('Error' + errorMessage);
                    }
                });
			},
			error: function(error) {
                console.log(error);
                swal("Failed!", error.responseJSON['messages'], {
                    icon : "error",
                    buttons: {
                        confirm: {
                            className : 'btn btn-danger'
                        }
                    },
                }).then(function(){
                    $('.modal').modal('hide');
                });
            }
		});
		return false;
	}
});
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-23581568-13');
</script>