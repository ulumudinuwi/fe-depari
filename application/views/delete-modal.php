
<div id="delete-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3 class="modal-title"><?php echo lang('confirmation'); ?></h3>
			</div>

			<div class="modal-body">
				<p><?php echo lang('are_you_sure'); ?></p>
			</div>

			<div class="modal-footer">
				<a id="delete-modal-continue" class="btn btn-danger btn-labeled"><b><i class="fa fa-trash-o"></i></b><?php echo lang('continue'); ?></a>
				<button id="delete-modal-cancel" type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('back'); ?></button>
			</div>
		</div>
	</div>
</div>
<script>
$('body').on('click', '[data-button=delete]', function(e) {
	$('#delete-modal-continue').attr('href', $(this).attr('href'));
	$('#delete-modal').modal('show');
	e.preventDefault();
});
</script>