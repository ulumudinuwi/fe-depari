<style>
	.form-group {
		margin-bottom: 2px;
	}
</style>
<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-white">
		<div class="panel-heading">
			<h5 class="panel-title">Kategori</h5>
			<div class="heading-elements" style="right: 20px;">
				<div class="heading-btn">
					<button class="btn btn-success btn-labeled" type="submit" id="simpan_1_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
					<button class="btn btn-default" type="button" id="batal_1_button"> Batal </button>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label class="col-md-4 control-label ">Nama</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="name" id="name" maxlength="25" autocomplete="off" placeholder="Nama...">
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<div class="heading-btn pull-right">
						<button class="btn-success btn-labeled btn" type="submit" id="simpan_2_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
						<button class="btn btn-default" type="button" id="batal_2_button"> Batal </button>
					</div>
				</div>
			</div>
		</div>
		<div>
			<input type="hidden" id="id" name="id" disabled="disabled" />
		</div>
	</div>
</form>
