<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $template['metas']; ?>

        <title><?php echo $template['title']; ?></title>

        <link rel="icon" href="<?php echo assets_url('img/logo.png') ?>" type="image/png">

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo css_url('icons/icomoon/styles.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('icons/webfont-medical-icons/wfmi-style.css'); ?>" rel="stylesheet">
        <link href="<?php echo css_url('bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('core.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('components.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('colors.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('vmt.custom.css') ?>" rel="stylesheet">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/forms/validation/validate.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/notifications/sweetalert.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo js_url('core/app.min.js') ?>"></script>
        <!-- /core JS files -->

        <script type="text/javascript" src="<?php echo js_url('core/app.js') ?>"></script>
        <!-- /theme JS files -->

    </head>
	
    <body class="login-container">
        <!-- Page container -->
        <div class="page-container">
            <div class="login-logo">
                <div class="col-md-offset-1">
                    <!-- <img src="<?= base_url(get_option('rs_logo')) ?>" width="200" /> -->
                     <?php 
                        $appname = $this->config->item('app_name');
                        $appversion = $this->config->item('app_version');
                    ?>
                    <a class="navbar-brand" href="<?php echo site_url() ?>">
                        <img src="<?php echo assets_url('img/logo.png') ?>" style="margin-top: -20px;height:50px">
                        <!-- <span style="font-size: 20px;color: #000;position: relative;top: -30px;left: 50px;">
                            <?= $appname.' '.$appversion; ?>
                        </span> -->
                    </a>
                </div>
            </div>

            <div class="content mt-20">
                <div class="row">
                    <div class="col-md-12 mt-20">
    					<?php echo $template['content']; ?>
                    </div>
                </div>

                <div class="footer text-white text-center">
                    &copy; <?php echo date('Y'); ?> D'epari Fresh.
                </div>
            </div>
		</div>
		<!-- /page container -->

    </body>
</html>
