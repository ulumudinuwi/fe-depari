<style>
	.form-group {
		margin-bottom: 2px;
	}
</style>
<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-white">
		<div class="panel-heading">
			<h5 class="panel-title">Detail Order</h5>
			<div class="heading-elements" style="right: 20px;">
				<div class="heading-btn">
					<!-- <button class="btn btn-success btn-labeled" type="submit" id="simpan_1_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button> -->
					<button class="btn btn-default" type="button" id="batal_1_button"> Batal </button>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Customer Name</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="recepient_name" id="recepient_name" maxlength="25" autocomplete="off" placeholder="Nama ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label ">Order Code</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="order_code" id="order_code" maxlength="25" autocomplete="off" placeholder="Order Code ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Supplier</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="supplier" id="supplier" maxlength="25" autocomplete="off" placeholder="Supplier ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Status</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="status" id="status" maxlength="25" autocomplete="off" placeholder="Status ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Payment Method</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="payment_method" id="payment_method" maxlength="25" autocomplete="off" placeholder="Payment Method ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Payment Provider</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="payment_provider" id="payment_provider" maxlength="25" autocomplete="off" placeholder="Payment Provider ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Payment Status</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="payment_status" id="payment_status" maxlength="25" autocomplete="off" placeholder="Payment Status ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Invoice Code</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="invoice_code" id="invoice_code" maxlength="25" autocomplete="off" placeholder="Invoice Code ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Delivery Note Code</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="delivery_note_code" id="delivery_note_code" maxlength="25" autocomplete="off" placeholder="Delivery Note Code ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Invoice Recap Code</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="invoice_recap_code" id="invoice_recap_code" maxlength="25" autocomplete="off" placeholder="Invoice Recap ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Payment Number</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="payment_numbers" id="payment_numbers" maxlength="25" autocomplete="off" placeholder="Payment Number ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Note</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="note" id="note" maxlength="25" autocomplete="off" placeholder="Note ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Report Url</label>
						<div class="col-md-8">
							<a href="#" class="btn btn-info"  id="pdf_url" target="_blank"><b><i class="icon-eye"></i></b> Lihat File</a>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Amount</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="amount" id="amount" maxlength="25" autocomplete="off" placeholder="Amount ...">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="table-responsive">
						<h3>Daftar Produk</h3>
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr class="bg-slate">
									<th class="text-center">Nama Product</th>
									<th class="text-center">Harga (Rp)</th>
									<th class="text-center">Qty</th>
									<th class="text-center">Total (Rp)</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="heading-elements">
				<div class="heading-btn pull-right">
					<!-- <button class="btn-success btn-labeled btn" type="submit" id="simpan_2_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button> -->
					<button class="btn btn-default" type="button" id="batal_2_button"> Batal </button>
				</div>
			</div>
		</div>
		</div>
		<div>
			<input type="hidden" id="id" name="id" disabled="disabled" />
		</div>
	</div>
</form>
