<style>
	.datatable-header, 
	.datatable-footer {
		padding: 10px 20px 0 20px;
	}
	.dataTables_filter {
		margin: 0 0 10px 20px;
	}
	.dataTables_length {
		margin: 0 0 10px 20px;
	}
	.dataTables_info {
		margin-bottom: 0;
	}
	.dataTables_paginate {
		padding-top: 0 !important;
		margin: 0 0 10px 20px;
	}
</style>
<?php echo messages(); ?>
<div class="panel panel-white">
	<div class="panel-heading">
		<h5 class="panel-title">Order</h5>
	</div>
	<div class="panel-body" style="padding:0">
		<button type="button" id="btn-refresh" class="btn bg-slate btn-float btn-rounded">
			<b><i class="icon-database-refresh"></i></b>
		</button>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="table">
			<thead>
				<tr class="bg-slate">
					<th class="text-center">Order Code</th>
					<th class="text-center">Payment Status</th>
					<th class="text-center">Status</th>
					<th class="text-center">Amount</th>
					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
        	<input type="hidden" name="id" id="id">
        	<input type="hidden" name="type" id="type">
          	<button type="button" class="btn btn-info" id="btnProses">Simpan</button>
          	<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>