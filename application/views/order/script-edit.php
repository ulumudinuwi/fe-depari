<script>
	let UID = "<?php echo $uid; ?>",
		form = '#form';

	var url = {
		index: "<?php echo site_url('order'); ?>",
		save : apiDataUrl+'api/admin/orders',
		getData: apiDataUrl+'api/admin/orders/',
	};

	function fillForm(uid) {
		blockElement($(form));
		$.ajax({
	        headers:{
	            "Authorization":token,
	            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
	        },
            url: url.getData+UID,
	        success: function(data,json,xhr) {
	        	if (data.error == false) {
		            data = data.data;

					$('#recepient_name').val(data.user.name);
					$('#order_code').val(data.order_code);
					$('#supplier').val(data.supplier.name);
					$('#status').val(data.status);
					$('#payment_provider').val(data.payment_provider);
					$('#payment_method').val(data.payment_method);
					$('#payment_status').val(data.payment_status ? data.payment_status : '-');
					$('#invoice_code').val(data.invoice_code);
					$('#delivery_note_code').val(data.delivery_note_code);
					$('#invoice_recap_code').val(data.invoice_recap_code ? data.invoice_recap_code : '-');
					$('#payment_numbers').val(data.payment_numbers ? data.payment_numbers : "-");
					$('#note').val(data.note ? data.note : '-');
					$('#pdf_url').attr('href', data.pdf_url);
					$('#amount').val(data.amount);
					if (data.order_products.length > 0) {
						for (var i = 0; i < data.order_products.length; i++) {
							addProduct(data.order_products[i], i);
						}
					}else{
						$('#table').find('tbody').append('<tr><td colspan="4" class="text-center">Tidak Ada Data</td></tr>');
					}
	        	}

				$(form).unblock();
	        }
	    });
	}

	function addProduct(data) {
		var tot = data.quantity * data.price;
		var tmp = "<tr>"
					+"<td>"+data.product.name+"</td>"
					+"<td class='text-right'>"+numeral(data.price).format('0.0,')+"</td>"
					+"<td class='text-center'>"+data.quantity+"</td>"
					+"<td class='text-right'>"+numeral(tot).format('0.0,')+"</td>"
				  +"<tr>"


		$('#table').find('tbody').append(tmp);
	}
	$('#batal_1_button, #batal_2_button').on('click', function() {
		window.location = url.index;
	});

	$(document).ready(function() {
		$(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
		$(".wysihtml5-min").wysihtml5({
		    "action": false,
		    "color": false,
		    "html": false,
		    "image": false,
		    "link": false,
		});

		$('.btn-save').on('click', function(e) {
			e.preventDefault();
			$(form).submit();
		});

		$(".btn-batal").click(function() {
			window.location.assign(url.index);
		});

		$(form).validate({
			rules: {
				name: { required: true, minlength: 1 },
				username: { required: true, minlength: 1 },
				email: { required: true, minlength: 1 },
				phone_number: { required: true, minlength: 1 },
			},
			focusInvalid: true,
			errorPlacement: function(error, element) {
				var placement = $(element).closest('.input-group');
				if (placement.length > 0) {
					error.insertAfter(placement);
				} else {
					error.insertAfter($(element));
				}
			},
			submitHandler: function (form) {
				swal({
					title: "Konfirmasi?",
					type: "warning",
					text: "Apakah data yang dimasukan telah benar??",
					showCancelButton: true,
					confirmButtonText: "Ya",
					confirmButtonColor: "#2196F3",
					cancelButtonText: "Batal",
					cancelButtonColor: "#FAFAFA",
					closeOnConfirm: true,
					showLoaderOnConfirm: true,
				},
				function() {
					$('.input-decimal').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					blockElement($(form));
					var formData = $(form).serialize();
					var uri = url.save; 
					var metod = 'POST'; 
					if (UID) {
						uri = url.save+'/'+UID; 
						metod = 'PUT'; 
					}
					$.ajax({
				        headers:{
				            "Authorization":token,
				            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				        },
						data: formData,
						type: metod,
						dataType: 'JSON', 
						url: uri,
						success: function(data){
							$(form).unblock();
							if (data.error == true) {
								console.log();
								$.each( data.messages, function( key, value ) {
									errorMessage('Peringatan', value);
								});
								return false;
							}
							successMessage('Berhasil', "Data berhasil disimpan.");
							window.location.assign(url.index);
						},
						error: function(data){
							$(form).unblock();
							errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
						}
					});
					return false;
				});
			}
		});

		if(UID !== "") {
			fillForm(UID);
		}
	});
</script>