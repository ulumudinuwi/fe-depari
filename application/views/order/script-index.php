<script type="text/javascript">
	var url_load_data = apiDataUrl+'api/admin/orders';
	var url_load_closeorder = apiDataUrl+'api/admin/orders/close';
	var url_load_status = apiDataUrl+'api/admin/orders/change-payment-status';
	var url_load_method = apiDataUrl+'api/admin/orders/change-payment';
	var url_edit = '<?php echo site_url("order/edit"); ?>';
</script>
<script type="text/javascript" src="<?php echo script_url('assets/js/pages/scripts/order/index.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		Index.init();
	});
</script>