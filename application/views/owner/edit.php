<style>
	.form-group {
		margin-bottom: 2px;
	}
</style>
<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-white">
		<div class="panel-heading">
			<h5 class="panel-title">Owner</h5>
			<div class="heading-elements" style="right: 20px;">
				<div class="heading-btn">
					<button class="btn btn-success btn-labeled" type="submit" id="simpan_1_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
					<button class="btn btn-default" type="button" id="batal_1_button"> Batal </button>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Nama Owner</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="name" id="name" maxlength="25" autocomplete="off" placeholder="Nama ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label ">No. KTP</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="identity_number" id="identity_number" maxlength="25" autocomplete="off" placeholder="No. KTP ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Email</label>
						<div class="col-md-8">
							<input type="email" class="form-control" name="email" id="email" maxlength="25" autocomplete="off" placeholder="Email ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">No. Handphone</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="phone_number" id="phone_number" maxlength="25" autocomplete="off" placeholder="Harga ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Password</label>
						<div class="col-md-8">
							<input type="password" class="form-control" name="password" id="password" maxlength="25" autocomplete="off" placeholder="Password ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label ">Password Confirmation</label>
						<div class="col-md-8">
							<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" maxlength="25" autocomplete="off" placeholder="Password Confirmation ...">
						</div>
					</div>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<div class="heading-btn pull-right">
						<button class="btn-success btn-labeled btn" type="submit" id="simpan_2_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
						<button class="btn btn-default" type="button" id="batal_2_button"> Batal </button>
					</div>
				</div>
			</div>
		</div>
		<div>
			<input type="hidden" id="id" name="id" disabled="disabled" />
		</div>
	</div>
</form>
