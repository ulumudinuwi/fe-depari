<script type="text/javascript">
	var url_load_data = apiDataUrl+'api/admin/owners';
	var url_load_deactive = apiDataUrl+'api/admin/owners/deactive';
	var url_load_active = apiDataUrl+'api/admin/owners/verify';
	var url_edit = '<?php echo site_url("owner/edit"); ?>';
</script>
<script type="text/javascript" src="<?php echo script_url('assets/js/pages/scripts/owner/index.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		Index.init();
	});
</script>