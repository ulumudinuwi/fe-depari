<style>
	.form-group {
		margin-bottom: 2px;
	}
</style>
<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-white">
		<div class="panel-heading">
			<h5 class="panel-title">Produk</h5>
			<div class="heading-elements" style="right: 20px;">
				<div class="heading-btn">
					<button class="btn btn-success btn-labeled" type="submit" id="simpan_1_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
					<button class="btn btn-default" type="button" id="batal_1_button"> Batal </button>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-2 control-label input-required">Nama</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="name" id="name" maxlength="25" autocomplete="off" placeholder="Nama ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label ">Kategori</label>
						<div class="col-md-10">
                            <select name="category_id" id="category_id">
                            	<option value="">- Pilih Kategori -</option>
                            </select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label input-required">Deskripsi</label>
						<div class="col-md-10">
							<textarea type="text" class="form-control" name="description" id="description" maxlength="25" autocomplete="off" placeholder="Deskripsi ..."> </textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-2 control-label input-required">Harga</label>
						<div class="col-md-10">
							<input type="text" class="form-control input-decimal" name="price" id="price" maxlength="25" autocomplete="off" placeholder="Harga ...">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label input-required">Diskon</label>
						<div class="col-md-10">
							<div class="input-group">
								<input type="text" class="form-control input-decimal" name="discount" id="discount" maxlength="25" autocomplete="off" placeholder="Diskon ...">
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label ">Stok</label>
						<div class="col-md-7">
							<input type="text" class="form-control input-decimal" name="stock" id="stock" maxlength="25" autocomplete="off" placeholder="Stok ...">
						</div>
						<div class="col-md-3">
                            <select name="unit" id="unit">
                            	<option value="ton">TON</option>
                            	<option value="pack">Pack</option>
                            </select>
                        </div>
					</div>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<div class="heading-btn pull-right">
						<button class="btn-success btn-labeled btn" type="submit" id="simpan_2_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
						<button class="btn btn-default" type="button" id="batal_2_button"> Batal </button>
					</div>
				</div>
			</div>
		</div>
		<div>
			<input type="hidden" id="id" name="id" disabled="disabled" />
		</div>
	</div>
</form>
