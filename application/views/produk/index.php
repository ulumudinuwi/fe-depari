<style>
	.datatable-header, 
	.datatable-footer {
		padding: 10px 20px 0 20px;
	}
	.dataTables_filter {
		margin: 0 0 10px 20px;
	}
	.dataTables_length {
		margin: 0 0 10px 20px;
	}
	.dataTables_info {
		margin-bottom: 0;
	}
	.dataTables_paginate {
		padding-top: 0 !important;
		margin: 0 0 10px 20px;
	}
</style>
<?php echo messages(); ?>
<div class="panel panel-white">
	<div class="panel-heading">
		<h5 class="panel-title">Produk</h5>
	</div>
	<div class="panel-body" style="padding:0">
		<button type="button" id="btn-refresh" class="btn bg-slate btn-float btn-rounded">
			<b><i class="icon-database-refresh"></i></b>
		</button>
	</div>
	<div class="table-responsive uppercase">
		<table class="table table-bordered table-striped" id="produk_table">
			<thead>
				<tr class="bg-slate">
					<th class="text-center">Name</th>
					<th class="text-center">Category</th>
					<th class="text-center">Harga (Rp.)</th>
					<th class="text-center" style="width: 15%;">Stock</th>
					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>