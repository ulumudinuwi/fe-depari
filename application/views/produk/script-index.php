<script type="text/javascript">
	var url_load_data = apiDataUrl+'api/admin/products';
	var url_load_blok = apiDataUrl+'api/admin/products/block';
	var url_load_unblok = apiDataUrl+'api/admin/products/unblock';
	$('.btn_add').addClass('hide');
	if (role == 'Operator') {
		url_load_data = apiDataUrl+'api/v1/suppliers/'+sup_id+'/products';
		url_load_blok = apiDataUrl+'api/v1/suppliers/'+sup_id+'/products/block';
		url_load_unblok = apiDataUrl+'api/v1/suppliers/'+sup_id+'/products/unblock';
		url_load_delete = apiDataUrl+'api/v1/suppliers/'+sup_id+'/products';
		$('.btn_add').removeClass('hide');
	}
	var url_edit = '<?php echo site_url("produk/edit"); ?>';
</script>
<script type="text/javascript" src="<?php echo script_url('assets/js/pages/scripts/produk/index.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		Index.init();
	});
</script>