<?php echo messages(); ?>
<style type="text/css">
    .btn-refresh {
        position: fixed;
        bottom: 10%;
        right: 20px;
        z-index: 10000;
    }
    #table_database_filter,
    #table_database_length,
    #table_database_paginate,
    #table_database_info {
        display: none;
    }
</style>
<div class="content">
    <ul class="nav nav-tabs nav-tabs-bottom">
        <li class="active">
            <a href="#tab-database" data-toggle="tab">
                <i class="icon-database"></i>&nbsp;&nbsp;
                Database
            </a>
        </li>
    </ul>

    <div class="tabbale">
    
        <div class="tab-content">
            <div class="tab-pane active" id="tab-database">
                <button type="button" id="btn-refresh-database" class="btn bg-slate btn-float btn-rounded btn-refresh">
                    <b>
                        <i class="icon-database-refresh"></i>
                    </b>
                </button>
                <div class="panel panel-flat">
                    <div class="panel-body uppercase">
                        <div class="no-padding-top">
                            <div class="table-responsive">
                                <table id="table_database" class="table table-bordered table-striped">
                                    <thead>
                                        <tr class="bg-slate">
                                            <td class="text-center"></td>
                                            <td class="text-center">Unit</td>
                                            <td class="text-center">Hostname</td>
                                            <td class="text-center">Status</td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>

<script>
$("select").select2();
$("[data-popup=tooltip]").tooltip();
// Style form components
$('.styled').uniform();
var firstActiveTab = $('a[data-toggle=tab]').first();
firstActiveTab.closest('li').addClass('active');
$(firstActiveTab.data('href')).addClass('active');
</script>