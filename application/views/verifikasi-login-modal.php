<div id="modal-verifikasi_login" class="modal fade" data-backdrop="static">
    <div class="modal-dialog modal-xs">
        <div class="form-horizontal">
            <form class="panel panel-body login-form">
            	<div class="panel-body">
					<div class="text-center">
						<h6 class="content-group"><span style="border-bottom: 2px solid #2fb7c5;" id="label-verifikasi_login">Verifikasi</span></h6>
					</div>

					<div class="form-group has-feedback has-feedback-left">
						<input type="text" class="form-control input input-rounded" placeholder="Username ..." id="username-verifikasi_login" autofocus required>
						<div class="form-control-feedback">
							<i class="icon-user text-muted"></i>
						</div>
					</div>
					
					<div class="form-group has-feedback has-feedback-left mt-20">
						<input type="password" class="form-control input-rounded" placeholder="Password ..." id="password-verifikasi_login" required />
						<div class="form-control-feedback">
							<i class="icon-lock2 text-muted"></i>
						</div>
					</div>

					<div class="form-group" style="margin-top: 20px;">
						<input type="hidden" id="pegawai-verifikasi_login">
						<button type="submit" id="btn-verifikasi_login" class="btn btn-rounded btn-block" style="background-color: #f58634; color: white;"><i class="icon-key position-left"></i>Verifikasi</button>
					</div>
            	</div>
			</form>
		</div>
	</div>
</div>
