module.exports = function (grunt) {
    grunt.initConfig({
        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        cwd: 'bower_components',
                        src: [
                            'angular/**/*.min.js',
                            'angular-animate/**/*.min.js',
                            'angular-bootstrap/**/*.min.js',
                            'angular-datatables/**/*.min.js',
                            'angular-leaflet-directive/**/*.min.js',
                            'angular-resource/**/*.min.js',
                            'angular-toastr/**/*.min.js',
                            'angular-ui-select2/**/*.js',
                            'leaflet/**/*.js',
                            'lodash/**/*.js',
                            'ngMask/**/*.min.js',
                            'ng-tags-input/*.min.js'
                        ],
                        dest: 'js/libs'
                    },
                    // includes files within path and its sub-directories
                ],
            },
        },
        // watch: {
        //     modules: {
        //         files: ['bower_components/**'],
        //         tasks: ['copy']
        //     }
        // }
    });
    grunt.loadNpmTasks('grunt-contrib-copy');
    // grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('rsbt:dev', ['copy']);
    grunt.registerTask('default', ['copy']);
};