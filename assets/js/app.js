var CONNECTION_LOST_TIMEOUT = null;
var PING_SESSION = null;
var NOTIFY_CONNECTION_LOST = null

function blockPage(message) {
    message = message || 'Loading...';
    $.blockUI({
        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;' + message + '</span>',
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.6,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#fff',
            '-webkit-border-radius': 2,
            '-moz-border-radius': 2,
            backgroundColor: '#333',
            'z-index' : 2000
        }
    });
}

function blockElement(elSelector) {
    $(elSelector).block({
       message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;Loading...</span>',
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.6,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#fff',
            '-webkit-border-radius': 2,
            '-moz-border-radius': 2,
            backgroundColor: '#333',
            'z-index' : 2000
        } 
    });
}

function blockPanel(block) {
    $(block).block({ 
        message: '<i class="icon-spinner2 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait',
            'box-shadow': '0 0 0 1px #ddd'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });
}
function unblockPanel(block) {
    window.setTimeout(function () {
       $(block).unblock();
    }, 1000); 
}

function calculateAge(birthdate) {
    let year = 0;
    let month = 0;
    let day = 0;

    if (birthdate !== '') {
        let today = moment();
        let dob = moment(birthdate);
        
        year = today.diff(dob, 'years');
        dob.add(year, 'years');
        
        month = today.diff(dob, 'months');
        dob.add(month, 'months');
        
        day = today.diff(dob, 'days');
    }

    return {year: year, month: month, day: day};
}

// $(document).ajaxStart(blockElement('.table-with-load')).ajaxStop($.unblockUI);

// $(document).ajaxStart(blockPage).ajaxStop($.unblockUI);

numeral.language('id', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'ribu',
        million: 'juta',
        billion: 'milyar',
        trillion: 'triliun'
    },
    currency: {
        symbol: 'Rp.'
    }
});
numeral.language('id');
	
// Data Tables - Config
(function ($) {

    'use strict';

    $.fn.getDropBoxApi = function (urlStr, idStr) {
        return $.ajax({
            url: urlStr,
            type: 'GET',
            dataType: 'json',
            success: function (json) {

                $('#' + idStr).append($('<option>').text('').attr('value', ''));
                $.each(json.result, function (i, value) {
                    $('#' + idStr).append($('<option>').text(value.nama).attr('value', value.uid));
                });
            }
        });
    }

    // we overwrite initialize of all datatables here
    // because we want to use select2, give search input a bootstrap look
    // keep in mind if you overwrite this fnInitComplete somewhere,
    // you should run the code inside this function to keep functionality.
    //
    // there's no better way to do this at this time :(
    if ($.isFunction($.fn[ 'dataTable' ])) {
        $.extend(true, $.fn.dataTable.defaults, {
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Cari:</span> _INPUT_',
                lengthMenu: '<span>Jumlah per halaman:</span> _MENU_',
                processing: '<i class="fa fa-spinner fa-spin"></i> Loading',
                paginate: {'first': 'Awal', 'last': 'Akhir', 'next': '&rarr;', 'previous': '&larr;'},
                emptyTable: "Tidak ada data yang dapat ditampilkan.",
                info: "Menampilkan halaman _PAGE_ dari _PAGES_",
                infoEmpty: "Tidak ada data yang dapat ditampilkan",
                infoFiltered: " - difilter dari _MAX_ data",
                zeroRecords: "Tidak ada data yang dapat ditampilkan."
            },
            autoWidth: false
        });

    }

}).apply(this, [jQuery]);

// String
function toTitleCase(str) {
    return str.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
}

// Random ID
function makeid(len) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

jQuery(function ($) {        
  $('form').bind('submit', function () {
    $(this).find('select').prop('disabled', false);
  });
});

/**
 * dob => Tanggal Lahir (Moment)
 * tanggal => Tanggal yang digunakan (Moment) (Default Moment())
 * format => Boolean jika TRUE mengembalikan dalam bentuk string, Object jika FALSE (DEFAULT TRUE)
 */
function getAge(dob, tanggal, format) {
    tanggal = typeof(tanggal) === 'undefined' ? moment() : tanggal;
    format = typeof(format) === 'undefined' ? true : format;

    if (! dob._isAMomentObject) {
        console.error('`dob` must be type of Moment Object');
        return;
    }

    if (! tanggal._isAMomentObject) {
        console.error('`tanggal` must be type of Moment Object');
    }

    var tahun = tanggal.diff(dob, 'years');
    var bulan = tanggal.diff(dob, 'months') % 12;
    var hari = tanggal.diff(dob, 'days');
    
    if (format) {
        var str;
        switch (true) {
            case tahun > 0 && bulan > 0:
                str = tahun + ' Thn ' + bulan + ' Bln ';
                break;
            case tahun > 0:
                str = tahun + ' Thn ';
            case bulan > 0:
                str = bulan + ' Bln ';
                break;
            case tahun == 0 && bulan == 0:
                str = hari + ' Hari ';
                break;
        }

        return str;
    } else {
        return {
            tahun: tahun,
            bulan: bulan,
            hari: hari
        };
    }
}

function getJenisKelamin(val) {
    if (parseInt(val) == 1) {
        return 'Laki-Laki';
    }
    return 'Perempuan';
}

function successMessage(title, message) {
    if (typeof(PNotify) != "undefined") {
        var notify = new PNotify({
            title: title,
            text: message,
            addclass: 'alert alert-success alert-styled-right',
            type: 'success',
            hide: true,
            buttons: {
                closer: false,
                sticker: false
            }
        });

        notify.get().click(function() {
            notify.remove();
        });
    } else if (typeof(swal) != "undefined") {
        swal(title, message, "success");
    } else {
        alert(title + "\r\n" + message);
    }
}

function errorMessage(title, message) {
    if (typeof(PNotify) != "undefined") {
        var notify = new PNotify({
            title: title,
            text: message,
            addclass: 'alert alert-danger alert-styled-right',
            type: 'error',
            hide: true,
            buttons: {
                closer: false,
                sticker: false
            }
        });

        notify.get().click(function() {
            notify.remove();
        });
    } else if (typeof(swal) != "undefined") {
        swal(title, message, "error");
    } else {
        alert(title + "\r\n" + message);
    }
}

function warningMessage(title, message) {
    if (typeof(PNotify) != "undefined") {
        var notify = new PNotify({
            title: title,
            text: message,
            addclass: 'alert alert-warning alert-styled-right',
            type: 'warning',
            hide: true,
            buttons: {
                closer: false,
                sticker: false
            }
        });

        notify.get().click(function() {
            notify.remove();
        });
    } else if (typeof(swal) != "undefined") {
        swal(title, message, "warning");
    } else {
        alert(title + "\r\n" + message);
    }
}

function server_ping()
{
    $.ajax({
        url: window.location,
        type: "POST",
        success: function (data, status) {
            clearTimeout(PING_SESSION);
            NOTIFY_CONNECTION_LOST = null;
            PNotify.removeAll();
            $.unblockUI();
            $('.modal').modal('hide');

            new PNotify({
                title: 'Koneksi telah terhubung',
                text: 'Telah terhubung kembali dengan server.',
                addclass: 'alert alert-success alert-styled-right',
                type: 'success',
            });
        }, 
        error: function (error) {
            clearTimeout(PING_SESSION);
            PING_SESSION = setTimeout(server_ping, 1000);
        }
    });
}

function getLamaHari(tanggal, today) {
    today = today || moment().format('YYYY-MM-DD');

    var mTanggal = moment(tanggal);
    var mToday = moment(today);
    return (mToday.diff(mTanggal, 'days') + 1) + ' Hari';
}

function getUmurPasien(tanggal, today) {
    today = today || moment().format('YYYY-MM-DD');

    var mTanggal = moment(tanggal);
    var mToday = moment(today);

    return Math.floor(mToday.diff(mTanggal, 'years')) + ' Thn';
}

function getDate(date) {
    var day = date.substr(0, 2);
    var month = date.substr(3, 2);
    var year = date.substr(6, 4);

    var newDate = year + "-" + month + "-" + day;
    return newDate;
}

$(document).ready(function () {
    $(document).ajaxError(function( event, request, settings ) {
        //When XHR Status code is 0 there is no connection with the server
        clearTimeout(CONNECTION_LOST_TIMEOUT);
        CONNECTION_LOST_TIMEOUT = setTimeout(function () {
            if (request.status == 0 && NOTIFY_CONNECTION_LOST == null){ 
                PNotify.removeAll();
                NOTIFY_CONNECTION_LOST = new PNotify({
                    title: 'Koneksi Terputus.',
                    text: 'Koneksi dengan server telah terputus.<br> silahkan refresh halaman atau panggil administrator yang ada.',
                    addclass: 'alert alert-danger alert-styled-right',
                    type: 'error',
                    hide: false,
                    width: 420,
                    html: true,
                    confirm: {
                        confirm: true,
                        buttons: [
                            {
                                text: 'Refresh Halaman',
                                addClass: 'btn btn-sm btn-primary',
                                click: function(notice) {
                                    window.location.assign(window.location);
                                }
                            },
                            {
                                text: 'Refresh Halaman',
                                addClass: 'btn btn-sm btn-primary hide',
                                click: function(notice) {
                                    window.location.assign(window.location);
                                }
                            }
                        ]
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    history: {
                        history: false
                    }
                });

                PING_SESSION = setTimeout(server_ping, 3000);
            }
        }, 5000);
    });

    $(document).ajaxSuccess(function( event, request, settings ) { 
        clearTimeout(CONNECTION_LOST_TIMEOUT);
    });

    // Agar ketika menginsert link didalam suatu modal, modal tersebut tetap terbuka hehe
    $('.bootstrap-wysihtml5-insert-link-modal').on('hidden.bs.modal', function(){
        if ($('.modal.in').length > 0) {
            $('body').addClass('modal-open');
        }
    });

    // Required Symbol
    $('.required-symbol').each(function () {
        $(this).text('*');
    });

    // Jika membuka modal setelah membuka modal
    $('.modal').on('hidden.bs.modal', function (e) {
        if($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "Form diperlukan.",
        remote: "Silahkan perbaiki form ini.",
        email: "Masukan email yang valid",
        url: "Masukan email yang valid.",
        date: "Masukan tanggal yang valid.",
        dateISO: "Masukan tanggal yang valid.",
        number: "Masukan nomor yang valid.",
        digits: "Masukan hanya angka saja.",
        creditcard: "Masukan no. kartu kredit yang valid.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
});

// JSON
function json_encode(data) {
    let content;
    try {
        content = JSON.stringify(data);
    } catch (err) {
        content = null;
    }

    return content;
}

function json_decode(content) {
    let data;
    try {
        data = JSON.parse(content);
    } catch (err) {
        data = null;
    }

    return data;
}

/**
 * CONFIRMATION DIALOG
 */
function confirmDialog(config) {
    swal({
        title: config.title,
        text: config.text,
        html: true,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: config.btn_confirm,
        confirmButtonColor: "#48A74C",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    },
    function(){
        $.ajax({
            url: config.url,
            type: 'POST',
            dataType: "json",
            data: config.data,
            success: function (res) {
                if (config.onSuccess) {
                    config.onSuccess(res);
                }
            },
            error: function (error) {
                if (config.onError) {
                    config.onError(error);
                }
                swal.close();
            },
            complete: function () {
                swal.close();
            }
        });
    });
}

/**
 * BATAL DIALOG DENGAN ALASAN
 */
function batalDialog(config) {
    swal({
        title: config.title,
        text: "<textarea class='form-control' id='swal-text' style='resize: vertical;'></textarea>",
        html: true,
        showCancelButton: true,
        confirmButtonText: config.btn_confirm,
        confirmButtonColor: "#F44336",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        animation: "slide-from-top",
        inputPlaceholder: ""
    }, function(inputValue) {
        var value = $("#swal-text").val();
        if (value === false) {
            $("#swal-text").focus();
            return false;
        }
        if (value === "") {
            swal.showInputError(config.message_input_error);
            $("#swal-text").focus();
            return false
        }

        var val = $("#swal-text").val();
        if (config.callback) {
            config.callback(val, () => {
                swal.close();
            });
        }
    });
}

/**
 * Verifikasi Dialog
 */
function getModalVerifikasi() {
    let modalVerifikasi = $(".modal-verifikasi_login");

    if (modalVerifikasi.length > 0) {
        return modalVerifikasi;
    }

    modalVerifikasi = $("<div/>")
        .addClass('modal modal-verifikasi_login fade')
        .data('backdrop', 'static')
        .appendTo($('body'));
    let modalDialog = $("<div/>")
        .addClass('modal-dialog modal-xs')
        .appendTo(modalVerifikasi);

    let form = $("<form/>")
        .addClass('panel panel-body login-form')
        .appendTo(modalDialog);
    let formBody = $("<div/>")
        .addClass('panel-body')
        .appendTo(form);

    formBody.append(`
        <div class="text-center">
            <h6 class="content-group"><span style="border-bottom: 2px solid #2fb7c5;" data-content="title">Verifikasi</span></h6>
        </div>
    `);

    // Username
    let usernameFormGroup = $("<div/>")
        .addClass('form-group form-group-username has-feedback has-feedback-left')
        .appendTo(formBody);
    let usernameInput = $("<input/>")
        .prop('type', 'text')
        .prop('name', 'username')
        .addClass('form-control input input-rounded')
        .prop('placeholder', 'Username ...')
        .prop('autofocus', true)
        .prop('required', true)
        .appendTo(usernameFormGroup);
    usernameFormGroup.append(`
        <div class="form-control-feedback">
            <i class="icon-user text-muted"></i>
        </div>
    `);

    // Password
    let passwordFormGroup = $("<div/>")
        .addClass('form-group form-group-password has-feedback has-feedback-left')
        .appendTo(formBody);
    let passwordInput = $("<input/>")
        .prop('type', 'password')
        .prop('name', 'password')
        .addClass('form-control input input-rounded')
        .prop('placeholder', 'Password ...')
        .prop('required', true)
        .appendTo(passwordFormGroup);
    passwordFormGroup.append(`
        <div class="form-control-feedback">
            <i class="icon-lock2 text-muted"></i>
        </div>
    `);

    let buttonFormGroup = $("<div/>")
        .addClass('form-group form-group-button')
        .css('margin-top', '20px')
        .appendTo(formBody);
    let input_pegawai_id = $("<input/>")
        .prop('type', 'hidden')
        .prop('name', 'pegawai_id')
        .appendTo(buttonFormGroup);
    let btnSubmit = $("<button/>")
        .prop('type', 'submit')
        .addClass('btn btn-rounded btn-block btn-verifikasi')
        .css('background-color', '#f58634')
        .css('color', 'white')
        .html(`<i class="icon-key position-left"></i> <span data-content="btn_verifikasi">Verifikasi</span>`)
        .appendTo(buttonFormGroup);

    modalVerifikasi.modal();

    return modalVerifikasi;
}

function verifikasiDialog(config) {
    config = $.extend(true, {
        title: 'Verifikasi',
        data: {
            username: '',
            password: '',
            pegawai_id: 0,
        },
        label: {
            username: 'Username',
            password: 'Password',
            btn_verifikasi: 'Verifikasi',
        },
        messages: {
            username: 'Username harus diisi.',
            password: 'Password harus diisi.',
        },
        success: (result) => { 
            console.log('verifikasiDialog.success', result); 
            successMessage('Success', 'Verifikasi berhasil dilakukan.');
        },
        error: (err) => { 
            console.error('verifikasiDialog.error', err); 
            errorMessage('Error', err.responseJSON.message);
        },
    }, config);

    if (! config.url) {
        console.error('verifikasiDialog.error', 'Please specify the url bla bla bla.....');
        return;
    }

    if (! config.type) {
        console.error('verifikasiDialog.error', 'Please specify the type of verification bla bla bla.....');
        return;
    }

    let modalVerifikasi = getModalVerifikasi();

    // Input Data
    modalVerifikasi.find('[name=username]').val(config.data.username);
    modalVerifikasi.find('[name=password]').val(config.data.password);
    modalVerifikasi.find('[name=pegawai_id]').val(config.data.pegawai_id);

    // Label
    modalVerifikasi.find('[name=username]').prop('placeholder', config.label.username);
    modalVerifikasi.find('[name=password]').prop('placeholder', config.label.password);

    // On Button Click
    modalVerifikasi.find('.btn-verifikasi').click((e) => {
        e.preventDefault();

        let input_username = modalVerifikasi.find('[name=username]');
        let input_password = modalVerifikasi.find('[name=password]');
        let input_pegawai_id = modalVerifikasi.find('[name=pegawai_id]')

        let username = input_username.val();
        let password = input_password.val();

        if(username == "" && password == "") {
            input_username.focus();
            warningMessage('Peringatan !', 'Username dan Password harus diisi.');
            return;
        } else if (username == "" && password != "") {
            input_username.focus();
            warningMessage('Peringatan !', config.messages.username || 'Username harus diisi.');
            return;
        } else if (username != "" && password == "") {
            input_password.focus();
            warningMessage('Peringatan !', config.messages.password || 'Password harus diisi.');
            return;
        }

        blockElement(modalVerifikasi.find('.modal-dialog').selector);
        $.ajax({
            url: config.url,
            data: {
                username: username,
                password: btoa(JSON.stringify(password)),
                pegawai_id: input_pegawai_id.val(),
                type: config.type,
            },
            type: 'POST',
            dataType: "json",
            success: function (result) {
                if (config.success) {
                    config.success(result);
                }

                modalVerifikasi.modal('hide');
                modalVerifikasi.find('.modal-dialog').unblock();

                // input_username.val('');
                // input_password.val('');
                // input_pegawai_id.val('');

                setTimeout(() => {
                    // Destroy
                    modalVerifikasi.remove();
                }, 500);
            },
            error: function (err) {
                modalVerifikasi.modal('hide');
                modalVerifikasi.find('.modal-dialog').unblock();

                if (config.error) {
                    config.error(err);
                }

                setTimeout(() => {
                    // Destroy
                    modalVerifikasi.remove();
                }, 500);
            }
        });
    });

    modalVerifikasi.modal('show');
}
