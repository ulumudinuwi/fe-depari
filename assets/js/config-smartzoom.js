/**
 * Created by agungrizkyana on 12/7/16.
 */


var lokalis = [];


var get_position = function get_position(event) {

    var position = $('#map_image').smartZoom('position', event.pageX, event.pageY);

    $('.map_container').append($('<img />', {
        class: 'marker',
        id: 'marker' + position.y,
        src: img_url + '/marker.png'
    }));
    $('#marker' + position.y).css('position', 'absolute');
    $('#marker' + position.y).css('top', position.y - 8);
    $('#marker' + position.y).css('left', position.x - 100);


    $("#modal-lokalis", window.parent.document).modal();

    lokalis.push({
        x : position.x - 100,
        y : position.y - 8
    });



    $('#lokalis').empty();
    $('#lokalis', window.parent.document).val(JSON.stringify(lokalis));

    console.log($("#lokalis", window.parent.document).val());

};

function onHideModalLokalis(){
    var catatan = $("#catatan-lokalis", window.parent.document).val();
    var lastLokalis = _.last(lokalis);

    lastLokalis.catatan = catatan;

    console.log("lokalis now", lokalis);
}

function onShowModalLokalis(){
    $('#catatan-lokalis', window.parent.document).val(null);
}

$(document).ready(function () {
    $('#map_image').smartZoom({'maxScale': 1, 'dblClickEnabled': true});
});

$("#modal-lokalis", window.parent.document).on('hide.bs.modal', onHideModalLokalis);
$("#modal-lokalis", window.parent.document).on('show.bs.modal', onShowModalLokalis);

