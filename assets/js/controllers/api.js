var api = angular.module('rsbt.api', ['ngResource']);


api.constant('CONFIG', {
    API_VERSION: 'v1',
    BASE_URL: 'http://rsbt.api.dev/',
    SERVICE_URL: '/service/',
    TOKEN_URL: '/auth/main/token/',
    TOKEN: '123456789',

    // rawat jalan

    SEARCH_URL: base_url + '/api/rawat_jalan/pemeriksaan_awal/search?s=',
    SEARCH_PEMERIKSAAN_DOKTER: base_url + '/api/rawat_jalan/pemeriksaan_dokter/search?s=',
    SEARCH_OBAT : base_url + '/api/master/obat/non_racikan?s=',

    // igd

    SEARCH_PASIEN_IGD: base_url + '/api/igd/pemeriksaan_awal/search?s=',
    SEARCH_PEMERIKSAAN_DOKTER_IGD: base_url + '/api/igd/pemeriksaan_dokter/search?s=',

    // ods_odc
    SEARCH_PASIEN_DAY_CARE : base_url + '/api/ods_odc/pemeriksaan_awal/search?s=',
    SEARCH_PEMERIKSAAN_DAY_CARE : base_url + '/api/ods_odc/pemeriksaan_dokter/search?s=',

    SEARCH_PASIEN: base_url + '/api/master/pasien/search?s=',
    SEARCH_PEGAWAI: base_url + '/api/master/pegawai/search?s=',
    SEARCH_LAYANAN: base_url + '/api/master/layanan/search?s=',
    SEARCH_DOKTER_POLI: base_url + '/api/master/dokter_poli/search?s=',
    SEARCH_LAB: base_url + '/api/master/laboratorium/search?s=',

    VERIFIKASI_SEP : base_url + '/api/bpjs/sep/detail',
    INACBG : {
        GET_CLAIM_DATA : base_url + '/api/bpjs/inacbg/get_claim_data'
    },
    JADWAL_OPERASI : base_url + '/ok/jadwal_operasi',
    RESERVASI_JADWAL_OK : base_url + '/api/ok/jadwal_operasi/reservasi',
    RESERVASI_KUNJUNGAN_ULANG : base_url + '/api/kunjungan_ulang/reservasi',
    RESERVASI_JADWAL_IGD_OK : base_url + '/api/ok/jadwal_operasi/reservasi1',
    JENIS_OPERASI : base_url + '/api/ok/jadwal_operasi/jenis_operasi',
    RUANG_OK : base_url + '/api/ok/jadwal_operasi/ruang_ok',
    RUANG : base_url + '/api/master/ruang',
    KELAS : base_url + '/api/master/kelas',
    BED : base_url + '/api/master/bed'
});


api.factory('api', ['$resource', 'CONFIG',
    function($resource, CONFIG) {
        var completeApiUrl = CONFIG.BASE_URL + CONFIG.API_VERSION + CONFIG.SERVICE_URL;

        var _nomor = $resource(base_url + '/api/master/nomor/', {}, {
            'getNomor': {
                method: 'GET',
                url: base_url + '/api/master/nomor',
                isArray: true
            },
            'getNomorIgd' : {
                method: 'GET',
                url: base_url + '/api/master/nomor/igd',
                isArray: true
            },
            'getNomorOdsOdc' : {
                method: 'GET',
                url: base_url + '/api/master/nomor/ods_odc',
                isArray: true
            }
        });

        var _pasien = $resource(base_url + '/api/master/pasien/:id', {
            id: '@id'
        }, {
            'search': {
                method: 'GET',
                url: base_url + '/api/master/pasien/search/'
            },
        });

        var _tindakan = $resource(base_url + "/api/master/tindakan/:id", {
            id: '@id'
        }, {
            'pendaftaran': {
                method: 'GET',
                url: base_url + '/api/master/tindakan/pendaftaran/'
            },
            'tindakanForm': {
                method: 'GET',
                url: base_url + '/api/master/tindakan/form',
                isArray: true
            }
        });

        var _rawat_jalan = $resource(base_url + "/api/rawat_jalan/:id", {
            id: '@id'
        }, {
            'pemeriksaanAwal': {
                method: 'GET',
                url: base_url + '/api/rawat_jalan/pemeriksaan_awal/'
            },

            'pendaftaranSearch' : {
                method : 'GET',
                url: base_url + '/api/rawat_jalan/pendaftaran/search',
                isArray: true
            },
            'pemeriksaanAwalSave' : {
                method : 'POST',
                url: base_url + '/api/rawat_jalan/pemeriksaan_awal/save'
            },
            'batalkanKunjungan' : {
                method : 'POST',
                url : base_url + '/api/rawat_jalan/daftar_kunjungan/batal'
            },
        });

        var _odc_ods = $resource(base_url + "/api/odc_ods/:id", {
            id: '@id'
        }, {
            'pemeriksaanAwal': {
                method: 'GET',
                url: base_url + '/api/odc_ods/pemeriksaan_awal/'
            },

            'pendaftaranSearch' : {
                method : 'GET',
                url: base_url + '/api/odc_ods/pendaftaran/search',
                isArray: true
            },
            'pemeriksaanAwalSave' : {
                method : 'POST',
                url: base_url + '/api/odc_ods/pemeriksaan_awal/save'
            },
            'batalkanKunjungan' : {
                method : 'POST',
                url : base_url + '/api/odc_ods/daftar_kunjungan/batal'
            },
        });

        var _kunjungan_ulang = $resource(base_url + "/api/kunjungan_ulang/:id", {
            id: '@id'
        }, {
            'pemeriksaanAwal': {
                method: 'GET',
                url: base_url + '/api/kunjungan_ulang/pemeriksaan_awal/'
            },

            'pendaftaranSearch' : {
                method : 'GET',
                url: base_url + '/api/kunjungan_ulang/pendaftaran/search',
                isArray: true
            },
            'pemeriksaanAwalSave' : {
                method : 'POST',
                url: base_url + '/api/kunjungan_ulang/pemeriksaan_awal/save'
            },
            'batalkanKunjungan' : {
                method : 'POST',
                url : base_url + '/api/kunjungan_ulang/batal'
            },
        });
        

        var _igd = $resource(base_url + "/api/igd/:id", {
            id: '@id'
        }, {
            'pemeriksaanAwal': {
                method: 'GET',
                url: base_url + '/api/igd/pemeriksaan_awal/'
            },

            'pendaftaranSearch' : {
                method : 'GET',
                url: base_url + '/api/igd/pendaftaran/search',
                isArray: true
            },
            'pemeriksaanAwalSave' : {
                method : 'POST',
                url: base_url + '/api/igd/pemeriksaan_awal/save'
            },
            'batalkanKunjungan' : {
                method : 'POST',
                url : base_url + '/api/igd/daftar_kunjungan/batal'
            }

        });





        return {

            //init
            Nomor: _nomor,

            //master
            Pasien: _pasien,

            //transaksi
            Tindakan: _tindakan,
            RawatJalan: _rawat_jalan,
            OdcOds: _odc_ods,
            kunjunganUlang: _kunjungan_ulang,
            Igd : _igd
        }
    }
])