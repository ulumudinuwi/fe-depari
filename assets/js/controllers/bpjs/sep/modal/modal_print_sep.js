/**
 * Created by agungrizkyana on 12/7/16.
 */
app.controller('ModalPrintSep', modalController);
modalController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$log', '$compile', 'items', '$timeout', 'toastr'];
function modalController($scope, api, $http, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $log, $compile, items, $timeout, toastr) {
    $scope.sep = items;

    $scope.url = base_url + '/bpjs/sep/cetak?param=' + JSON.stringify(items);

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.close = function () {
        $uibModalInstance.close();
    }
}