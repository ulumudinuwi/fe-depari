$formFoto           = $('#form-foto');
$fotoProfile        = $('#foto-profile');
$notifFoto          = $('#notif-foto');
$modalFoto          = $('#add_foto_modal');
$btnSubmitFoto      = $('#btn-submit-foto');
base_file           = base_url.replace('/index.php','');

$(function(){
    $formFoto.validate({
        submitHandler: function(form) {
            var target_url  = base_url+page_url+'save_foto/'+pegawai_id;
            var captionBtn  = $btnSubmitFoto.html();
            var data = new FormData($(form)[0]);
            console.log(data);
            $.ajax({
                url         : target_url,
                type        : "POST",
                data        : data,
                dataType    : 'json',
                mimeType    : "multipart/form-data",
                contentType : false,
                cache       : false,
                processData : false,
                beforeSend: function() {
                        $btnSubmitFoto.html('Please wait....');
                },
                success:function(data)
                {
                    $btnSubmitFoto.html(captionBtn);
                    console.log(data);
                    var pegawai = data.data;
                    console.log('pegawai ',pegawai);
                    $modalFoto.modal('hide');
                    $notifFoto.text(data.info);
                    $notifFoto.fadeIn('slow').delay('3500').fadeOut('slow');
                    $formFoto[0].reset();
                    $fotoProfile.prop('src',base_file+pegawai.foto);
                }
            });
            return false;
        }
    });
});

var Foto = {
    updateFoto: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.insert(url,data,function(data){
            console.log(data);
            Foto.getFoto();
            $notifFoto.html(data.info);
            $notifFoto.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahFoto: function(){
        $modalFoto.modal('show');
    }
}
