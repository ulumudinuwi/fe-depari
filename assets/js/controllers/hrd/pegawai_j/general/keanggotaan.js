$formKeanggotaan     = $('#form-keanggotaan');
$notifGeneral       = $('#notif');
$modalKeanggotaan    = $('#add_keanggotaan_modal');

$(function(){
    $formKeanggotaan.validate({
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_keanggotaan/'+pegawai_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Keanggotaan.getKeanggotaan();
                $modalKeanggotaan.modal('hide');
                $notifGeneral.text(data.info);
                $notifGeneral.fadeIn('slow').delay('3500').fadeOut('slow');
                $formKeanggotaan[0].reset();
            });
            return false;
        }
    });
});

var Keanggotaan = {
    getKeanggotaan: function() {
        var page_url    = '/api/hrd/pegawai/get_keanggotaan/'+pegawai_id;
        var columns     = [
            {
                "data": "tanggal_mulai",
                "render":function(data){
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "membership_id",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/pegawai/hapus_keanggotaan/' + row.uid + '" onClick="Keanggotaan.deleteKeanggotaan(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-keanggotaan',columns);
    },
    deleteKeanggotaan: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Keanggotaan.getKeanggotaan();
            $notifGeneral.text(data.info);
            $notifGeneral.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahKeanggotaan: function(){
        $modalKeanggotaan.modal('show');
    }
}
