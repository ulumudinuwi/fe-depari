$(function(){
    var the_table;
    $tgl        = $('#tgl_penyelenggaraan');
    $unitusaha  = $('#unitusaha');

    $tgl.daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
          format: 'DD/MM/YYYY'
        },
        startDate: {
            value: moment().subtract(29, 'days')
        },
        endDate: {
            value: moment()
        }
    });

    $tgl.on('apply.daterangepicker', function(ev, picker) {
        console.log('jalan');
        tgl_mulai = picker.startDate.format('YYYY-MM-DD');
        tgl_akhir = picker.endDate.format('YYYY-MM-DD');
        unitusaha = $unitusaha.val();
        get_data(tgl_mulai,tgl_akhir,unitusaha);
    });

    $unitusaha.change(function(){
        unitusaha = $unitusaha.val();
        tgl_mulai = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
        tgl_akhir = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        console.log('mulai ',tgl_mulai);
        console.log('akhir ',tgl_akhir);
        get_data(tgl_mulai,tgl_akhir,unitusaha);
    });

    get_data();
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function get_data(tgl_mulai,tgl_akhir,unitusaha){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_pelamar").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/rekrutment/pelamar',
            "type": "POST",
            "dataType":"json",
            "data": {tgl_mulai:tgl_mulai,tgl_akhir:tgl_akhir,unitusaha:unitusaha}
        },
        "columns": [
            {
                "data": "no",
                "searchable": false
            },
            {
                "data": "tgl_daftar",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<a href="'+base_url+'/hrd/rekrutment/pelamar/edit/' + row.uid + '">' + data + '</a>';
                }
            },
            {
                "data": "tempat_lahir"
            },
            {
                "data": "tgl_lahir",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(row.tgl_lahir).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "jenis_kelamin",
                "render": function(data, type, row, meta) {
                    if(data == 1)
                    return 'Laki-laki';
                    else
                    return 'Perempuan';
                }
            },
            {
                "data": "telepon",
            },

        ],
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        }
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}
