$formOrganisasi     = $('#form-organisasi');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalOrganisasi    = $('#add_organisasi_modal');

$.validator.addMethod("onlyNumber",
    function (value, element, options)
    {
        var req = /^\d+$/;

        return (value.match(req));
    },
    "Please enter a number."
);

$(function(){
    $formOrganisasi.validate({
        rules: {
            "tahun_masuk": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            },
            "tahun_keluar": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            }
        },
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_organisasi/'+pelamar_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_organisasi(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Organisasi.getOrganisasi();
                $modalOrganisasi.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formOrganisasi[0].reset();
            });
            return false;
        }
    });
});

var Organisasi = {
    getOrganisasi: function() {
        var page_url    = '/api/hrd/rekrutment/pelamar/get_organisasi/'+pelamar_id;
        var columns     = [
            {
                "data": "organisasi",
            },
            {
                "data": "tahun_masuk",
            },
            {
                "data": "tahun_keluar",
            },
            {
                "data": "keterangan",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/rekrutment/pelamar/hapus_organisasi/' + row.uid + '" onClick="Organisasi.deleteOrganisasi(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-organisasi',columns);
    },
    deleteOrganisasi: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Organisasi.getOrganisasi();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahOrganisasi: function(){
        $modalOrganisasi.modal('show');
    }
}
