/**
 * Created by agungrizkyana on 8/20/16.
 */
app.controller('MasterDokterPoliController', ['$scope', 'api', '$http', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr',
    function ($scope, api, $http, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr) {

        $scope.data = {};

        $scope.selected = function (item) {
            console.log(item);
            if (item) {
                $scope.data = angular.copy(item.originalObject);
                $scope.data.tanggal = new Date($scope.data.tanggal);
            }
        };


        function buildTable() {
            var url = base_url + '/api/master/dokter_poli/data_dokter';

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax', {

                    url: url,
                    type: 'POST'
                })
                // or here
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('order', [[0, 'desc']])
                .withOption('createdRow', function (row) {
                    // Recompiling so we can bind Angular directive to the DT
                    $compile(angular.element(row).contents())($scope);
                })
                .withPaginationType('full_numbers');

            $scope.dtInstance = {};
            $scope.dtColumns = [
                DTColumnBuilder.newColumn('created_at').withTitle('Tanggal'),
                DTColumnBuilder.newColumn('nik').withTitle('NIK'),
                DTColumnBuilder.newColumn('nama').withTitle('Nama Dokter'),
                DTColumnBuilder.newColumn('jabatan').withTitle('Jabatan'),
                DTColumnBuilder.newColumn('nama_layanan').withTitle('Poli'),
                DTColumnDefBuilder.newColumnDef(5).withTitle('Action').renderWith(function (data, type, row, meta) {
                    console.log(row.id);
                    return '<a ng-click="cancelKunjungan(\'' + row.id + '\')" class="btn bg-vmt-cancel-button"> <i class="fa fa-trash-o"></i></a>&nbsp;<a ng-click="cancelKunjungan(\'' + row.id + '\')" class="btn bg-vmt-preview"> <i class="fa fa-edit"></i></a>';
                })
            ];
        }

        buildTable();

    }
]);