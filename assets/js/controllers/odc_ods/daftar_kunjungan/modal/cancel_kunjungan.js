/**
 * Created by agungrizkyana on 10/7/16.
 */
app.controller('ModalCancelKunjunganController', modalCancelKunjunganController);
modalCancelKunjunganController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$compile', 'items'];
function modalCancelKunjunganController($scope, api, $http, $uibModalInstance, $log, $compile, items) {

    $scope.data = {
        id : items,
        alasan : ''
    };

    $scope.save = save;

    $scope.close = close;
    $scope.dismiss = dismiss;

    function save() {

        api.OdcOds.batalkanKunjungan(angular.copy($scope.data)).$promise.then(function(result){
            console.log(result);
            close(result);
        }).catch(function(err){
            console.log(err);
        });
        // close({});
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }
    function close(result) {
        console.log("data : ", result);
        $uibModalInstance.close(result);
    }

}