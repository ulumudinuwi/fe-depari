app.controller('PemeriksaanAwalFormController', ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$location',
    function ($rootScope, $scope, api, $http, CONFIG, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $location) {

        var url = base_url + '/api/odc_ods/pemeriksaan_awal/list_rawat_jalan/0';
        $scope.rows = [];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                url: url,
                type: 'POST',
                data: function (data, dtInstance) {

                }
            })
            // or here
            .withDataProp('data')
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('order', [[0, 'desc']])
            .withOption('createdRow', function (row) {
                $rootScope.$emit('ajax:stop');
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);

            })
            .withPaginationType('full_numbers');

        $scope.dtInstance = {};
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {
                return moment(row.tanggal).format('DD-MMM-YYYY H:mm:s');
                // return '<a href="' + base_url + '/rawat_jalan/pemeriksaan_awal?s=' + row.pelayanan_id + '&rawat_jalan=' + row.id + '">' + moment(row.tanggal).format('DD-MMM-YYYY H:mm:s') + '</a>';
            }).withOption('searchable', false),
            DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No RM'),
            DTColumnBuilder.newColumn('no_register').withTitle('No Register').renderWith(function (data, type, row, meta) {
                $scope.rows.push(row);

                return '<a  ng-click="openDetailPasien(' + row.id + ')">' + data + '</a>';
            }),
            DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
            DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
            DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter')
        ];

        $scope.dtInstance = {};


        $scope.openDetailPasien = function (idx) {
            var dataObj ;
            var result = $scope.rows.map(function(a){
                if (idx==a.id){
                    dataObj=a;
                }else {

                }
            });
            $scope.modalData = dataObj;
            console.log("modal data", $scope.modalData);
            $scope.modalData.fromNow = moment($scope.modalData.created_at).fromNow();
            $('#modal_detail').modal();
        };



    }]);