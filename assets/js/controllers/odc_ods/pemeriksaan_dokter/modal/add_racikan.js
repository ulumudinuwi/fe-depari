/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddRacikanController', modalAddRacikanController);

/**
 * Modal Add Racikan
 * @type {string[]}
 */
modalAddRacikanController.$inject = ['$rootScope', '$scope', 'api', '$http', '$uibModalInstance', '$log', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'items', '$filter', '$resource'];
function modalAddRacikanController($rootScope, $scope, api, $http, $uibModalInstance, $log, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, items, $filter, $resource) {

     var url = base_url + "/api/master/obat/non_racikan/" + items.data.kelompok_pasien_id;
    var _tarif = [];

    $rootScope.searchObat = base_url + '/api/master/obat/non_racikan?kelompok_pasien='+items.data.kelompok_pasien_id+'&s=';

    $scope.obat = {
        catatan: items.catatan
    };
    $scope.dataObat = [];
    $scope.signa = [];
    $scope.signaEdit = [];

    $scope.signa_dosis = [];
    $scope.signaDosisEdit = [];
    $scope.signa_cara_makan = [];
    $scope.signaCaraMakanEdit = [];
    $scope.signa_keterangan = [];
    $scope.signaKeteranganEdit = [];

    $scope.selectedObat = selectedObat;
    $scope.selectedSignaDosis = selectedSignaDosis;
    $scope.selectSignaCaraMakan = selectSignaCaraMakan;
    $scope.selectSignaKeterangan = selectSignaKeterangan;
    $scope.remove = remove;

    $scope.total = 0;

    $scope.isEdit = false;

    // isEdit

    if (items.isEdit) {

        $scope.isEdit = true;

        $scope.dataObat = items.dataObat.tarif;

        console.log(items.dataObat);

        var strSigna = "";
        _.each(_.map(items.dataObat.signa, function (o) {
            $scope.signa.push(o.id);
            return o.label;
        }), function (o) {
            strSigna += o + ", ";
        });

        $scope.signaEdit = strSigna;

        // Signa DOSIS
        var signa_dosis = [];
        var signaDosisEdit = [];
        for (var i = 0; i < items.dataObat.signa.length; i++) {
            if (items.dataObat.signa[i].kelompok == 'DOSIS') {
                signa_dosis.push(items.dataObat.signa[i].id);
                signaDosisEdit.push(items.dataObat.signa[i].label);
            }
        }
        $scope.signa_dosis = signa_dosis;
        $scope.signaDosisEdit = signaDosisEdit.join(', ');
        // Signa CARA MAKAN
        var signa_cara_makan = [];
        var signaCaraMakanEdit = [];
        for (var i = 0; i < items.dataObat.signa.length; i++) {
            if (items.dataObat.signa[i].kelompok == 'CARA MAKAN') {
                signa_cara_makan.push(items.dataObat.signa[i].id);
                signaCaraMakanEdit.push(items.dataObat.signa[i].label);
            }
        }
        $scope.signa_cara_makan = signa_cara_makan;
        $scope.signaCaraMakanEdit = signaCaraMakanEdit.join(', ');
        // Signa Keterangan/LAINNYA
        var signa_keterangan = [];
        var signaKeteranganEdit = [];
        for (var i = 0; i < items.dataObat.signa.length; i++) {
            if (items.dataObat.signa[i].kelompok == 'LAINNYA') {
                signa_keterangan.push(items.dataObat.signa[i].id);
                signaKeteranganEdit.push(items.dataObat.signa[i].label);
            }
        }
        $scope.signa_keterangan = signa_keterangan;
        $scope.signaKeteranganEdit = signaKeteranganEdit.join(', ');


        // $scope.signa = $scope.signaEdit;


        onChangeTotal();
    }

    $scope.close = function () {
        var signa = [];
        var signaEdit = [];
        var i = 0;

        // Signa DOSIS
        for (i = 0; i < $scope.signa_dosis.length; i++) {
            signa.push($scope.signa_dosis[i]);
        }
        for (i = 0; i < $scope.signaDosisEdit.length; i++) {
            signaEdit.push($scope.signaDosisEdit[i]);
        }
        // Signa CARA MAKAN
        for (i = 0; i < $scope.signa_cara_makan.length; i++) {
            signa.push($scope.signa_cara_makan[i]);
        }
        for (i = 0; i < $scope.signaCaraMakanEdit.length; i++) {
            signaEdit.push($scope.signaCaraMakanEdit[i]);
        }
        // Signa Keterangan/LAINNYA
        for (i = 0; i < $scope.signa_keterangan.length; i++) {
            signa.push($scope.signa_keterangan[i]);
        }
        for (i = 0; i < $scope.signaKeteranganEdit.length; i++) {
            signaEdit.push($scope.signaKeteranganEdit[i]);
        }

        var racikan = {
            nama: items.racikan,
            dataObat: angular.copy($scope.dataObat),
            signa: angular.copy(signa),
            signaEdit: angular.copy(signaEdit),
            isEdit: items.isEdit,
            idx: (items.idx) ? items.idx : 0
        };
        console.log("data : ", racikan);
        $uibModalInstance.close({
            racikan : racikan,
            catatan : $scope.obat.catatan
        });
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onChangeQty = onChangeQty;
    $scope.onChangeDiskon = onChangeDiskon;

    $scope.$on('CHANGE:TOTAL', onChangeTotal);

    $scope.selectedSigna = selectedSigna;

    function selectedSigna(val) {

        /// permintaan 08/01/2018
        // if ($scope.isEdit) {
        //     $scope.signa.push(val);
        // } else {
            $scope.signa = val;
       // }

    }

    function selectedSignaDosis(val) {
        $scope.signa_dosis = val;
    }
    function selectSignaCaraMakan(val) {
        $scope.signa_cara_makan = val;
    }
    function selectSignaKeterangan(val) {
        $scope.signa_keterangan = val;
    }

    function onChangeQty(idx, qty) {
        if(qty < 1 || qty > $scope.dataObat[idx].stock){
            $scope.dataObat[idx].qty = "";
        }else{
            $scope.dataObat[idx].jumlah = $scope.dataObat[idx].harga * $scope.dataObat[idx].qty;
            // $scope.dataObat[idx].stock -= qty;
            $scope.$emit('CHANGE:TOTAL');
        }

    }

    function onChangeDiskon(idx, diskon) {
        if (diskon) {
            if (diskon >= 1) {
                var qty = $scope.dataObat[idx].qty;
                var harga = qty * $scope.dataObat[idx].harga;
                $scope.dataObat[idx].jumlah = harga - ((harga / diskon) * 100);
                $scope.$emit('CHANGE:TOTAL');
            }
        }
    }

    function selectedObat(item) {
        if (item) {
            var obat = item.originalObject;
            obat.qty = 1;
            obat.diskon = 0;
            obat.jumlah = obat.harga;
            $scope.dataObat.push(obat);
            $scope.total = _.sum(_.map($scope.dataObat, function (val) {
                return parseInt(val.jumlah);
            }));
        }
    }

    function remove(index) {
        $scope.dataObat.splice(index, 1);
        onChangeTotal();
    }

    function onChangeTotal() {
        $scope.total = _.sum(_.map($scope.dataObat, function (val) {
            return parseInt(val.jumlah);
        }));
    }


}

