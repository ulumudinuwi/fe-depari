/** Init Variable **/
var ruang_operasi = $('#ruang_operasi').val();
/** Date Time Picker : Input Waktu**/
$(function(){
  $('.pickadate').pickadate({
    // options
    format: 'dd-mm-yyyy'
  });

  // Basic initialization
  $("#jam_mulai").AnyTime_picker({
    format: "%H:%i",
    firstDOW: 1
  });

  $("#jam_mulai_anesthesi").AnyTime_picker({
    format: "%H:%i",
    firstDOW: 1
  });

  $("#jam_selesai").AnyTime_picker({
    format: "%H:%i",
    firstDOW: 1
  });

});

function set_quantity(element){
  $tr = $(element).parent().parent();
  var quantity  = $(element).val();
  var rate     = $(element).parent().find('.tarif-rate').val();
  var total     = parseInt(rate)*parseInt(quantity);
  console.log(quantity,rate,total);
  $tr.find('.tarif-tindakan').val(total);
  $tr.find('.tarif').text(numeral(total).format('0,0'));
}
/** Init Variable Tindakan **/
var tindakan_current_counter = 0;
/** Pilih Tindakan in Modal **/
function pilihTindakan(element){
    var counter = tindakan_current_counter;

    var obat = [];
    obat['obat_id']      = $('.obat_id',element).val();
    obat['kode']         = $('.kode',element).val();
    obat['tarif']        = $('.tarif',element).val();
    obat['nama_asli']    = $('.nama_asli',element).val();
    obat['parent']       = $('.parent',element).val();
    obat['nama_operasi'] = $('.nama_operasi',element).val();

    obat['nama']         = $(element).parent().parent().find('.nama').text();
    // obat['satuan']    = $('.satuan',this).html();

    var nama_tindakan    = obat['parent']+'/'+obat['nama_asli'];
    if(obat['nama_operasi'])
      nama_tindakan += ('/'+obat['nama_operasi']);

    console.log(obat);
    console.log(counter);
    $("#tindakan_barang_id_"+counter).val(obat['obat_id']);
    $("#tindakan_parent_id_"+counter).val(obat['obat_id']);
    $("#tindakan_id_"+counter).val(obat['obat_id']);
    $("#tindakan_nama_operasi_"+counter).val(nama_tindakan);
    $("#tindakan_disp_kode_"+counter).val(obat['kode']);
    $("#tindakan_disp_barang_"+counter).val(nama_tindakan);
    $("#tindakan_tarif_"+counter).val(obat['tarif']);
    $("#tindakan_rate_"+counter).val(obat['tarif']);
    $("#tindakan_label_tarif_"+counter).html(numeral(obat['tarif']).format('0,0'));

    $("#barang_modal_tindakan").modal('hide');
}
/** Tindakan **/
$(document).ready(function() {
  $("#button_tambah_list_tindakan").click(function() {
    var $tr = $('#row_IDTABLE_tindakan_detail_clone').clone();

    var counter = parseInt($("#counter_tindakan").val());
    console.log(counter);
    counter++;
    $("#counter_tindakan").val(counter);

    $tr.attr('id', '')
       .attr('style', '');
    $tr.find('.cari_barang_tindakan').val(counter);
    $tr.find('#clone_IDTABLE_tindakan_detail_id')
      .attr('id', 'IDTABLE_tindakan_detail_id_' + counter)
      .attr('name', 'IDTABLE_tindakan_detail_id[]')
      .attr('value', 'new_IDTABLE_tindakan_detail_id_' + counter);

    $tr.find('#clone_tindakan_barang_id')
      .attr('id', 'tindakan_barang_id_' + counter)
      .attr('name', 'tindakan_barang_id[]')
      .attr('value', 0);

    $tr.find('#clone_tindakan_id')
      .attr('id', 'tindakan_id_' + counter)
      .attr('name', 'tindakan_id[]')
      .attr('value', 0);

    $tr.find('#clone_tindakan_parent_id')
      .attr('id', 'tindakan_parent_id_' + counter)
      .attr('name', 'tindakan_parent_id[]')
      .attr('value', 0);

    $tr.find('#clone_tindakan_nama_operasi')
      .attr('id', 'tindakan_nama_operasi_' + counter)
      .attr('name', 'tindakan_nama_operasi[]')
      .attr('value', '');

    $tr.find('#clone_tindakan_label_kode')
      .attr('id', 'tindakan_label_kode_' + counter);

    $tr.find('#clone_tindakan_disp_kode')
      .attr('id', 'tindakan_disp_kode_' + counter)
      .attr('placeholder', 'Kode ...');

    $tr.find('#clone_tindakan_label_barang')
      .attr('id', 'tindakan_label_barang_' + counter);

    $tr.find('#clone_tindakan_div_barang')
      .attr('id', 'tindakan_div_barang_' + counter);

    $tr.find('#clone_tindakan_disp_barang')
      .attr('id', 'tindakan_disp_barang_' + counter)
      .attr('placeholder', 'Nama Barang ...');

    $tr.find('#clone_tindakan_nama')
      .attr('id', 'tindakan_nama_' + counter)
      .attr('name', 'tindakan_nama[]');

    $tr.find('#clone_tindakan_kode')
      .attr('id', 'tindakan_kode_' + counter)
      .attr('name', 'tindakan_kode[]');

    $tr.find('#clone_tindakan_rate')
      .attr('name', 'tindakan_rate[]')
      .attr('id', 'tindakan_rate_' + counter);

    $tr.find('#clone_tindakan_tarif')
      .attr('name', 'tindakan_tarif[]')
      .attr('id', 'tindakan_tarif_' + counter);

    $tr.find('#clone_tindakan_label_tarif')
      .attr('id', 'tindakan_label_tarif_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $tr.find('#clone_tindakan_quantity')
      .attr('id', 'tindakan_quantity_' + counter)
      .attr('name', 'tindakan_quantity[]')
      .attr('value', 1);

    $tr.find('#clone_tindakan_label_quantity')
      .attr('id', 'tindakan_label_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $tr.find('#clone_tindakan_disp_quantity')
      .attr('id', 'tindakan_disp_quantity_' + counter)
      .attr('value', 1)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false}).on('keyup',function(){
        set_quantity(this);
        console.log("keyup")
      });

    $tr.find('#clone_tindakan_dokter_id')
      .attr('id', 'tindakan_dokter_id_' + counter)
      .attr('name', 'tindakan_dokter_id[]')
      .attr('value', 1);

    $tr.find('#clone_tindakan_dokter')
      .attr('id', 'tindakan_dokter_' + counter)
      .attr('name', 'tindakan_dokter[]');

    $tr.find('#clone_tindakan_label_dokter_id')
      .attr('id', 'tindakan_label_dokter_id_' + counter);


    var dokter_operator = $(document).find('#dokter_operator').val();
    console.log(dokter_operator);

    var options = '';
    var dr_op = $('#dokter_operator');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#asisten_operator');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#asisten');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#dokter_anastesi');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#asisten_anastesi');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#dokter_konsulen');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });


    $tr.find('#clone_tindakan_disp_dokter_id')
      .attr('id', 'tindakan_disp_dokter_id_' + counter)
      .html(options);


    $("#IDTABLE_tindakan").find("tbody").append($tr);

    $("#button_0_tindakan").attr('id', '').addClass('button_tambah_search').html('<i class="fa fa-search"></i>');
    $("#button_1_tindakan").attr('id', '').addClass('button_tambah_simpan').html('<i class="fa fa-save"></i>');
    $("#button_2_tindakan").attr('id', '').addClass('button_tambah_batal').html('<i class="fa fa-times"></i>');

    $(".button_edit").prop('disabled', true);
    $(".button_hapus").prop('disabled', true);

    $("#button_tambah_list_tindakan").prop('disabled', true);

    $('#simpan1').prop('disabled', true);
    $('#simpan2').prop('disabled', true);
    $('#batal1').prop('disabled', true);
    $('#batal2').prop('disabled', true);

    $tr.find('.select_kode').focus();

    return false;
  });

  $('#IDTABLE_tindakan').on('click', '.button_tambah_simpan', function() {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    var kode = $tr.find('#tindakan_disp_kode_' + counter).val();
    $tr.find('#tindakan_label_kode_' + counter).text(kode).show();
    $tr.find('#tindakan_disp_kode_' + counter).remove();
    $tr.find('#tindakan_kode_' + counter).val(kode);

    var barang = $tr.find('#tindakan_disp_barang_' + counter).val();
    $tr.find('#tindakan_label_barang_' + counter).text(barang).show();
    $tr.find('#tindakan_div_barang_' + counter).remove();
    $tr.find('#tindakan_nama_' + counter).val(barang);

    var quantity = $tr.find('#tindakan_disp_quantity_' + counter).autoNumeric('get');
    $tr.find('#tindakan_quantity_' + counter).val(quantity);
    $tr.find('#tindakan_label_quantity_' + counter).autoNumeric('set', quantity).show();
    $tr.find('#tindakan_disp_quantity_' + counter).remove();

    var dokter_id = $tr.find('#tindakan_disp_dokter_id_' + counter).val();
    var dokter_nama = $tr.find('#tindakan_disp_dokter_id_' + counter + ' option:selected').text();
    $tr.find('#tindakan_dokter_id_' + counter).val(dokter_id);
    $tr.find('#tindakan_label_dokter_id_' + counter).text(dokter_nama).show();
    $tr.find('#tindakan_dokter_' + counter).val(dokter_nama);
    $tr.find('#tindakan_disp_dokter_id_' + counter).remove();

    set_tarif();
    // $(".button_tambah_simpan").remove();
    $("#IDTABLE_tindakan .button_tambah_search").removeClass('button_tambah_search').addClass('button_search');
    $("#IDTABLE_tindakan .button_tambah_simpan").removeClass('button_tambah_simpan').addClass('button_edit');
    $("#IDTABLE_tindakan .button_edit").html('<i class="fa fa-edit"></i>');
    $("#IDTABLE_tindakan .button_tambah_batal").removeClass('button_tambah_batal').addClass('button_hapus');
    $("#IDTABLE_tindakan .button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_search").show();
    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list_tindakan").prop('disabled', false);
    $("#button_tambah_list_tindakan").focus();

    $('#simpan1').prop('disabled', false);
    $('#simpan2').prop('disabled', false);
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    return false;
  });

  $('#IDTABLE_tindakan').on('click', '.button_tambah_batal', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();
    $tr.remove();

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list_tindakan").prop('disabled', false);
    $("#button_tambah_list_tindakan").focus();

    $('.close').show();
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    tbody = $("#IDTABLE_tindakan").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#simpan1').prop('disabled', true);
      $('#simpan2').prop('disabled', true);
    } else {
      $('#simpan1').prop('disabled', false);
      $('#simpan2').prop('disabled', false);
    }

    return false;
  });

  $('#IDTABLE_tindakan').on('click', '.button_edit', function() {
    $tr = $(this).parent().parent();
    console.log($tr);
    var counter = getCounter($tr);

    var kode = $tr.find('#tindakan_label_kode_' + counter).text();
    var quantity = $tr.find('#tindakan_quantity_' + counter).val();
    $tr.find('#tindakan_label_quantity_' + counter).hide();
    $tr.children('td').eq(2).append('<input id="tindakan_disp_quantity_' + counter + '" class="quantity-row form-control" type="text" value="' + quantity + '" style="text-align: right;" />');
    $tr.find('#tindakan_disp_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false})
      .focus()
      .select()
      .on('keyup',function(){
        set_quantity(this);
        console.log("keyup")
      });

    var dokter_id = $tr.find('#tindakan_dokter_id_' + counter).val();
    var dokter_nama = $tr.find('#tindakan_dokter_' + counter).val();
    $tr.find('#tindakan_label_dokter_id_' + counter).hide();

    var options = '';

    var dr_op = $('#dokter_operator');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#asisten_operator');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#asisten');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#dokter_anastesi');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#asisten_anastesi');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    var dr_op = $('#dokter_konsulen');
    var dr_operator = dr_op.tagsinput('items');
    $.each(dr_operator,function(i,v){
      console.log('dr ',v);
      options += '<option value="'+v.value+'">'+v.text+'</option>';
    });

    $tr.children('td').eq(4).append('<select id="tindakan_disp_dokter_id_' + counter + '" class="dokter_id-row form-control" >'+options+'</select>');

    $tr.find('#tindakan_disp_dokter_id_' + counter).val(dokter_id);

    $tr.find(".button_edit").removeClass('button_edit').addClass('button_edit_simpan');
    $tr.find(".button_edit_simpan").html('<i class="fa fa-save"></i>');
    $tr.find(".button_hapus").removeClass('button_hapus').addClass('button_edit_batal');
    $tr.find(".button_edit_batal").html('<i class="fa fa-times"></i>');

    $(".button_edit").prop('disabled', true);
    $(".button_hapus").prop('disabled', true);

    $("#button_tambah_list_tindakan").prop('disabled', true);

    $('#simpan1').prop('disabled', true);
    $('#simpan2').prop('disabled', true);
    $('#batal1').prop('disabled', true);
    $('#batal2').prop('disabled', true);

    return false;
  });

  $('#IDTABLE_tindakan').on('click', '.button_hapus', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    $tr.remove();

        $("#button_tambah_list_tindakan").focus();

        tbody = $("#IDTABLE_tindakan").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#simpan1').prop('disabled', true);
      $('#simpan2').prop('disabled', true);
    } else {
      $('#simpan1').prop('disabled', false);
      $('#simpan2').prop('disabled', false);
    }
    return false;
  });

  $('#IDTABLE_tindakan').on('click', '.button_edit_simpan', function(event) {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    var quantity = $tr.find('#tindakan_disp_quantity_' + counter).autoNumeric('get');
    $tr.find('#tindakan_quantity_' + counter).val(quantity);
    $tr.find('#tindakan_label_quantity_' + counter).autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
    $tr.find('#tindakan_label_quantity_' + counter).autoNumeric('set', quantity).show();
    $tr.find('#tindakan_disp_quantity_' + counter).remove();

    var dokter_id = $tr.find('#tindakan_disp_dokter_id_' + counter).val();
    var dokter_nama = $tr.find('#tindakan_disp_dokter_id_' + counter + ' option:selected').text();
    $tr.find('#tindakan_dokter_id_' + counter).val(dokter_id);
    $tr.find('#tindakan_label_dokter_id_' + counter).text(dokter_nama).show();
    $tr.find('#tindakan_dokter_' + counter).val(dokter_nama);
    $tr.find('#tindakan_disp_dokter_id_' + counter).remove();

    set_tarif();

    $("#IDTABLE_tindakan .button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
    $("#IDTABLE_tindakan .button_edit").html('<i class="fa fa-edit"></i>');
    $("#IDTABLE_tindakan .button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
    $("#IDTABLE_tindakan .button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list_tindakan").prop('disabled', false);
    $("#button_tambah_list_tindakan").focus();

    $('#simpan1').prop('disabled', false);
    $('#simpan2').prop('disabled', false);
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    return false;
  });

  $('#IDTABLE_tindakan').on('click', '.button_edit_batal', function() {

    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    $tr.find('#tindakan_disp_kode_' + counter).remove();
    $tr.find('#tindakan_label_kode_' + counter).show();

    $tr.find('#tindakan_div_barang_' + counter).remove();
    $tr.find('#tindakan_label_barang_' + counter).show();

    $tr.find('#tindakan_disp_quantity_' + counter).remove();
    $tr.find('#tindakan_label_quantity_' + counter).show();

    $("#IDTABLE_tindakan .button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
    $("#IDTABLE_tindakan .button_edit").html('<i class="fa fa-edit"></i>');
    $("#IDTABLE_tindakan .button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
    $("#IDTABLE_tindakan .button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list_tindakan").prop('disabled', false);
    $("#button_tambah_list_tindakan").focus();

    $('#simpan1').prop('disabled', false);
    $('#simpan2').prop('disabled', false);
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    return false;
  });

  $("#IDTABLE_tindakan").on("click", ".cari_barang_tindakan", function() {
    $tr = $(this).parent().parent();
    // console.log($(this).val());
    tindakan_current_counter = $(this).val();
    var counter = getCounter($tr);
    $('#counter_barang_tindakan').val(counter);
    var dokter_anastesi = $(document).find('#dokter_anastesi').val();
    var asisten_anastesi = $(document).find('#asisten_anastesi').val();
    var dokter_konsulen = $(document).find('#dokter_konsulen').val();
    var dokter_operator = $(document).find('#dokter_operator').val();
    var asisten_operator = $(document).find('#asisten_operator').val();
    var dokter = '';
    if(dokter_operator)
      dokter = "dokter_1";
    if(asisten_operator){
      if(dokter) dokter += ",";
      dokter += "dokter_2";
    }
    if(dokter_konsulen){
      if(dokter) dokter += ",";
      dokter += "dokter_3";
    }
    if(dokter_anastesi){
      if(dokter) dokter += ",";
      dokter += "dokter_4";
    }
    console.log('dokter ',dokter);
    // alert()
    get_data(dokter);
    // DaftarTindakan.init();
    $("#barang_modal_tindakan").modal('show');
  });
});
/** End of Tindakan **/

/** Init Variable Anesthesi **/
var anesthesi_current_counter = 0;
/** Pilih Anesthesi in Modal **/
function pilihAnesthesi(element){
    var counter = anesthesi_current_counter;
    // var counter = parseInt($("#counter").val());
    var obat = [];
    obat['obat_id'] = $('.obat_id',element).val();
    obat['kode'] = $('.kode',element).val();
    obat['nama'] = $('.nama',element).val();
    obat['satuan'] = $('.satuan',element).val();
    console.log(obat);
    console.log(counter);
    $("#anesthesi_id_"+counter).val(obat['obat_id']);
    $("#disp_anesthesi_kode_"+counter).val(obat['kode']);
    $("#disp_anesthesi_"+counter).val(obat['nama']);
    $("#anesthesi_label_satuan_"+counter).html(obat['satuan']);

    $("#anesthesi_modal").modal('hide');
}

/** Penggunaan ANESTHESI **/
$(document).ready(function() {
  $("#anesthesi_button_tambah_list").click(function() {
    var $tr = $('#row_IDTABLE_anesthesi_detail_clone').clone();

    var counter = parseInt($("#counter").val());
    console.log(counter);
    counter++;
    $("#counter").val(counter);

    $tr.attr('id', '')
       .attr('style', '');
    $tr.find('.cari_barang').val(counter);
    $tr.find('#clone_IDTABLE_anesthesi_detail_id')
      .attr('id', 'IDTABLE_anesthesi_detail_id_' + counter)
      .attr('name', 'IDTABLE_anesthesi_detail_id[]')
      .attr('value', 'new_IDTABLE_anesthesi_detail_id_' + counter);

    $tr.find('#clone_anesthesi_id')
      .attr('id', 'anesthesi_id_' + counter)
      .attr('name', 'anesthesi_id[]')
      .attr('value', 0);

    $tr.find('#clone_anesthesi_kode')
      .attr('id', 'anesthesi_kode_' + counter)
      .attr('name', 'anesthesi_kode[]');

    $tr.find('#clone_anesthesi_nama')
      .attr('id', 'anesthesi_nama_' + counter)
      .attr('name', 'anesthesi_nama[]');

    $tr.find('#clone_jenis_anesthesi')
      .attr('id', 'jenis_anesthesi_' + counter)
      .attr('name', 'jenis_anesthesi[]');

    $tr.find('#clone_anesthesi_satuan')
      .attr('id', 'anesthesi_satuan_' + counter)
      .attr('name', 'anesthesi_satuan[]');

    $tr.find('#clone_anesthesi_anesthesi_label_kode')
      .attr('id', 'anesthesi_label_kode_' + counter);

    $tr.find('#clone_disp_anesthesi_kode')
      .attr('id', 'disp_anesthesi_kode_' + counter)
      .attr('placeholder', 'Kode ...');

    $tr.find('#clone_anesthesi_label_barang')
      .attr('id', 'label_anesthesi_' + counter);

    $tr.find('#clone_anesthesi_div_barang')
      .attr('id', 'div_anesthesi_' + counter);

    $tr.find('#clone_anesthesi_disp_barang')
      .attr('id', 'disp_anesthesi_' + counter)
      .attr('placeholder', 'Nama Barang ...');

    $tr.find('#clone_anesthesi_label_jenis')
      .attr('id', 'label_jenis_anesthesi_' + counter);

    $tr.find('#clone_anesthesi_div_jenis')
      .attr('id', 'div_jenis_anesthesi_' + counter);

    $tr.find('#clone_anesthesi_disp_jenis')
      .attr('id', 'disp_jenis_anesthesi_' + counter);

    $tr.find('#clone_jenis_anesthesi_id')
      .attr('id', 'jenis_anesthesi_id_' + counter)
      .attr('name', 'jenis_anesthesi_id[]')
      .attr('value', 0);

    $tr.find('#clone_anesthesi_label_satuan')
      .attr('id', 'anesthesi_label_satuan_' + counter);

    $tr.find('#clone_anesthesi_quantity')
      .attr('id', 'anesthesi_quantity_' + counter)
      .attr('name', 'anesthesi_quantity[]')
      .attr('value', 1);

    $tr.find('#clone_anesthesi_label_quantity')
      .attr('id', 'label_anesthesi_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $tr.find('#clone_anesthesi_disp_quantity')
      .attr('id', 'disp_anesthesi_quantity_' + counter)
      .attr('value', 1)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $("#IDTABLE_anesthesi").find("tbody").append($tr);

    $("#anesthesi_button_1").attr('id', '').addClass('button_tambah_simpan').html('<i class="fa fa-save"></i>');
    $("#anesthesi_button_2").attr('id', '').addClass('button_tambah_batal').html('<i class="fa fa-times"></i>');

    $(".button_edit").prop('disabled', true);
    $(".button_hapus").prop('disabled', true);

    $("#anesthesi_button_tambah_list").prop('disabled', true);

    $('#anesthesi_simpan1').prop('disabled', true);
    $('#anesthesi_simpan2').prop('disabled', true);
    $('#anesthesi_batal1').prop('disabled', true);
    $('#anesthesi_batal2').prop('disabled', true);

    $tr.find('.select_kode').focus();

    return false;
  });

  $('#IDTABLE_anesthesi').on('click', '.button_tambah_simpan', function() {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    var kode = $tr.find('#disp_anesthesi_kode_' + counter).val();
    $tr.find('#anesthesi_label_kode_' + counter).text(kode).show();
    $tr.find('#disp_anesthesi_kode_' + counter).remove();
    $tr.find('#anesthesi_kode_' + counter).val(kode);

    var barang = $tr.find('#disp_anesthesi_' + counter).val();
    $tr.find('#anesthesi_nama_' + counter).val(barang);
    $tr.find('#label_anesthesi_' + counter).text(barang).show();
    $tr.find('#div_anesthesi_' + counter).remove();

    var satuan = $tr.find('#anesthesi_label_satuan_' + counter).text();
    $tr.find('#anesthesi_satuan_' + counter).val(satuan);

    var id_jenis = $tr.find('#disp_jenis_anesthesi_' + counter).val();
    $tr.find('#jenis_anesthesi_id_' + counter).val(id_jenis);


    var jenis = $tr.find('#disp_jenis_anesthesi_' + counter + ' option:selected').text();
    $tr.find('#label_jenis_anesthesi_' + counter).text(jenis).show();
    $tr.find('#jenis_anesthesi_' + counter).val(jenis);
    $tr.find('#div_jenis_anesthesi_' + counter).remove();

    var quantity = $tr.find('#disp_anesthesi_quantity_' + counter).autoNumeric('get');
    $tr.find('#anesthesi_quantity_' + counter).val(quantity);
    $tr.find('#label_anesthesi_quantity_' + counter).autoNumeric('set', quantity).show();
    $tr.find('#disp_anesthesi_quantity_' + counter).remove();

    $(".button_tambah_simpan").remove();
    // $(".button_tambah_simpan").removeClass('button_tambah_simpan').addClass('button_edit');
    // $(".button_edit").html('<i class="fa fa-edit"></i>');
    $(".button_tambah_batal").removeClass('button_tambah_batal').addClass('button_hapus');
    $(".button_hapus").html('<i class="fa fa-trash-o"></i>');

    // $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#anesthesi_button_tambah_list").prop('disabled', false);
    $("#anesthesi_button_tambah_list").focus();

    $('#anesthesi_simpan1').prop('disabled', false);
    $('#anesthesi_simpan2').prop('disabled', false);
    $('#anesthesi_batal1').prop('disabled', false);
    $('#anesthesi_batal2').prop('disabled', false);

    return false;
  });

  $('#IDTABLE_anesthesi').on('click', '.button_tambah_batal', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();
    $tr.remove();

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#anesthesi_button_tambah_list").prop('disabled', false);
    $("#anesthesi_button_tambah_list").focus();

    $('.close').show();
    $('#anesthesi_batal1').prop('disabled', false);
    $('#anesthesi_batal2').prop('disabled', false);

    tbody = $("#IDTABLE_anesthesi").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#anesthesi_simpan1').prop('disabled', true);
      $('#anesthesi_simpan2').prop('disabled', true);
    } else {
      $('#anesthesi_simpan1').prop('disabled', false);
      $('#anesthesi_simpan2').prop('disabled', false);
    }

    return false;
  });

  $('#IDTABLE_anesthesi').on('click', '.button_edit', function() {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    var kode = $tr.find('#anesthesi_label_kode_' + counter).text();
    $tr.find('#anesthesi_label_kode_' + counter).hide();
    $tr.children('td').eq(0).append('<input id="disp_anesthesi_kode_' + counter + '" class="select_kode form-control" type="text" value="' + kode + '" placeholder="Kode ..." />');

    var barang = $tr.find('#label_anesthesi_' + counter).text();
    $tr.find('#label_anesthesi_' + counter).hide();
    $tr.children('td').eq(1).append('<div id="div_anesthesi_' + counter + '" class="col-sm-12 no-margin">' +
                      '<div class="input-group">' +
                        '<input class="form-control select_barang" type="text" id="disp_anesthesi_' + counter + '" value="' + barang + '" placeholder="barang ..." />' +
                        '<div class="input-group-btn">' +
                        '<button type="button" class="cari_barang btn btn-primary" value="'+ counter +'"><i class="fa fa-search"></i></button></div>' +
                      '</div>' +
                    '</div>');

    var quantity = $tr.find('#label_anesthesi_quantity_' + counter).autoNumeric('get');
    $tr.find('#label_anesthesi_quantity_' + counter).hide();
    $tr.children('td').eq(3).append('<input id="disp_anesthesi_quantity_' + counter + '" class="quantity-row form-control" type="text" value="' + quantity + '" style="text-align: right;" />');
    $tr.find('#disp_anesthesi_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false})
      .focus()
      .select();

    $tr.find(".button_edit").removeClass('button_edit').addClass('button_edit_simpan');
    $tr.find(".button_edit_simpan").html('<i class="fa fa-save"></i>');
    $tr.find(".button_hapus").removeClass('button_hapus').addClass('button_edit_batal');
    $tr.find(".button_edit_batal").html('<i class="fa fa-times"></i>');

    $(".button_edit").prop('disabled', true);
    $(".button_hapus").prop('disabled', true);

    $("#anesthesi_button_tambah_list").prop('disabled', true);

    $('#anesthesi_simpan1').prop('disabled', true);
    $('#anesthesi_simpan2').prop('disabled', true);
    $('#anesthesi_batal1').prop('disabled', true);
    $('#anesthesi_batal2').prop('disabled', true);

    return false;
  });

  $('#IDTABLE_anesthesi').on('click', '.button_hapus', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    $tr.remove();

        $("#anesthesi_button_tambah_list").focus();

        tbody = $("#IDTABLE_anesthesi").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#anesthesi_simpan1').prop('disabled', true);
      $('#anesthesi_simpan2').prop('disabled', true);
    } else {
      $('#anesthesi_simpan1').prop('disabled', false);
      $('#anesthesi_simpan2').prop('disabled', false);
    }
    return false;
  });

  $('#IDTABLE_anesthesi').on('click', '.button_edit_simpan', function(event) {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    var kode = $tr.find('#disp_anesthesi_kode_' + counter).val();
    $tr.find('#anesthesi_label_kode_' + counter).text(kode).show();
    $tr.find('#disp_anesthesi_kode_' + counter).remove();

    var barang = $tr.find('#disp_anesthesi_' + counter).val();
    $tr.find('#div_anesthesi_' + counter).remove();
    $tr.find('#label_anesthesi_' + counter).text(barang).show();

    var quantity = $tr.find('#disp_anesthesi_quantity_' + counter).autoNumeric('get');
    $tr.find('#anesthesi_quantity_' + counter).val(quantity);
    $tr.find('#label_anesthesi_quantity_' + counter).autoNumeric('set', quantity).show();
    $tr.find('#disp_anesthesi_quantity_' + counter).remove();

    $(".button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
    $(".button_edit").html('<i class="fa fa-edit"></i>');
    $(".button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
    $(".button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#anesthesi_button_tambah_list").prop('disabled', false);
    $("#anesthesi_button_tambah_list").focus();

    $('#anesthesi_simpan1').prop('disabled', false);
    $('#anesthesi_simpan2').prop('disabled', false);
    $('#anesthesi_batal1').prop('disabled', false);
    $('#anesthesi_batal2').prop('disabled', false);

    return false;
  });

  $('#IDTABLE_anesthesi').on('click', '.button_edit_batal', function() {

    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    $tr.find('#disp_anesthesi_kode_' + counter).remove();
    $tr.find('#anesthesi_label_kode_' + counter).show();

    $tr.find('#div_anesthesi_' + counter).remove();
    $tr.find('#label_anesthesi_' + counter).show();

    $tr.find('#disp_anesthesi_quantity_' + counter).remove();
    $tr.find('#label_anesthesi_quantity_' + counter).show();

    $(".button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
    $(".button_edit").html('<i class="fa fa-edit"></i>');
    $(".button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
    $(".button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#anesthesi_button_tambah_list").prop('disabled', false);
    $("#anesthesi_button_tambah_list").focus();

    $('#anesthesi_simpan1').prop('disabled', false);
    $('#anesthesi_simpan2').prop('disabled', false);
    $('#anesthesi_batal1').prop('disabled', false);
    $('#anesthesi_batal2').prop('disabled', false);

    return false;
  });

  $("#IDTABLE_anesthesi").on("click", ".cari_barang", function() {
    $tr = $(this).parent().parent();
    console.log($(this).val());
    anesthesi_current_counter = $(this).val();
    var counter = getCounter($tr);
    $('#anesthesi_counter_barang').val(counter);
    get_data_obat('/ok/pelaksanaan/get_obat_anesthesi','dataTable_barang_anesthesi','pilihAnesthesi');
    $("#anesthesi_modal").modal('show');
  });
});
/** End of Anesthesi **/

/** Init Variable Pengguanaan Obat **/
var obat_current_counter = 0;
/** Pilih Obat in Modal **/
function pilihObat(element){
    var counter = obat_current_counter;
    // var counter = parseInt($("#counter").val());
    var obat = [];
    obat['obat_id'] = $('.obat_id',element).val();
    obat['kode'] = $('.kode',element).val();
    obat['nama'] = $('.nama',element).val();
    obat['satuan'] = $('.satuan',element).val();
    console.log(obat);
    console.log(counter);
    $("#barang_id_"+counter).val(obat['obat_id']);
    $("#disp_kode_"+counter).val(obat['kode']);
    $("#disp_barang_"+counter).val(obat['nama']);
    $("#label_satuan_"+counter).html(obat['satuan']);

    $("#barang_modal").modal('hide');
}
/** Penggunaan OBAT **/
$(document).ready(function() {
  $("#button_tambah_list").click(function() {
    var $tr = $('#row_IDTABLE_detail_clone').clone();

    // var counter = parseInt($("#counter").val());
    var counter = parseInt($("#button_tambah_list").closest('table').next('#counter').val());
    console.log(counter);
    counter++;
    $("#button_tambah_list").closest('table').next('#counter').val(counter);

    $tr.attr('id', '')
       .attr('style', '');
    $tr.find('.cari_barang').val(counter);
    $tr.find('#clone_IDTABLE_detail_id')
      .attr('id', 'IDTABLE_detail_id_' + counter)
      .attr('name', 'IDTABLE_detail_id[]')
      .attr('value', 'new_IDTABLE_detail_id_' + counter);

    $tr.find('#clone_barang_id')
      .attr('id', 'barang_id_' + counter)
      .attr('name', 'barang_id[]')
      .attr('value', 0);

    $tr.find('#clone_barang_kode')
      .attr('id', 'barang_kode_' + counter)
      .attr('name', 'barang_kode[]');

    $tr.find('#clone_label_kode')
      .attr('id', 'label_kode_' + counter);

    $tr.find('#clone_disp_kode')
      .attr('id', 'disp_kode_' + counter)
      .attr('placeholder', 'Kode ...');

    $tr.find('#clone_barang_nama')
      .attr('id', 'barang_nama_' + counter)
      .attr('name', 'barang_nama[]');

    $tr.find('#clone_label_barang')
      .attr('id', 'label_barang_' + counter);

    $tr.find('#clone_div_barang')
      .attr('id', 'div_barang_' + counter);

    $tr.find('#clone_disp_barang')
      .attr('id', 'disp_barang_' + counter)
      .attr('placeholder', 'Nama Barang ...');

    $tr.find('#clone_satuan')
      .attr('id', 'satuan_' + counter)
      .attr('name', 'satuan[]');

    $tr.find('#clone_label_satuan')
      .attr('id', 'label_satuan_' + counter);

    $tr.find('#clone_quantity')
      .attr('id', 'quantity_' + counter)
      .attr('name', 'quantity[]')
      .attr('value', 1);

    $tr.find('#clone_label_quantity')
      .attr('id', 'label_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $tr.find('#clone_disp_quantity')
      .attr('id', 'disp_quantity_' + counter)
      .attr('value', 1)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $("#IDTABLE_table").find("tbody").append($tr);

    $("#button_1").attr('id', '').addClass('button_tambah_simpan').html('<i class="fa fa-save"></i>');
    $("#button_2").attr('id', '').addClass('button_tambah_batal').html('<i class="fa fa-times"></i>');

    $(".button_edit").prop('disabled', true);
    $(".button_hapus").prop('disabled', true);

    $("#button_tambah_list").prop('disabled', true);

    $('#simpan1').prop('disabled', true);
    $('#simpan2').prop('disabled', true);
    $('#batal1').prop('disabled', true);
    $('#batal2').prop('disabled', true);

    $tr.find('.select_kode').focus();

    return false;
  });

  $('#IDTABLE_table').on('click', '.button_tambah_simpan', function() {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    var kode = $tr.find('#disp_kode_' + counter).val();
    $tr.find('#barang_kode_' + counter).val(kode);
    $tr.find('#label_kode_' + counter).text(kode).show();
    $tr.find('#disp_kode_' + counter).remove();

    var barang = $tr.find('#disp_barang_' + counter).val();
    $tr.find('#barang_nama_' + counter).val(barang);

    $tr.find('#label_barang_' + counter).text(barang).show();
    $tr.find('#div_barang_' + counter).remove();

    var satuan = $tr.find('#label_satuan_' + counter).text();
    $tr.find('#satuan_' + counter).val(satuan);

    var quantity = $tr.find('#disp_quantity_' + counter).autoNumeric('get');
    $tr.find('#quantity_' + counter).val(quantity);
    $tr.find('#label_quantity_' + counter).autoNumeric('set', quantity).show();
    $tr.find('#disp_quantity_' + counter).remove();

     // $(".button_tambah_simpan").remove();
    $(".button_tambah_simpan").removeClass('button_tambah_simpan').addClass('button_edit');
    $(".button_edit").html('<i class="fa fa-edit"></i>');
    $(".button_tambah_batal").removeClass('button_tambah_batal').addClass('button_hapus');
    $(".button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list").prop('disabled', false);
    $("#button_tambah_list").focus();

    $('#simpan1').prop('disabled', false);
    $('#simpan2').prop('disabled', false);
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    return false;
  });

  $('#IDTABLE_table').on('click', '.button_tambah_batal', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();
    $tr.remove();

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list").prop('disabled', false);
    $("#button_tambah_list").focus();

    $('.close').show();
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    tbody = $("#IDTABLE_table").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#simpan1').prop('disabled', true);
      $('#simpan2').prop('disabled', true);
    } else {
      $('#simpan1').prop('disabled', false);
      $('#simpan2').prop('disabled', false);
    }

    return false;
  });

  $('#IDTABLE_table').on('click', '.button_edit', function() {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    /*var kode = $tr.find('#label_kode_' + counter).text();
    $tr.find('#label_kode_' + counter).hide();
    $tr.children('td').eq(0).append('<input id="disp_kode_' + counter + '" class="select_kode form-control" type="text" value="' + kode + '" placeholder="Kode ..." />');

    var barang = $tr.find('#label_barang_' + counter).text();
    $tr.find('#label_barang_' + counter).hide();
    $tr.children('td').eq(1).append('<div id="div_barang_' + counter + '" class="col-sm-12 no-margin">' +
                      '<div class="input-group">' +
                        '<input class="form-control select_barang" type="text" id="disp_barang_' + counter + '" value="' + barang + '" placeholder="barang ..." />' +
                        '<div class="input-group-btn">' +
                        '<button type="button" class="cari_barang btn btn-primary" value="'+ counter +'"><i class="fa fa-search"></i></button></div>' +
                      '</div>' +
                    '</div>');*/
    var quantity = $tr.find('#quantity_' + counter).val();
    $tr.find('#label_quantity_' + counter).hide();
    $tr.children('td').eq(3).append('<input id="disp_quantity_' + counter + '" class="quantity-row form-control" type="text" value="' + quantity + '" style="text-align: right;" />');
    $tr.find('#disp_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false})
      .focus()
      .select();

    $tr.find(".button_edit").removeClass('button_edit').addClass('button_edit_simpan');
    $tr.find(".button_edit_simpan").html('<i class="fa fa-save"></i>');
    $tr.find(".button_hapus").removeClass('button_hapus').addClass('button_edit_batal');
    $tr.find(".button_edit_batal").html('<i class="fa fa-times"></i>');

    $(".button_edit").prop('disabled', true);
    $(".button_hapus").prop('disabled', true);

    $("#button_tambah_list").prop('disabled', true);

    $('#simpan1').prop('disabled', true);
    $('#simpan2').prop('disabled', true);
    $('#batal1').prop('disabled', true);
    $('#batal2').prop('disabled', true);

    return false;
  });

  $('#IDTABLE_table').on('click', '.button_hapus', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    $tr.remove();

        $("#button_tambah_list").focus();

        tbody = $("#IDTABLE_table").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#simpan1').prop('disabled', true);
      $('#simpan2').prop('disabled', true);
    } else {
      $('#simpan1').prop('disabled', false);
      $('#simpan2').prop('disabled', false);
    }
    return false;
  });

  $('#IDTABLE_table').on('click', '.button_edit_simpan', function(event) {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    // var kode = $tr.find('#disp_kode_' + counter).val();
    // $tr.find('#label_kode_' + counter).text(kode).show();
    // $tr.find('#disp_kode_' + counter).remove();

    // var barang = $tr.find('#disp_barang_' + counter).val();
    // $tr.find('#div_barang_' + counter).remove();
    // $tr.find('#label_barang_' + counter).text(barang).show();

    var quantity = $tr.find('#disp_quantity_' + counter).autoNumeric('get');
    $tr.find('#quantity_' + counter).val(quantity);
    $tr.find('#label_quantity_' + counter).autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
    $tr.find('#label_quantity_' + counter).autoNumeric('set', quantity).show();
    $tr.find('#disp_quantity_' + counter).remove();

    $(".button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
    $(".button_edit").html('<i class="fa fa-edit"></i>');
    $(".button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
    $(".button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list").prop('disabled', false);
    $("#button_tambah_list").focus();

    $('#simpan1').prop('disabled', false);
    $('#simpan2').prop('disabled', false);
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    return false;
  });

  $('#IDTABLE_table').on('click', '.button_edit_batal', function() {

    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    $tr.find('#disp_kode_' + counter).remove();
    $tr.find('#label_kode_' + counter).show();

    $tr.find('#div_barang_' + counter).remove();
    $tr.find('#label_barang_' + counter).show();

    $tr.find('#disp_quantity_' + counter).remove();
    $tr.find('#label_quantity_' + counter).show();

    $(".button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
    $(".button_edit").html('<i class="fa fa-edit"></i>');
    $(".button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
    $(".button_hapus").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit").prop('disabled', false);
    $(".button_hapus").prop('disabled', false);

    $("#button_tambah_list").prop('disabled', false);
    $("#button_tambah_list").focus();

    $('#simpan1').prop('disabled', false);
    $('#simpan2').prop('disabled', false);
    $('#batal1').prop('disabled', false);
    $('#batal2').prop('disabled', false);

    return false;
  });

  $("#IDTABLE_table").on("click", ".cari_barang", function() {
    $tr = $(this).parent().parent();
    console.log($(this).val());
    obat_current_counter = $(this).val();
    var counter = getCounter($tr);
    console.log("COUNTER", counter);
    $('#counter_barang').val(counter);
    get_data_obat('/ok/pelaksanaan/get_obat','dataTable_barang','pilihObat');
    $("#barang_modal").modal('show');
  });
});
/** End of Obat **/

/** Init Variable Pengguanaan BHP **/
var bhp_current_counter = 0;
/** Pilih BHP in Modal **/
function pilihBHP(element){
    // var counter = parseInt($("#counter_bhp").val());
    var counter = bhp_current_counter;
    var obat = [];
    obat['obat_id'] = $('.obat_id',element).val();
    obat['kode'] = $('.kode',element).val();
    obat['nama'] = $('.nama',element).val();
    obat['satuan'] = $('.satuan',element).val();

    $("#bhp_id_"+counter).val(obat['obat_id']);
    $("#bhp_disp_kode_"+counter).val(obat['kode']);
    $("#bhp_disp_barang_"+counter).val(obat['nama']);
    $("#bhp_label_satuan_"+counter).html(obat['satuan']);

    $("#barang_modal_bhp").modal('hide');
}
/** Penggunaan BHP **/
$(document).ready(function() {
  $("#button_tambah_list1").click(function() {
    var $tr = $('#row_IDTABLE1_detail_clone').clone();

    var counter = parseInt($("#counter_bhp").val());
    counter++;
    $("#counter_bhp").val(counter);

    $tr.attr('id', '')
       .attr('style', '');
    $tr.find('.cari_barang').val(counter);
    $tr.find('#clone_IDTABLE1_detail_id')
      .attr('id', 'IDTABLE1_detail_id_' + counter)
      .attr('name', 'IDTABLE1_detail_id[]')
      .attr('value', 'new_IDTABLE1_detail_id_' + counter);

    $tr.find('#clone_bhp_id')
      .attr('id', 'bhp_id_' + counter)
      .attr('name', 'bhp_id[]')
      .attr('value', 0);

    $tr.find('#clone_bhp_kode')
      .attr('id', 'bhp_kode_' + counter)
      .attr('name', 'bhp_kode[]');

    $tr.find('#clone_bhp_nama')
      .attr('id', 'bhp_nama_' + counter)
      .attr('name', 'bhp_nama[]');

    $tr.find('#clone_bhp_satuan')
      .attr('id', 'bhp_satuan_' + counter)
      .attr('name', 'bhp_satuan[]');

    $tr.find('#clone_bhp_label_kode1')
      .attr('id', 'bhp_label_kode_' + counter);

    $tr.find('#clone_bhp_disp_kode1')
      .attr('id', 'bhp_disp_kode_' + counter)
      .attr('placeholder', 'Kode ...');

    $tr.find('#clone_label_barang1')
      .attr('id', 'label_barang_' + counter);

    $tr.find('#clone_div_barang1')
      .attr('id', 'div_barang_' + counter);

    $tr.find('#clone_bhp_disp_barang1')
      .attr('id', 'bhp_disp_barang_' + counter)
      .attr('placeholder', 'Nama Barang ...');

    $tr.find('#clone_bhp_label_satuan')
      .attr('id', 'bhp_label_satuan_' + counter);

    $tr.find('#clone_bhp_quantity')
      .attr('id', 'bhp_quantity_' + counter)
      .attr('name', 'bhp_quantity[]')
      .attr('value', 1);

    $tr.find('#clone_label_bhp_quantity')
      .attr('id', 'label_bhp_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $tr.find('#clone_disp_bhp_quantity1')
      .attr('id', 'disp_bhp_quantity_' + counter)
      .attr('value', 1)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

    $("#IDTABLE1_table").find("tbody").append($tr);

    $("#button_11").attr('id', '').addClass('button_tambah_simpan1').html('<i class="fa fa-save"></i>');
    $("#button_21").attr('id', '').addClass('button_tambah_batal1').html('<i class="fa fa-times"></i>');

    $(".button_edit1").prop('disabled', true);
    $(".button_hapus1").prop('disabled', true);

    $("#button_tambah_list1").prop('disabled', true);

    $('#simpan11').prop('disabled', true);
    $('#simpan21').prop('disabled', true);
    $('#batal11').prop('disabled', true);
    $('#batal21').prop('disabled', true);

    $tr.find('.select_kode').focus();

    return false;
  });

  $('#IDTABLE1_table').on('click', '.button_tambah_simpan1', function() {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    var kode = $tr.find('#bhp_disp_kode_' + counter).val();
    $tr.find('#bhp_label_kode_' + counter).text(kode).show();
    $tr.find('#bhp_disp_kode_' + counter).remove();
    $tr.find('#bhp_kode_' + counter).val(kode);

    var barang = $tr.find('#bhp_disp_barang_' + counter).val();
    $tr.find('#bhp_nama_' + counter).val(barang);
    $tr.find('#label_barang_' + counter).text(barang).show();
    $tr.find('#div_barang_' + counter).remove();

    var satuan = $tr.find('#bhp_label_satuan_' + counter).text();
    $tr.find('#bhp_satuan_' + counter).val(satuan);

    var bhp_quantity = $tr.find('#disp_bhp_quantity_' + counter).autoNumeric('get');
    $tr.find('#bhp_quantity_' + counter).val(bhp_quantity);
    $tr.find('#label_bhp_quantity_' + counter).autoNumeric('set', bhp_quantity).show();
    $tr.find('#disp_bhp_quantity_' + counter).remove();

    // $(".button_tambah_simpan1").remove();
    $(".button_tambah_simpan1").removeClass('button_tambah_simpan1').addClass('button_edit1');
    $(".button_edit1").html('<i class="fa fa-edit"></i>');
    $(".button_tambah_batal1").removeClass('button_tambah_batal1').addClass('button_hapus1');
    $(".button_hapus1").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit1").prop('disabled', false);
    $(".button_hapus1").prop('disabled', false);

    $("#button_tambah_list1").prop('disabled', false);
    $("#button_tambah_list1").focus();

    $('#simpan11').prop('disabled', false);
    $('#simpan21').prop('disabled', false);
    $('#batal11').prop('disabled', false);
    $('#batal21').prop('disabled', false);

    return false;
  });

  $('#IDTABLE1_table').on('click', '.button_tambah_batal1', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();
    $tr.remove();

    $(".button_edit1").prop('disabled', false);
    $(".button_hapus1").prop('disabled', false);

    $("#button_tambah_list1").prop('disabled', false);
    $("#button_tambah_list1").focus();

    $('.close').show();
    $('#batal11').prop('disabled', false);
    $('#batal21').prop('disabled', false);

    tbody = $("#IDTABLE1_table").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#simpan11').prop('disabled', true);
      $('#simpan21').prop('disabled', true);
    } else {
      $('#simpan11').prop('disabled', false);
      $('#simpan21').prop('disabled', false);
    }

    return false;
  });

  $('#IDTABLE1_table').on('click', '.button_edit1', function() {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    /*var kode = $tr.find('#bhp_label_kode_' + counter).text();
    $tr.find('#bhp_label_kode_' + counter).hide();
    $tr.children('td').eq(0).append('<input id="bhp_disp_kode_' + counter + '" class="select_kode form-control" type="text" value="' + kode + '" placeholder="Kode ..." />');

    var barang = $tr.find('#label_barang_' + counter).text();
    $tr.find('#label_barang_' + counter).hide();
    $tr.children('td').eq(1).append('<div id="div_barang_' + counter + '" class="col-sm-12 no-margin">' +
                      '<div class="input-group">' +
                        '<input class="form-control select_barang" type="text" id="bhp_disp_barang_' + counter + '" value="' + barang + '" placeholder="barang ..." />' +
                        '<div class="input-group-btn">' +
                        '<button type="button" class="cari_barang btn btn-primary" value="'+ counter +'"><i class="fa fa-search"></i></button></div>' +
                      '</div>' +
                    '</div>');*/

    var bhp_quantity = $tr.find('#bhp_quantity_' + counter).val();
    $tr.find('#label_bhp_quantity_' + counter).hide();
    $tr.children('td').eq(3).append('<input id="disp_bhp_quantity_' + counter + '" class="bhp_quantity-row form-control" type="text" value="' + bhp_quantity + '" style="text-align: right;" />');
    $tr.find('#disp_bhp_quantity_' + counter)
      .autoNumeric('init', {aSep: '.', aDec:',', aPad: false})
      .focus()
      .select();

    $tr.find(".button_edit1").removeClass('button_edit1').addClass('button_edit1_simpan');
    $tr.find(".button_edit1_simpan").html('<i class="fa fa-save"></i>');
    $tr.find(".button_hapus1").removeClass('button_hapus1').addClass('button_edit1_batal');
    $tr.find(".button_edit1_batal").html('<i class="fa fa-times"></i>');

    $(".button_edit1").prop('disabled', true);
    $(".button_hapus1").prop('disabled', true);

    $("#button_tambah_list1").prop('disabled', true);

    $('#simpan11').prop('disabled', true);
    $('#simpan21').prop('disabled', true);
    $('#batal11').prop('disabled', true);
    $('#batal21').prop('disabled', true);

    return false;
  });

  $('#IDTABLE1_table').on('click', '.button_hapus1', function(event) {
    event.preventDefault;
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);
    $tr.remove();

        $("#button_tambah_list1").focus();

        tbody = $("#IDTABLE1_table").find('tbody');
    length = tbody.children().length;

    if(length == 0) {
      $('#simpan11').prop('disabled', true);
      $('#simpan21').prop('disabled', true);
    } else {
      $('#simpan11').prop('disabled', false);
      $('#simpan21').prop('disabled', false);
    }
    return false;
  });

  $('#IDTABLE1_table').on('click', '.button_edit1_simpan', function(event) {
    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    // var kode = $tr.find('#bhp_disp_kode_' + counter).val();
    // $tr.find('#bhp_label_kode_' + counter).text(kode).show();
    // $tr.find('#bhp_disp_kode_' + counter).remove();

    // var barang = $tr.find('#bhp_disp_barang_' + counter).val();
    // $tr.find('#div_barang_' + counter).remove();
    // $tr.find('#label_barang_' + counter).text(barang).show();

    var bhp_quantity = $tr.find('#disp_bhp_quantity_' + counter).autoNumeric('get');
    $tr.find('#bhp_quantity_' + counter).val(bhp_quantity);
    $tr.find('#label_bhp_quantity_' + counter).autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
    $tr.find('#label_bhp_quantity_' + counter).autoNumeric('set', bhp_quantity).show();
    $tr.find('#disp_bhp_quantity_' + counter).remove();

    $(".button_edit1_simpan").removeClass('button_edit1_simpan').addClass('button_edit1');
    $(".button_edit1").html('<i class="fa fa-edit"></i>');
    $(".button_edit1_batal").removeClass('button_edit1_batal').addClass('button_hapus1');
    $(".button_hapus1").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit1").prop('disabled', false);
    $(".button_hapus1").prop('disabled', false);

    $("#button_tambah_list1").prop('disabled', false);
    $("#button_tambah_list1").focus();

    $('#simpan11').prop('disabled', false);
    $('#simpan21').prop('disabled', false);
    $('#batal11').prop('disabled', false);
    $('#batal21').prop('disabled', false);

    return false;
  });

  $('#IDTABLE1_table').on('click', '.button_edit1_batal', function() {

    $tr = $(this).parent().parent();

    var counter = getCounter($tr);

    $tr.find('#bhp_disp_kode_' + counter).remove();
    $tr.find('#bhp_label_kode_' + counter).show();

    $tr.find('#div_barang_' + counter).remove();
    $tr.find('#label_barang_' + counter).show();

    $tr.find('#disp_bhp_quantity_' + counter).remove();
    $tr.find('#label_bhp_quantity_' + counter).show();

    $(".button_edit1_simpan").removeClass('button_edit1_simpan').addClass('button_edit1');
    $(".button_edit1").html('<i class="fa fa-edit"></i>');
    $(".button_edit1_batal").removeClass('button_edit1_batal').addClass('button_hapus1');
    $(".button_hapus1").html('<i class="fa fa-trash-o"></i>');

    $(".button_edit1").prop('disabled', false);
    $(".button_hapus1").prop('disabled', false);

    $("#button_tambah_list1").prop('disabled', false);
    $("#button_tambah_list1").focus();

    $('#simpan11').prop('disabled', false);
    $('#simpan21').prop('disabled', false);
    $('#batal11').prop('disabled', false);
    $('#batal21').prop('disabled', false);

    return false;
  });

  $("#IDTABLE1_table").on("click", ".cari_barang", function() {
    console.log($(this).val());
    bhp_current_counter = $(this).val();
    $tr = $(this).parent().parent();
    var counter = getCounter($tr);
    $('#counter1_barang').val(counter);
    get_data_obat('/ok/pelaksanaan/get_bhp','dataTable_barang_bhp','pilihBHP');
    $("#barang_modal_bhp").modal('show');
  });
});
/** End of Penggunaan BHP **/

/** Function **/
function getCounter(tr) {
  var id = tr.find('input').attr('id');
  var aId = id.split('_');
  return parseInt(aId[aId.length - 1]);
}

function viewDetailTarif(element)
{
  $tr = $(element).parent().parent();
  var id_tindakan = $tr.find('td input[name="tindakan_id[]"]').val();
  var dokter_anastesi = $(document).find('#dokter_anastesi').val();
  var asisten_anastesi = $(document).find('#asisten_anastesi').val();
  var dokter_konsulen = $(document).find('#dokter_konsulen').val();
  var dokter_operator = $(document).find('#dokter_operator').val();
  var asisten_operator = $(document).find('#asisten_operator').val();

  var dokter = '';
  if(dokter_operator)
    dokter = "dokter_1";
  if(asisten_operator){
    if(dokter) dokter += ",";
    dokter += "dokter_2";
  }
  if(dokter_konsulen){
    if(dokter) dokter += ",";
    dokter += "dokter_3";
  }
  if(dokter_anastesi){
    if(dokter) dokter += ",";
    dokter += "dokter_4";
  }

  console.log('dokter ',dokter);
  var dr = '';
  if(dokter)
    dr = ('&dokter='+dokter);

  $.ajax({
    "url": url_detail_tarif_pelayanan+dr,
    "type": 'POST',
    "dataType": 'JSON',
    "data": {
      tindakan_id : id_tindakan,
    },
    "success":function(data){
      var detail = data.data;
      console.log('detail tarif ',data);
      $detail = $("#detail_tarif_modal");
      var nama_tindakan = $tr.find('td:eq(1) label').text();

      $detail.find('#nama_tindakan').text(nama_tindakan);
      $detail.find('#tarif_dasar').text('Rp. '+numeral(detail.tarif_dasar).format('0,0'));
      $detail.find('#dokter_operator .tarif').text('Rp. '+numeral(detail.dokter_1).format('0,0'));
      $detail.find('#dokter_pendamping .tarif').text('Rp. '+numeral(detail.dokter_2).format('0,0'));
      $detail.find('#dokter_anethesi .tarif').text('Rp. '+numeral(detail.dokter_3).format('0,0'));
      $detail.find('#penata .tarif').text('Rp. '+numeral(detail.dokter_4).format('0,0'));
      $detail.find('#insentif_karyawan .tarif').text('Rp. '+numeral(detail.insentif_karyawan).format('0,0'));
      $detail.find('#rs .tarif').text('Rp. '+numeral(detail.rumah_sakit).format('0,0'));
      $detail.find('#jasa_sarana .tarif').text('Rp. '+numeral(detail.jasa_sarana).format('0,0'));

      $detail.find('#dokter_operator .persen').text(detail.persen_dokter_1+'%');
      $detail.find('#dokter_pendamping .persen').text(detail.persen_dokter_2+'%');
      $detail.find('#dokter_anethesi .persen').text(detail.persen_dokter_3+'%');
      $detail.find('#penata .persen').text(detail.persen_dokter_4+'%');
      $detail.find('#insentif_karyawan .persen').text(detail.persen_insentif_karyawan+'%');
      $detail.find('#rs .persen').text(detail.persen_rumah_sakit+'%');
      $detail.find('#jasa_sarana .persen').text(detail.persen_jasa_sarana+'%');

      var dr_operator       = $("#dokter_operator").tagsinput('items');
      var dr_pendamping     = $("#asisten_operator").tagsinput('items');
      var dr_anastesi       = $("#dokter_anastesi").tagsinput('items');
      var asisten_anastesi  = $("#asisten_anastesi").tagsinput('items');

      /*var dokter_1 = '';
      dr_operator.forEach(function(i,v){
        if(i>0)
          dokter_1 = ', ';
        dokter_1 += v.text;
      });*/
      var dokter_1 = $tr.find('td:eq(4) label').text();

      /*dr_pendamping.forEach(function(v,i){
        console.log(v);
        if(i>0)
          dokter_2 = ', ';
        dokter_2 += v.text;
      });*/
      var dokter_2 = '';
      for (var i = 0, len = dr_pendamping.length; i < len; i++) {
        if(i>0)
          dokter_2 += '<br>';
        dokter_2 += dr_pendamping[i].text;
      }

      var dokter_3 = '';
      dr_anastesi.forEach(function(v,i){
        if(i>0)
          dokter_3 += '<br>';
        dokter_3 += v.text;
      });

      var dokter_4 = '';
      asisten_anastesi.forEach(function(v,i){
        if(i>0)
          dokter_4 += '<br>';
        dokter_4 += v.text;
      });

      $detail.find('#dokter_operator .dokter').html(dokter_1);
      $detail.find('#dokter_pendamping .dokter').html(dokter_2);
      $detail.find('#dokter_anethesi .dokter').html(dokter_3);
      $detail.find('#penata .dokter').html(dokter_4);

      $detail.find('#jumlah .tarif').text('Rp. '+numeral(detail.tarif).format('0,0'));
      $detail.modal('show');
    }
  });
}

function get_data(dokter){
  if(typeof tableTindakan != 'undefined'){
    tableTindakan.destroy();
  }

  var dr = '';
  console.log('dokter',dokter);
  if(dokter)
    dr = ('&dokter='+dokter);
  tableTindakan = $("#dataTable_barang_tindakan").DataTable({
    "pageLength"  : 50,
    "displayStart"  : page,
    "processing"  : true,
    "serverSide"  : true,
    "ajax"      : {
      "url": url_load_data_tarif_pelayanan+dr,
      "type": "GET",
      "data"    : function(d) {
        d.root_uid = $('#root_uid').val();
      },
    },
    "order"         : [[ 2, "asc" ]],
    "columns": [
    { "visible": false, "targets": [ 0 ] },
    { "visible": false, "targets": [ 1 ] },
    { "visible": false, "targets": [ 2 ] },
    {
      "width": "10%",
      "render": function(data, type, row, meta) {
        var sLink = '';
        console.log(row);
        switch (row[1]) {
          case 'Root':
          sLink = '<strong>' + data + '</strong>';
          break;
          case 'Kelompok':
          case 'Rincian':
          sLink = data;
          break;
        }
        return sLink;
      },
      "targets": [ 3 ]
    },
    {
      "render": function(data, type, row, meta) {
        var sLink = '';
        switch (row[1]) {
          case 'Root':
          sLink = '<span class="nama"><strong>' + data + '</strong></span>';
          break;
          case 'Kelompok':
          case 'Rincian':
          sLink = data;
          console.log(row[1],data,row[9]);
          if(row[9] == '')
          {
            sLink = '<div class="text-primary" style="cursor:pointer" onClick="pilihTindakan(this)">'
                      +'<input type="hidden" class="obat_id" value="'+row[6]+'">'
                      +'<input type="hidden" class="kode" value="'+row[3]+'">'
                      +'<input type="hidden" class="tarif" value="'+row[2]+'">'
                      +'<input type="hidden" class="parent" value="'+row[7]+'">'
                      +'<input type="hidden" class="nama_asli" value="'+row[8]+'">'
                      +'<input type="hidden" class="nama_operasi" value="">'
                      +'<span>'+data+'</span>'
                      +'</div>';
          }
          else
          {
            var detail = row[9].split(',');
            detail.forEach(function(v,i){
              sLink += '<div class="text-primary" style="cursor:pointer" onClick="pilihTindakan(this)">'
                      +'<input type="hidden" class="obat_id" value="'+row[6]+'">'
                      +'<input type="hidden" class="kode" value="'+row[3]+'">'
                      +'<input type="hidden" class="tarif" value="'+row[2]+'">'
                      +'<input type="hidden" class="parent" value="'+row[7]+'">'
                      +'<input type="hidden" class="nama_asli" value="'+row[8]+'">'
                      +'<input type="hidden" class="nama_operasi" value="'+v+'">'
                      +'<span>'+v+'</span>'
                      +'</div>';
            });
          }
          break;
        }
        return sLink;
        // return '<span class="nama">'+data+'<br>'+detail+'</span>';
      },
      "targets": [ 4 ]
    },
    {
      "width": "15%",
      "orderable": false,
      "render": function(data, type, row, meta) {
        return numeral(data).format('0,0');
      },
      "data": 2
    }
    ]
  });
}

function get_data_obat(url,desTable,actionClick){
    console.log('run');
    if(typeof tableTindakan != 'undefined'){
        tableTindakan.destroy();
    }
    tableTindakan = $("#"+desTable).DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+url, //'/ok/pelaksanaan/get_tindakan'
            "type": "POST",
            "dataType":"json",
            "data":{ruang_operasi:ruang_operasi}
        },
        "columns": [
            {
                "data": "kode",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" onClick="'+actionClick+'(this)">'
                    +'<input type="hidden" class="obat_id" value="'+row.obat_id+'">'
                    +'<input type="hidden" class="kode" value="'+row.kode+'">'
                    +'<input type="hidden" class="nama" value="'+row.nama+'">'
                    +'<input type="hidden" class="satuan" value="'+row.satuan+'">'
                    +'<span class="nama_kode">'+row.kode+'</span>'
                    +'</span>';
                }
            },
            {
                "data": "nama",
            },
            {
                "data": "satuan",
            }
        ]
    });
}
/** --End Of Function-- **/

/** --Select Operasi-- **/
$(function(){
  $('#jenis_operasi').change(function(){
    var jenis_operasi = $(this).val();
    console.log('jenis_operasi ',jenis_operasi);
    var parent_id     = $('#operasi').val();
    console.log('operasi ',parent_id);
    $.ajax({
      url: base_url+'/ok/registrasi/select_nama_operasi',
      dataType: 'json',
      type: 'POST',
      data: {
        parent_id: parent_id,
        jenis_operasi: jenis_operasi
      }
    }).then(function(data){
      console.log(data);
      $('#nama_operasi').empty();
      // $('#nama_operasi').append('<option value="">-Pilih-</option>');
      $.each(data,function(i,v){
        console.log(v);
        $('#nama_operasi').append('<option>'+v+'</option>');
      });
      // $('#nama_operasi').addClass('select2');
    });
  });

  $('#operasi').change(function(){
      console.log('hadir')
      $('#nama_operasi option').remove();
      $("#nama_operasi").select2();
      $("#jenis_operasi").val('');

      var operasi = $(this).val();
      var parent_id = operasi;
      $.ajax({
        url: base_url+'/ok/registrasi/select_jenis_operasi',
        dataType: 'json',
        type: 'POST',
        data: {
          parent_id: parent_id
        }
      }).then(function(data){
        console.log(data);
        $('#jenis_operasi').empty();
        // $('#jenis_operasi').append('<option value="">-Pilih-</option>');
        $.each(data,function(i,v){
          console.log(v);
          $('#jenis_operasi').append('<option>'+v+'</option>');
        });
    });
  });
});
/** --End Of Select Operasi-- **/

function set_tarif(){
  var counter_tindakan = $('#counter_tindakan').val();
  var dokter_anastesi = $(document).find('#dokter_anastesi').val();
  var asisten_anastesi = $(document).find('#asisten_anastesi').val();
  var dokter_konsulen = $(document).find('#dokter_konsulen').val();
  var dokter_operator = $(document).find('#dokter_operator').val();
  var asisten_operator = $(document).find('#asisten_operator').val();
  var dokter = '';
  if(dokter_operator)
    dokter = "dokter_1";
  if(asisten_operator){
    if(dokter) dokter += ",";
    dokter += "dokter_2";
  }
  if(dokter_konsulen){
    if(dokter) dokter += ",";
    dokter += "dokter_3";
  }
  if(dokter_anastesi){
    if(dokter) dokter += ",";
    dokter += "dokter_4";
  }
  var dr = '';
  console.log('dokter',dokter);
  if(dokter)
    dr = ('&dokter='+dokter);

  $table = $('#IDTABLE_tindakan').find('tbody tr');
  $table.each(function(i,v){
    var baris = i+1;
    var tindakan_id = $(v).find("#tindakan_id_"+baris).val();
    var dokter_1    = $(v).find("#tindakan_dokter_id_"+baris).val();
    console.log(tindakan_id);
    console.log('baris ',baris);
    var dokter_1_discount = 0;
    $table.each(function(j,vd){
      var row = j+1;
      var dokter_bd    = $(vd).find("#tindakan_dokter_id_"+row).val();
      if(i != j)
      {
        if(dokter_1 == dokter_bd)
        {
          dokter_1_discount = 75;
        }
      }
    });

    $.ajax({
      "url":url_load_get_tarif_pelayanan+dr,
      "type": "GET",
      "dataType": "json",
      "data": {"tarif_id":tindakan_id,"dokter_1_discount":dokter_1_discount},
      "success":function(d){
        $Inputtarif = $(v).find("#tindakan_tarif_"+baris);
        $Labeltarif = $(v).find("#tindakan_label_tarif_"+baris);
        console.log('baris ',baris);
        $Inputtarif.val(d.tarif);
        $Labeltarif.text(numeral(d.tarif).format('0,0'));
        console.log(d);
        console.log('format number ',numeral(d.tarif).format('0,0'));
      }
    });
  });
}
