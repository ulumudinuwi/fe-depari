app.controller('PemeriksaanAwalFormController', ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$location',
    function ($rootScope, $scope, api, $http, CONFIG, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $location) {
        var url = base_url + '/api/rawat_jalan/pemeriksaan_awal/list_pemeriksaan';

        $scope.rows = [];
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                url: url,
                type: 'POST',
                data: function (data, dtInstance) {

                }
            })
            // or here
            .withDataProp('data')
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('order', [[0, 'desc']])
            .withOption('createdRow', function (row) {
                $rootScope.$emit('ajax:stop');
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);

            })
            .withPaginationType('full_numbers');

        $scope.dtInstance = {};

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('tanggal').withTitle('Urutan').withClass('text-center').renderWith(function (data, type, row, meta) {
                return meta.row + 1;
            }),
            DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {
                return moment(row.tanggal).format('DD-MMM-YYYY H:mm:s') + '<br /><i><small>' + moment(row.tanggal).fromNow() + '</i></small>';
                // return '<a href="' + base_url + '/rawat_jalan/pemeriksaan_awal?s=' + row.pelayanan_id + '&rawat_jalan=' + row.id + '">' + moment(row.tanggal).format('DD-MMM-YYYY H:mm:s') + '</a>';
            }).withOption('searchable', false),
            DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No RM'),
            DTColumnBuilder.newColumn('no_register').withTitle('No Register').renderWith(function (data, type, row, meta) {
                $scope.rows.push(row);

                return '<a  ng-click="goToPemeriksaanAwal(' + row.id + ')">' + data + '</a>';
            }),
            DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
            DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
            DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter')
        ];
        $scope.dtInstance = {};
        $scope.goToPemeriksaanAwal = function (id) {
            window.location.href = base_url + '/rawat_jalan/pemeriksaan_awal/index/' + id;
        };

        $scope.onChangeTanggal = onChangeTanggal;

        function onChangeTanggal(tanggal){
            $scope.dtOptions.ajax.url = url + "?range-tanggal=" + tanggal;
            $scope.dtInstance.rerender();
        }

        $(document).ready(function(){

            $('#range-tanggal').daterangepicker();
        })

    }


]);