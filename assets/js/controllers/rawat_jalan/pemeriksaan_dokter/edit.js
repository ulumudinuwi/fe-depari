app.controller('PemeriksaanDokterFormController', pemeriksaanDokterFormController);
pemeriksaanDokterFormController.$inject = ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$uibModal', '$log', 'EVENT', '$q', '$timeout'];
function pemeriksaanDokterFormController($rootScope, $scope, api, $http, CONFIG, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $uibModal, $log, EVENT, $q, $timeout) {

    var id = window.location.pathname.match(/([^\/]*)\/*$/)[1];

    //rootscope var
    $rootScope.rujukan = [];

    // initial vars
    $scope.data = {};
    // $scope.data.rujukan = {
    //     rujuk_ok: 0,
    //     rujuk_rawatinap: 0,
    //     rujuk_audiometri: 0,
    //     rujuk_cathlab: 0,
    //     rujuk_ctscan: 0,
    //     kunjungan_ulang:0,
    //     rujuk_diagnostik_fungsional: 0,
    //     rujuk_fisioterapi: 0,
    //     rujuk_laboratorium: 0,
    //     rujuk_ods_odc: 0,
    //     rujuk_radiologi: 0
    // };
    $scope.data.rujuk = [];

    $scope.dataTindakan = [];
    $scope.dataRujukan = [];
    $scope.dataBphp = [];
    $scope.dataObat = [];
    $scope.dataFarmasi = [];
    $scope.dataLaboratorium = [];
    $scope.dataRacikanInduk = [];
    $scope.racikan = {};
    $scope.rawatInap = {};
    $scope.farmasi = {};
    $scope.laboratorium = {};
    $scope.tarifTotalRacikanInduk = 0;
    $scope.catatan = "";

    // boolean vars
    $scope.isTindakanEdit = false;
    $scope.isBphpEdit = false;
    $scope.isObatEdit = false;
    $scope.isOpenAddRacikan = false;
    $scope.isSelectedPasien = false;
    $scope.isSubmit = false;


    // function vars
    $scope.addRacikan = addRacikan;
    $scope.openModalAddTindakan = addTindakan;
    $scope.openModalAddBphp = addBphp;
    $scope.openModalAddNonRacikan = addNonRacikan;
    $scope.openModalAddFarmasi = addFarmasi;


    $scope.openModalAddLaboratorium = addLaboratorium;
    $scope.openModalAddRadiologi = addRadiologi;
    $scope.openModalAddCtScan = addCtScan;
    $scope.openModalAddFisioterapi = addFisioterapi;
    $scope.openModalAddCatchLab = addCatchLab;
    $scope.openModalAddAudiometri = addAudiometri;
    $scope.openModalAddDiagnostikFungsional = addDiagnostikFungsional;
    $scope.openModalAddOdsOdc = addOdsOdc;

    $scope.openModalAddRawatInap = addRawatInap;
    $scope.openModalReservasiOK = reservasiOk;
    $scope.openModalAddKunjunganUlang = kunjunganUlang;
    $scope.deleteTindakan = deleteTindakan;
    $scope.deleteRacikan = deleteRacikan;
    $scope.deleteBphp = deleteBphp;
    $scope.deleteObat = deleteObat;
    $scope.deleteFarmasi = deleteFarmasi;
    $scope.deleteLaboratorium = deleteLaboratorium;
    $scope.racikanQtyChange = racikanQtyChange;
    $scope.racikanDiskonChange = racikanDiskonChange;
    $scope.tindakanQtyChange = tindakanQtyChange;
    $scope.tindakanDiskonChange = tindakanDiskonChange;
    $scope.bphpQtyChange = bphpQtyChange;
    $scope.bphpDiskonChange = bphpDiskonChange;
    $scope.obatQtyChange = obatQtyChange;
    $scope.obatDiskonChange = obatDiskonChange;
    $scope.selected = selected;
    $scope.clear = clear;
    $scope.save = save;
    // init function
    buildTable();
    initData();
    selected(id);


    function initData(param, cb) {

        if (typeof param === 'object' || param) {
            $q.all([
                $http.get(base_url + '/api/rawat_jalan/pemeriksaan_dokter/tindakan_by_pasien/' + param.pasien.id)
            ]).then(function (results) {


                if (Array.isArray(results[0].data)) {
                    $scope.dataTindakan = _.map(results[0].data, function (val) {
                        val.data_mode = 2;
                        val.pasien_id = param.pasien.id;
                        val.no_register = param.pasien.no_register;
                        val.layanan_id = $scope.data.layanan_id;
                        val.nama = val.nama_tarif_pelayanan;

                        if (parseInt(val.tarif) == 0) {
                            val.tarif = parseInt(val.jasa_sarana) + parseInt(val.dokter_1) + parseInt(val.dokter_2) + parseInt(val.dokter_3) + parseInt(val.dokter_4) + parseInt(val.insentif_karyawan) + parseInt(val.rumah_sakit) + parseInt(val.lain_lain);
                        }

                        val.qty = parseInt(val.quantity);
                        val.biaya = parseInt((val.tarif * val.quantity));
                        val.total = parseInt((val.tarif * val.quantity));
                        return val;
                    });
                }

                cb();

            });
        }
    }

    function addRacikan(racikan, isEdit, idx) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddRacikan.html',
            controller: 'ModalAddRacikanController',
            size: 'lg',
            resolve: {
                items: function () {
                    return {
                        isEdit: isEdit,
                        racikan: racikan.nama,
                        data: $scope.data,
                        dataObat: racikan,
                        catatan: $scope.catatan,
                        idx: (idx) ? idx : undefined
                    };
                }
            }
        });

        modalInstance.result.then(then, dismiss);

        function then(selectedItem) {

            console.log("hasil dari racikan obat", selectedItem);

            // $scope.selected = selectedItem;
            var _tarif = {
                racikan: racikan.nama,
                tarif: [],
                biaya: 0,

            };

            var totalBiaya = 0;


            // get signa
            if (selectedItem.racikan.signa || selectedItem.racikan.signa.length >= 1) {
                var _signa = [];
            } else {
                toastr.warn('Belum Ada Signa', 'Peringatan!');
            }

            _tarif.tarif = _.map(selectedItem.racikan.dataObat, function (val) {
                val.biaya = val.harga;
                return val;
            });
            _tarif.biaya += _.sum(_.map(selectedItem.racikan.dataObat, function (data) {
                console.log("racikan", data);
                return parseFloat(data.jumlah);
            }));

            _tarif.qty = 1;
            _tarif.total = _tarif.biaya;
            _tarif.signa = selectedItem.racikan.signa;

            var signaTask = [];
            _.each(_tarif.signa, function (item) {
                signaTask.push($http.get(base_url + '/api/master/signa/get_by_id/' + item));
            });


            $q.all(signaTask).then(function (results) {
                _tarif.signa = _.map(results, function (result) {
                    return result.data;
                });

                _tarif.signa_label = "";
                _tarif.catatan = "-";
                _.each(_tarif.signa, function (signa) {
                    _tarif.signa_label += signa.label + ", ";
                });

                //if (selectedItem.catatan) _tarif.signa_label += selectedItem.catatan;
                if (selectedItem.catatan) _tarif.catatan = selectedItem.catatan;
                $scope.catatan = _tarif.catatan;

                $scope.isOpenAddRacikan = false;
                _tarif.totalObatRacikan = selectedItem.racikan.dataObat.length;
                _tarif.nama = selectedItem.racikan.nama;
                _tarif.isEdit = selectedItem.racikan.isEdit;
                _tarif.idx = selectedItem.racikan.idx;
                console.log("racikan sekarang", _tarif);
                if (_tarif.isEdit) {
                    $scope.dataRacikanInduk[_tarif.idx] = _tarif;
                } else {
                    $scope.dataRacikanInduk.push(_tarif);
                }
            });


        }

    }

    function addTindakan(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddTindakan.html',
            controller: 'ModalAddTindakanController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.kelas = 0; // default rawat jalan
                    $scope.data.jenis_dokter = 1; // default dokter rawat jalan

                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            _.each(selectedItem, function (data) {
                // marshall bmhp
                if (data.bmhp) {
                    _.each(data.bmhp, function(bmhp){
                        bmhp.obat = bmhp.nama;
                        bmhp.qty = parseInt(bmhp.quantity);
                        bmhp.harga = bmhp.tarif;
                        bmhp.biaya = bmhp.tarif;
                        bmhp.stock = parseInt(bmhp.jumlah);
                        bmhp.diskon = 0;
                        bmhp.paket = true; // karena harga tarif belum termasuk bmhp
                        $scope.dataBphp.push(bmhp);
                    });
                }

                delete data.bmhp;
                data.data_mode = 1;
                $scope.dataTindakan.push(data);
            });

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addBphp(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddBphp.html',
            controller: 'ModalAddBphpController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            _.each(selectedItem, function (data) {
                data.biaya = data.harga;
                data.qty = 1;
                data.diskon = 0;
                $scope.dataBphp.push(data);
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addNonRacikan(isEdit, idx) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddNonRacikan.html',
            controller: 'ModalAddNonRacikanController',
            size: 'lg',
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan,
                        isEdit: isEdit,
                        idx: idx,
                        catatan: $scope.catatan,
                        dataObat: (idx > -1) ? angular.copy($scope.dataObat[idx]) : []
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            console.log("non racikan", selectedItem);

            _.each(selectedItem.obat, function (data) {
                data.biaya = data.harga;
                data.total = data.harga * data.qty;

                var signaTask = [];
                _.each(data.signa, function (item) {
                    signaTask.push($http.get(base_url + '/api/master/signa/get_by_id/' + item));
                });


                $q.all(signaTask).then(function (results) {
                    data.signa = _.map(results, function (result) {
                        return result.data;
                    });

                    data.signa_label = "";
                    data.catatan = "-";
                    _.each(data.signa, function (signa) {
                        data.signa_label += signa.label + ", ";
                    });

                    //if (selectedItem.catatan) data.signa_label += selectedItem.catatan;
                    if (selectedItem.catatan) data.catatan = selectedItem.catatan;
                    $scope.catatan = data.catatan;

                    if (isEdit) {
                        $scope.dataObat[idx] = data;
                    } else {
                        $scope.dataObat.push(data);
                    }

                });


            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addFarmasi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddFarmasi.html',
            controller: 'ModalAddFarmasiController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            _.each(selectedItem, function (data) {
                $scope.dataFarmasi.push(data);
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addLaboratorium(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddLaboratorium.html',
            controller: 'ModalAddLaboratoriumController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.data.rujukan.rujuk_laboratorium = 1;
            $scope.data.laboratorium_uid = selectedItem.uid;

            _.each(selectedItem.data, function (o) {
                // $scope.data.tindakan.push(o);
                $scope.dataRujukan.push(o);
            });

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addRadiologi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddRadiologi.html',
            controller: 'ModalAddRadiologiController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_radiologi = 1;
            $scope.data.radiologi_uid = selectedItem.uid;

            var task = [];
            var tindakan = _.remove(selectedItem.data, function (o) {
                return o.jenis == 'Rincian';
            });

            _.each(tindakan, function (o) {
                // $scope.data.tindakan.push(o);
                task.push($http.get(base_url + '/api/master/tindakan/tarif_by_pelayanan', {
                    params: {
                        id: o.id,
                        kelas: 9
                    }
                }));
            });

            $q.all(task).then(function (results) {
                _.each(results, function (o) {
                    console.log(o);
                    // $scope.dataTindakan.push(o.data);
                    $scope.dataRujukan.push(o.data);
                });
            })
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addCtScan(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddCtScan.html',
            controller: 'ModalAddCtScanController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_ctscan = 1;
            $scope.data.ctscan_uid = selectedItem.uid;

            var task = [];
            var tindakan = _.remove(selectedItem.data, function (o) {
                return o.jenis == 'Rincian';
            });

            _.each(tindakan, function (o) {
                // $scope.data.tindakan.push(o);
                task.push($http.get(base_url + '/api/master/tindakan/tarif_by_pelayanan', {
                    params: {
                        id: o.id,
                        kelas: 9
                    }
                }));
            });

            $q.all(task).then(function (results) {
                _.each(results, function (o) {
                    $scope.dataTindakan.push(o.data);
                    $scope.dataRujukan.push(o.data);
                });
            })
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addFisioterapi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddFisioterapi.html',
            controller: 'ModalAddFisioterapiController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_fisioterapi = 1;
            $scope.data.fisioterapi_uid = selectedItem.uid;

            var task = [];
            var tindakan = _.remove(selectedItem.data, function (o) {
                return o.jenis == 'Rincian';
            });

            _.each(tindakan, function (o) {
                // $scope.data.tindakan.push(o);
                task.push($http.get(base_url + '/api/master/tindakan/tarif_by_pelayanan', {
                    params: {
                        id: o.id,
                        kelas: 9
                    }
                }));
            });

            $q.all(task).then(function (results) {
                _.each(results, function (o) {
                    $scope.dataTindakan.push(o.data);
                    $scope.dataRujukan.push(o.data);
                });
            })

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addCatchLab(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddCatchLab.html',
            controller: 'ModalAddCatchLabController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_cathlab = 1;
            $scope.data.cathlab_uid = selectedItem.uid;

            var task = [];
            var tindakan = _.remove(selectedItem.data, function (o) {
                return o.jenis == 'Rincian';
            });

            _.each(tindakan, function (o) {
                // $scope.data.tindakan.push(o);
                task.push($http.get(base_url + '/api/master/tindakan/tarif_by_pelayanan', {
                    params: {
                        id: o.id,
                        kelas: 9
                    }
                }));
            });

            $q.all(task).then(function (results) {
                _.each(results, function (o) {
                    $scope.dataTindakan.push(o.data);
                    $scope.dataRujukan.push(o.data);
                });
            })
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addAudiometri(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddAudiometri.html',
            controller: 'ModalAddAudiometriController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_audiometri = 1;
            $scope.data.audiometri_uid = selectedItem.uid;

            var task = [];
            var tindakan = _.remove(selectedItem.data, function (o) {
                return o.jenis == 'Rincian';
            });

            _.each(tindakan, function (o) {
                // $scope.data.tindakan.push(o);
                task.push($http.get(base_url + '/api/master/tindakan/tarif_by_pelayanan', {
                    params: {
                        id: o.id,
                        kelas: 9
                    }
                }));
            });

            $q.all(task).then(function (results) {
                _.each(results, function (o) {
                    $scope.dataTindakan.push(o.data);
                    $scope.dataRujukan.push(o.data);
                });
            })
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }


    function addDiagnostikFungsional(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddDiagnostikFungsional.html',
            controller: 'ModalAddDiagnostikFungsionalController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_diagnostik_fungsional = 1;
            $scope.data.diagnostikfungsional_uid = selectedItem.uid;

            var task = [];
            var tindakan = _.remove(selectedItem.data, function (o) {
                return o.jenis == 'Rincian';
            });

            _.each(tindakan, function (o) {
                // $scope.data.tindakan.push(o);
                task.push($http.get(base_url + '/api/master/tindakan/tarif_by_pelayanan', {
                    params: {
                        id: o.id,
                        kelas: 9
                    }
                }));
            });

            $q.all(task).then(function (results) {
                _.each(results, function (o) {
                    $scope.dataTindakan.push(o.data);
                    $scope.dataRujukan.push(o.data);
                });
            })
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addOdsOdc(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddOdsOdc.html',
            controller: 'ModalAddOdsOdcController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_ods_odc = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }


    function addRawatInap(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddRawatInap.html',
            controller: 'ModalAddRawatInapController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            // $scope.dataObat.push(selectedItem);
            console.log("data rawat inap, ", selectedItem);
            $scope.data.rawatInap = selectedItem.result;
            $scope.data.rujukan.rujuk_rawatinap = selectedItem.rujuk_rawatinap;

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function reservasiOk(isReservasi) {
        if (isReservasi) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalReservasiOK.html',
                controller: 'ModalReservasiOKController',
                size: 'lg',
                resolve: {
                    items: function () {
                        return $scope.data;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                // $scope.selected = selectedItem;
                // $scope.dataObat.push(selectedItem);
                console.log("data  OK, ", selectedItem);
                $scope.data.reservasiOk = selectedItem.result;
                $scope.data.rujukan.rujuk_ok = selectedItem.rujuk_ok;

            }, function () {
                $scope.data.getReservasiOK = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

    }
    function kunjunganUlang(isReservasi) {
        console.log("masuk knjungan ulang ");
        if (isReservasi) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalKunjunganUlang.html',
                controller: 'ModalKunjunganUlangController',
                size: 'lg',
                resolve: {
                    items: function () {
                        return $scope.data;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                console.log("data  kunjungan ulang, ", selectedItem);
                $scope.data.kunjunganUlang = selectedItem.result.data.result;
                $scope.data.rujukan.kunjungan_ulang = selectedItem.kunjungan_ulang;

            }, function () {
                $scope.data.getKunjunganUlang = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

    }

    function save() {

        $scope.data.tindakan = angular.copy($scope.dataTindakan);
        $scope.data.bphp = angular.copy($scope.dataBphp);
        $scope.data.obat = angular.copy($scope.dataObat);
        $scope.data.racikan = angular.copy($scope.dataRacikanInduk);
        $scope.data.rujukan.apotek = 0;

        // marshall tindakan
        $scope.data.tindakan = _.map($scope.data.tindakan, function (val) {
            delete val.bed;
            delete val.bed_id;
            delete val.unit_usaha_id;
            delete val.jenis_kelompok_pasien;
            delete val.kelompok_pasien_id;
            delete val.kelas_id;
            delete val.jenis_operasi;
            delete val.jenis_dokter;
            delete val.tarif;
            delete val.jenis;
            delete val.biaya;
            delete val.nama;
            delete val.kelas;
            delete val.kode;
            delete val.ruang;

            if (!val.pasien_id) val.pasien_id = $scope.data.pasien_id;

            val.layanan_id = $scope.data.layanan_id;
            val.pelayanan_id = $scope.data.pelayanan_id;
            val.no_register = $scope.data.no_register;
            val.quantity = 1; // default untuk tindakan
            val.dokter_id = $scope.data.dokter_id;
            val.jenis_tindakan = 1;
            val.tanggal = moment().format('YYYY-MM-DD hh:mm:ss');

            return val;
        });

        $scope.data.obat = _.map($scope.data.obat, function (val) {
            return val;
        });

        $scope.data.racikan = _.map($scope.data.racikan, function (val) {
            val.is_racikan = true;
            val.detail = JSON.stringify(val.tarif);
            val.nama = val.racikan;
            val.total = val.biaya * val.qty;
            return val;
        });

        console.log("post data, ", $scope.data);

        $.ajax({
            url: base_url + '/api/rawat_jalan/pemeriksaan_dokter/update',
            method: 'post',
            data: {
                data: JSON.stringify(angular.copy($scope.data))
            },
        }).then(function (result) {
            console.log("result save, ", result);
            $scope.isSubmit = true;
            toastr.success('Update Pemeriksaan Dokter Rawat Jalan Berhasil !', 'Sukses');
        }, function (err) {
            console.log("err", err);
            toastr.error('Terdapat kesalahan input data', 'Gagal');
        });

    }

    function selected(item) {

        $http.get(base_url + '/api/rawat_jalan/pemeriksaan_dokter/search_by_uid/' + item)
            .then(function (result) {
                var obj = angular.copy(result.data);
                console.log(obj);
                obj.rujukan = JSON.parse(obj.rujukan);
                console.log("data siap edit");
                console.log(obj);

                // Riwayat Pasien iFrame
                $("#riwayat_pasien-iframe").prop('src', base_url + '/rekam_medis/riwayat_pasien/form/' + obj.pasien_uid + '?iframe=1');
                
                $scope.isSelectedPasien = true;
                initData({
                    pasien: {
                        id: obj.pelayanan_id
                    }
                }, function () {
                    $scope.data = obj;
                    console.log("inininini");
                    console.log(obj.rujukan);
                    $scope.data.tanggal = new Date($scope.data.tanggal);
                    $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');

                    $scope.data.rujukan = {
                        rujuk_ok: (obj.rujukan.rujuk_ok == null || typeof obj.rujukan.rujuk_ok =='undefined') ? 0: obj.rujukan.rujuk_ok,
                        kunjungan_ulang:(obj.rujukan.kunjungan_ulang == null || typeof obj.rujukan.kunjungan_ulang =='undefined') ? 0: obj.rujukan.kunjungan_ulang,
                        rujuk_rawatinap:(obj.rujukan.rujuk_rawatinap == null || typeof obj.rujukan.rujuk_rawatinap =='undefined') ? 0: obj.rujukan.rujuk_rawatinap,
                        rujuk_audiometri:(obj.rujukan.rujuk_audiometri == null || typeof obj.rujukan.rujuk_audiometri =='undefined') ? 0: obj.rujukan.rujuk_audiometri,
                        rujuk_cathlab: (obj.rujukan.rujuk_cathlab == null || typeof obj.rujukan.rujuk_cathlab =='undefined') ? 0: obj.rujukan.rujuk_cathlab,
                        rujuk_ctscan: (obj.rujukan.rujuk_ctscan == null || typeof obj.rujukan.rujuk_ctscan =='undefined') ? 0: obj.rujukan.rujuk_ctscan,
                        rujuk_diagnostik_fungsional:(obj.rujukan.rujuk_diagnostik_fungsional == null || typeof obj.rujukan.rujuk_diagnostik_fungsional =='undefined') ? 0: obj.rujukan.rujuk_diagnostik_fungsional,
                        rujuk_fisioterapi: (obj.rujukan.rujuk_fisioterapi == null || typeof obj.rujukan.rujuk_fisioterapi =='undefined') ? 0: obj.rujukan.rujuk_fisioterapi,
                        rujuk_laboratorium:(obj.rujukan.rujuk_laboratorium == null || typeof obj.rujukan.rujuk_laboratorium =='undefined') ? 0: obj.rujukan.rujuk_laboratorium,
                        rujuk_ods_odc:(obj.rujukan.rujuk_ods_odc == null || typeof obj.rujukan.rujuk_ods_odc =='undefined') ? 0: obj.rujukan.rujuk_ods_odc,
                        rujuk_radiologi: (obj.rujukan.rujuk_radiologi == null || typeof obj.rujukan.rujuk_radiologi =='undefined') ? 0: obj.rujukan.rujuk_radiologi
                    };
                   // $scope.data.rujukan =obj.rujukan;

                    if (obj.kelompokpasien == 'BPJS') {
                        obj.inacbgs = JSON.parse(obj.inacbgs);
                        if( typeof obj.inacbgs !== 'undefined' && obj.inacbgs !== "" && obj.inacbgs !== null
                            && obj.inacbgs !== "0" && obj.inacbgs !== 0) {
                            $scope.data.diagnosa = obj.inacbgs.bpjs.diagAwal.kdDiag + ' ' + obj.inacbgs.bpjs.diagAwal.nmDiag;
                        }
                        //$scope.data.diagnosa = obj.inacbgs.bpjs.diagAwal.kdDiag + ' ' + obj.inacbgs.bpjs.diagAwal.nmDiag;
                    }

                    // marshall bmhp paket
                    _.each(obj.bmhp, function(bmhp){
                        bmhp.obat = bmhp.nama;
                        bmhp.qty = parseInt(bmhp.quantity);
                        bmhp.harga = bmhp.tarif;
                        bmhp.biaya = bmhp.tarif;
                        bmhp.stock = parseInt(bmhp.quantity);
                        bmhp.paket = true;
                        $scope.dataBphp.push(bmhp);
                    });
                    
                    //load obat
                    console.log("data hasil");
                    console.log($scope.data);
                    console.log("rujukan");
                    console.log(obj.data_rujukan.ok[0]);
                    console.log(obj.data_rujukan.ok.length);
                    
                    _.each(obj.obat_racikan_detail, function(val){
                        // var _tarif = {
                        //     racikan: val.nama,
                        //     tarif: [],
                        //     biaya: 0,

                        // };
                        // var detail_obat = JSON.parse(val.detail);
                        // _tarif.totalObatRacikan = detail_obat.length;
                        // _tarif.nama = val.nama;
                        // _tarif.isEdit = true;
                        // _tarif.idx = 0;
                        // _tarif.biaya = val.harga;
                        // _tarif.catatan = val.catatan;
                        // _tarif.racikan = val.nama;
                        // _tarif.tarif = val ;
                        // _tarif.signa = val.signa;
                        // _tarif.signa_label = val.signa_label;
                        // $scope.dataRacikanInduk.push(_tarif);

                        var detail_obat = JSON.parse(val.detail);
                        var detail_signa = JSON.parse(val.signa);

                        var _tarif = {
                            biaya: val.harga,
                            catatan: val.catatan,
                            idx: 0,
                            isEdit: true,
                            nama: val.nama,
                            qty: parseFloat(val.qty),
                            racikan: val.nama,
                            signa: detail_signa,
                            signa_label: val.signa_label,
                            tarif: detail_obat,
                            total: val.harga,
                            totalObatRacikan: detail_obat.length
                        };
                        $scope.dataRacikanInduk.push(_tarif);

                    });

                    _.each(obj.obat_non_racikan_detail, function(val){
                        var data= {};
                        data.id = val.id;
                        data.biaya = val.harga;
                        data.obat_id = val.obat_id;
                        data.obat = val.nama;
                        data.old = val.old;
                        data.qty = parseInt(val.qty);
                        data.total = val.harga * val.qty;
                        data.signa = JSON.parse(val.signa);
                        data.signa_label = val.signa_label;
                        data.catatan = val.catatan;
                        data.detail = val;
                        $scope.dataObat.push(data);



                    });

                    //load rujukan OK
                    $scope.data.reservasiOk = obj.data_rujukan.ok[0] ;
                    //load kunjungan ulang
                    $scope.data.kunjunganUlang = obj.data_rujukan.kunjungan_ulang;


                    // marshall diagnosa
                    $http.get(base_url + "/api/rawat_jalan/pemeriksaan_dokter/icd10_ids", {
                        params: {
                            ids: obj.diagnosa
                        }
                    }).then(function(result){
                        console.log("result diagnosa", result);
                        $scope.data.diagnosa_text = result.data.diagnosa;
                    })

                });


            }).catch(function (err) {
            console.log(err);
        });


    }

    function deleteTindakan(idx) {
        $scope.dataTindakan.splice(idx, 1);
    }

    function deleteRacikan(idx) {
        $scope.dataRacikanInduk.splice(idx, 1);
    }

    function deleteBphp(idx) {
        $scope.dataBphp.splice(idx, 1);
    }

    function deleteObat(idx) {
        $scope.dataObat.splice(idx, 1);
    }

    function deleteFarmasi(idx) {
        $scope.dataFarmasi.splice(idx, 1);
    }

    function deleteLaboratorium(idx) {
        $scope.dataLaboratorium.splice(idx, 1);
    }

    function racikanQtyChange(idx, qty) {
        if (!qty || qty <= 0) {
            $scope.dataRacikanInduk[idx].qty = "";
        } else {
            $scope.dataRacikanInduk[idx].qty = qty;
            $scope.dataRacikanInduk[idx].total = $scope.dataRacikanInduk[idx].biaya * qty;

            console.log($scope.dataRacikanInduk[idx]);

            $scope.tarifTotalRacikanInduk = 0;
            $scope.tarifTotalRacikanInduk += parseFloat(_.sum(_.map($scope.dataRacikanInduk, function (data) {
                return parseFloat(data.total);
            })));
        }

    }

    function racikanDiskonChange(idx, diskon) {
        if (diskon) {
            var diskon = diskon / 100;
            var diskonTotal = ($scope.dataRacikanInduk[idx].biaya) * diskon;
            var total = ($scope.dataRacikanInduk[idx].biaya) - diskonTotal;
            $scope.dataRacikanInduk[idx].total = total;
            $scope.tarifTotalRacikanInduk = 0;
            $scope.tarifTotalRacikanInduk += parseFloat(_.sum(_.map($scope.dataRacikanInduk, function (data) {
                return parseFloat(data.total);
            })));
        }
    }


    function tindakanQtyChange(idx, qty) {
        if (qty <= 0) {
            $scope.dataTindakan[idx].qty = 1;
        } else {
            $scope.dataTindakan[idx].qty = qty;
            $scope.dataTindakan[idx].total = $scope.dataTindakan[idx].biaya * qty;
        }
    }

    function tindakanDiskonChange(idx, diskon) {
        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) * diskon;
        var total = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) - diskonTotal;
        $scope.dataTindakan[idx].total = total;
        // $scope.dataTindakan[idx].biaya = total;
        console.log("tindakan total", {
            diskon: diskon,
            diskonTotal: diskonTotal,
            total: total
        });
    }

    function bphpQtyChange(idx, qty) {
        var stock = $scope.dataBphp[idx].stock;
        if (!qty || qty <= 0 || qty > stock) {
            $scope.dataBphp[idx].qty = "";
        } else {
            $scope.dataBphp[idx].qty = qty;
            $scope.dataBphp[idx].total = $scope.dataBphp[idx].biaya * qty;
            // $scope.dataBphp[idx].stock -= qty;
        }
    }

    function bphpDiskonChange(idx, diskon) {
        if (diskon >= 0 && diskon <= 100) {
            var diskon = diskon / 100;
            var diskonTotal = ($scope.dataBphp[idx].biaya * $scope.dataBphp[idx].qty) * diskon;
            var total = ($scope.dataBphp[idx].biaya * $scope.dataBphp[idx].qty) - diskonTotal;
            console.log({
                diskon: diskon,
                diskonTotal: diskonTotal,
                total: total
            });
            $scope.dataBphp[idx].harga = total;

        } else {
            $scope.dataBphp[idx].diskon = 0;
        }
    }

    function obatQtyChange(idx, qty) {

        if (!qty || qty <= 0) {
            $scope.dataObat[idx].qty = "";
        } else {
            $scope.dataObat[idx].qty = qty;
            $scope.dataObat[idx].total = $scope.dataObat[idx].biaya * qty;
        }

    }

    function obatDiskonChange(idx, diskon) {
        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataObat[idx].biaya * $scope.dataObat[idx].qty) * diskon;
        var total = ($scope.dataObat[idx].biaya * $scope.dataObat[idx].qty) - diskonTotal;
        $scope.dataObat[idx].total = total;
        console.log("data obat");
        console.log($scope.dataObat[idx]);
    }

    function clear() {
        $scope.data = {};
    }

    /**
     * Build Table
     */
    $scope.rows = [];
    function buildTable() {
        var url = base_url + '/api/rawat_jalan/pemeriksaan_awal/list_rawat_jalan/1';

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                url: url,
                type: 'POST',
                data: function (data, dtInstance) {

                    // data.columns[data.length - 1].name = "m_pegawai.nama";
                    // Modify the data object properties here before being passed to the server
                }
            })
            // or here
            .withDataProp('data')
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('order', [[0, 'desc']])
            .withOption('createdRow', function (row) {
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);
            })
            .withPaginationType('full_numbers');

        $scope.dtInstance = {};
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('tanggal_update').withTitle('Tanggal').renderWith(function (data, type, row, meta) {
                console.log(row);
                return moment(row.tanggal_update).format('DD-MMM-YYYY HH:mm:s');
            }).withOption('searchable', false),
            DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No RM'),
            DTColumnBuilder.newColumn('no_register').withTitle('No Register').renderWith(function (data, type, row, meta) {
                $scope.rows.push(row);
                return '<a  ng-click="openDetailPasien(' + meta.row + ')">' + data + '</a>';
            }),
            DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
            DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
            DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter'),
            DTColumnBuilder.newColumn('id').withTitle('').notSortable().withOption('searchable', false).renderWith(function (data, type, row, meta) {
                $button = "<a href='" + base_url + '/rawat_jalan/pemeriksaan_dokter/rujukan_lanjutan/' + row.pelayanan_id + "' class='btn btn-primary btn-sm'><i class='fa fa-chevron-right'></i></a>";
                return $button;
            }),
            DTColumnBuilder.newColumn('id').withTitle('').notSortable().withOption('searchable', false).renderWith(function (data, type, row, meta) {
                $button = "<a href='" + base_url + '/rawat_jalan/pemeriksaan_dokter/edit/' + row.pelayanan_id + "' class='btn btn-primary btn-sm'><i class='fa fa-chevron-right'></i></a>";
                return $button;
            })

        ];

        $scope.dtInstance = {};
    }

    $scope.openDetailPasien = function (idx) {

        $scope.modalData = $scope.rows[idx];


        $http.get(base_url + "/api/rawat_jalan/pemeriksaan_dokter/icd10_ids", {
            params: {
                ids: $scope.rows[idx].diagnosa
            }
        }).then(function (result) {
            $scope.rows[idx].diagnosa_text = result.data.diagnosa;
            $scope.modalData.fromNow = moment($scope.modalData.created_at).fromNow();
            $('#modal_detail').modal();
        }, function (err) {
            console.log(err);
        });


    };
    /**
     * Dismiss Modal
     */
    function dismiss() {
        $log.info('Modal dismissed at: ' + new Date());
    }


    // jquery
    $timeout(function () {


        $('#diagnosa').select2({

            placeholder: 'PILIH ICD',
            ajax: {
                url: base_url + '/api/rawat_jalan/pemeriksaan_dokter/icd10',
                dataType: 'json',
                method: 'post',
                data: function (param) {
                    return {
                        delay: 0.3,
                        q: param.term
                    }
                },
                processResults: function (data) {

                    return {
                        results: _.map(data.items, function (obj) {
                            return {
                                id: obj.id,
                                text: obj.code + ' - ' + obj.description,
                            }
                        })
                    }
                },
                cache: true,
            }
        })
    }, 1000);

}








