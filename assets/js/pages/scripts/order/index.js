var Index = function () {
	
	var oDaftarTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var totdata = '';
	var value = '';
	$('#table').on('search.dt', function() {
	    value = $('.dataTables_filter input').val();
	    loadheader();
	});

	function loadheader(){
	    $.ajax({
	        headers:{
	            "Authorization":token,
	            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
	        },
	        data: {'by_order_code':value},
            url: url_load_data,
	        success: function(data,json,xhr) {
	            totdata = xhr.getResponseHeader('Total-Count');
	        }
	    });
	}

	var handleDaftarTable = function() {
		loadheader();
		oDaftarTable = $('#table').DataTable({
			'paging'        : true,
	        'lengthChange'  : false,
	        'pageLength'    : 20,
	        'searching'     : true,
	        'ordering'      : false,
	        'info'          : true,
	        'autoWidth'     : true,
	        'processing'    : false, //Feature control the processing indicator.
	        'serverSide'    : true,
            "ajax": {
            	'headers': {'Authorization': token,
                    'Accept':"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"},
                "url": url_load_data,
                "type": "GET",
                'data': function ( d ) {
	                d.page = $('#table').DataTable().page.info()['page'] + 1;
	                d.by_order_code = d.search.value;
	            },
	            'dataFilter': function(data){
	                var json = jQuery.parseJSON( data );
	                json.recordsTotal = totdata;
	                json.recordsFiltered = totdata;
	                json.data = json.data;
	                return JSON.stringify( json ); // return JSON string
	            },
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "order_code"},
				{"data": "payment_status"},
				{"data": "status"},
				{"data": "amount"},
				{
					"data": "id",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						var btnList = '<a class="edit-row text-white btn btn-primary" data-id="' + row.id + '" data-id="' + row.id + '" title="Detail"><i class="fa fa-eye"></i></a>';

							btnList += ' <a class="changemethod-row text-white btn btn-info" data-id="' + row.id + '" data-id="' + row.id + '" title="Change Method"><i class="fa fa-check-square"></i></a>';
							btnList += ' <a class="changestatus-row text-white btn btn-warning" data-id="' + row.id + '" data-id="' + row.id + '" title="Change Payment Status"><i class="fa fa-repeat"></i></a>';
						if (row.status != 'close') {
							btnList += ' <a class="closeorder-row text-white btn btn-danger" data-id="' + row.id + '" data-id="' + row.id + '" title="Close Order"><i class="fa fa-close"></i></a>';
						}

						return btnList;
					},
					"className": "text-center"
				}
			]
		});
			
    };
	
	var reloadDaftarTable = function() {
        if (oDaftarTable == null) {
			handleDaftarTable();
        }
		else {
			oDaftarTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarTable();
			
			$("#table").on("click", ".edit-row", function () {
				var id = $(this).data('id');
				window.location = url_edit + '?id=' + id;
			});
			
			$("#table").on("click", ".changestatus-row", function () {
				var id = $(this).data('id');
				$('.modal-title').html('Change Payment Status');
				$('#type').val('status');
				$('#id').val(id);

				$('.modal-body').empty();
				$('.modal-body').append('<select name="status" id="status" class="form-control">"'
							        		+'<option value=""> --- Pilih Status --- </option>'
							        		+'<option value="pending">Pending</option>'
							        		+'<option value="settlement">settlement</option>'
							        		+'<option value="expire">expire</option>'
							        	+'</select>')
				$('select').select2();
				$('#myModal').modal('show'); 
			});

			$("#table").on("click", ".changemethod-row", function () {
				var id = $(this).data('id');
				$('.modal-title').html('Change Method');
				$('#type').val('method');
				$('#id').val(id);

				$('.modal-body').empty();
				$('.modal-body').append('<select name="methode" id="methode" class="form-control">'
											+'<option value=""> --- Pilih Metode --- </option>'
											+'<option value="bank_transfer">Bank transfer</option>'
											+'<option value="echannel">E-Channel</option>'
										+'</select>')

				$('.modal-body').append('<br><br><select name="provider" id="provider" class="form-control">'
											+'<option value=""> --- Pilih Provider --- </option>'
										+'</select>')
				$('select').select2();

				$('#methode').on('change', function() {
				  var val = this.value;
				  if (val == 'bank_transfer') {
				  	$('#provider').empty();
				  	$('#provider').append('<option value=""> --- Pilih Provider --- </option><option value="bni">bank BNI</option><option value="bca">bank BCA</option>');
				  }else if (val == 'echannel') {
				  	$('#provider').empty();
				  	$('#provider').append('<option value=""> --- Pilih Provider --- </option><option value="mandiri">bank Mandiri</option>');
				  }else{
				  	$('#provider').empty();
				  }
				});

				$('#myModal').modal('show'); 
			});

			$("#myModal").on("click", "#btnProses", function () {
				let id = $('#id').val(),
					status = $('#status').val(),
					methode = $('#methode').val(),
					provider = $('#provider').val();
					type = $('#type').val();
					url = "";
					data = "";

					if (type == 'status') {
						if (status == "") {
							swal({
								title: "Pilih Status Terlebih dahulu!",
								confirmButtonColor: "red"
							});
							return false;
						}
					  	data = {
							  	"id": id,
							  	"payment_status": status
							}
						url += url_load_status; 
					}else{
						if (methode == "" || provider == "" ) {
							swal({
								title: "Pilih Terlebih dahulu!",
								confirmButtonColor: "red"
							});
							return false;
						}
					  	data = {
							  "id": id,
							  "payment_method": methode,
							  "payment_provider": provider
							}
						url += url_load_method;
					}
				swal({
					title: "Anda yakin untuk Proses Data ini!",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Note ..."
				},
				function(note){
				  if (note === false) return false;
					$.ajax({
				        headers:{
				            "Authorization":token,
				            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				        },
						data: data,
						type: "POST",
						dataType: 'JSON', 
						url: url,
						success: function(data){
							reloadDaftarTable();
							if (data.error == true) {
								$.each( data.messages, function( key, value ) {
									errorMessage('Peringatan', value);
								});
								return false;
							}
							$('#myModal').modal('hide'); 

							swal({
								title: "Data telah diperoses!",
								confirmButtonColor: "#2196F3"
							});
						},
						error: function(data){
							swal({
								title: "Data gagal diperoses!",
								confirmButtonColor: "#2196F3"
							});
						}
					});

				});
			});

			$("#table").on("click", ".closeorder-row", function () {
				$('#myModal').show();

				var id = $(this).data('id');
				var hapus = false;
				swal({
					title: "Anda yakin untuk Close Order Data ini!",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Note ..."
				},
				function(note){
				  if (note === false) return false;
				  	data = {
				  		'id' : id,
				  		'note' :note
				  	}
					$.ajax({
				        headers:{
				            "Authorization":token,
				            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				        },
						data: data,
						type: "POST",
						dataType: 'JSON', 
						url: url_load_closeorder,
						success: function(data){
							reloadDaftarTable();
							swal({
								title: "Data telah Close Order!",
								confirmButtonColor: "#2196F3"
							});
						},
						error: function(data){
							swal({
								title: "Data gagal Close Order!",
								confirmButtonColor: "#2196F3"
							});
						}
					});

				});

				// function(isConfirm){
				// 	if (isConfirm) {
				// 		

				// 	}
				// });
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTable();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();