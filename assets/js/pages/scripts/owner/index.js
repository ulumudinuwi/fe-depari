var Index = function () {
	
	var oDaftarTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var totdata = '';
	var value = '';
	$('#table').on('search.dt', function() {
	    value = $('.dataTables_filter input').val();
	    loadheader();
	});

	function loadheader(){
	    $.ajax({
	        headers:{
	            "Authorization":token,
	            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
	        },
	        data: {'by_email':value},
            url: url_load_data,
	        success: function(data,json,xhr) {
	            totdata = xhr.getResponseHeader('Total-Count');
	        }
	    });
	}

	var handleDaftarTable = function() {
		loadheader();
		oDaftarTable = $('#table').DataTable({
			'paging'        : true,
	        'lengthChange'  : false,
	        'pageLength'    : 20,
	        'searching'     : true,
	        'ordering'      : false,
	        'info'          : true,
	        'autoWidth'     : true,
	        'processing'    : false, //Feature control the processing indicator.
	        'serverSide'    : true,
            "ajax": {
            	'headers': {'Authorization': token,
                    'Accept':"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"},
                "url": url_load_data,
                "type": "GET",
                'data': function ( d ) {
	                d.page = $('#table').DataTable().page.info()['page'] + 1;
	                d.by_email = d.search.value;
	            },
	            'dataFilter': function(data){
	                var json = jQuery.parseJSON( data );
	                json.recordsTotal = totdata;
	                json.recordsFiltered = totdata;
	                json.data = json.data;
	                return JSON.stringify( json ); // return JSON string
	            },
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "name"},
				{"data": "email"},
				{"data": "is_verified"},
				{
					"data": "id",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						var btnList = '<a class="edit-row text-white btn btn-primary" data-id="' + row.id + '" data-id="' + row.id + '" title="Detail"><i class="fa fa-list-alt"></i></a>';

							if (row.is_verified == false) {
								btnList += ' <a class="active-row text-white btn btn-info" data-id="' + row.id + '" data-id="' + row.id + '" title="Verifikasi"><i class="fa fa-check"></i></a>';
							}else{
								btnList += ' <a class="deactive-row text-white btn btn-danger" data-id="' + row.id + '" data-id="' + row.id + '" title="Deactive"><i class="fa fa-close"></i></a>';
							}
						return btnList;
					},
					"className": "text-center"
				}
			]
		});
			
    };
	
	var reloadDaftarTable = function() {
        if (oDaftarTable == null) {
			handleDaftarTable();
        }
		else {
			oDaftarTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarTable();
			
			$("#table").on("click", ".edit-row", function () {
				var id = $(this).data('id');
				window.location = url_edit + '?id=' + id;
			});
			
			$("#table").on("click", ".deactive-row", function () {
				var id = $(this).data('id');
				var hapus = false;
				swal({
					title: "Anda yakin untuk Deactive User tersebut!",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Note ..."
				},
				function(note){
				  if (note === false) return false;
				  	data = {
				  		'id' : id,
				  		'note' :note
				  	}
					$.ajax({
				        headers:{
				            "Authorization":token,
				            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				        },
						data: data,
						type: "POST",
						dataType: 'JSON', 
						url: url_load_deactive,
						success: function(data){
							reloadDaftarTable();
							swal({
								title: "Data telah Deactive!",
								confirmButtonColor: "#2196F3"
							});
						},
						error: function(data){
							swal({
								title: "Data gagal Deactive!",
								confirmButtonColor: "#2196F3"
							});
						}
					});

				});

				// function(isConfirm){
				// 	if (isConfirm) {
				// 		

				// 	}
				// });
			});

			$("#table").on("click", ".active-row", function () {
				var id = $(this).data('id');
				var hapus = false;
				swal({
					title: "Anda yakin untuk Memverifikasi User tersebut!",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Note ..."
				},
				function(note){
				  if (note === false) return false;
				  	data = {
				  		'id' : id,
				  		'note' :note
				  	}
					$.ajax({
				        headers:{
				            "Authorization":token,
				            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				        },
						data: data,
						type: "POST",
						dataType: 'JSON', 
						url: url_load_active,
						success: function(data){
							reloadDaftarTable();
							swal({
								title: "Data telah Memverifikasi!",
								confirmButtonColor: "#2196F3"
							});
						},
						error: function(data){
							swal({
								title: "Data gagal Memverifikasi!",
								confirmButtonColor: "#2196F3"
							});
						}
					});

				});

				// function(isConfirm){
				// 	if (isConfirm) {
				// 		

				// 	}
				// });
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTable();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();