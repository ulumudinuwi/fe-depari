var Index = function () {
	
	var oDaftarProduk = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var totdata = '';
	var value = '';
	$('#produk_table').on('search.dt', function() {
	    value = $('.dataTables_filter input').val();
	    loadheader();
	});

	function loadheader(){
	    $.ajax({
	        headers:{
	            "Authorization":token,
	            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
	        },
	        data: {'by_name':value},
            url: url_load_data,
	        success: function(data,json,xhr) {
	            totdata = xhr.getResponseHeader('Total-Count');
	        }
	    });
	}

	var handleDaftarPekerjaanTable = function() {
		loadheader();
		oDaftarProduk = $('#produk_table').DataTable({
			'paging'        : true,
	        'lengthChange'  : false,
	        'pageLength'    : 20,
	        'searching'     : true,
	        'ordering'      : false,
	        'info'          : true,
	        'autoWidth'     : true,
	        'processing'    : false, //Feature control the processing indicator.
	        'serverSide'    : true,
            "ajax": {
            	'headers': {'Authorization': token,
                    'Accept':"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"},
                "url": url_load_data,
                "type": "GET",
                'data': function ( d ) {
	                d.page = $('#produk_table').DataTable().page.info()['page'] + 1;
	                d.by_name = d.search.value;
	            },
	            'dataFilter': function(data){
	                var json = jQuery.parseJSON( data );
	                json.recordsTotal = totdata;
	                json.recordsFiltered = totdata;
	                json.data = json.data;
	                return JSON.stringify( json ); // return JSON string
	            },
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "name"},
				{
					"data": "category",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						return row.category.name;
					},
					"className": "text-center"
				},
				{
					"data": "price",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						return numeral(data).format('0.0,');
					},
					"className": "text-center"
				},
				{
					"data": "stock",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						return data + " "+ row.unit;
					},
					"className": "text-center"
				},
				{
					"data": "id",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						var btnList = '<a class="edit-row text-white btn btn-primary" data-id="' + row.id + '" data-id="' + row.id + '" title="Detail"><i class="fa fa-list-alt"></i></a>';

						if (row.is_blocked == true) {
							btnList += ' <a class="unblock-row text-white btn btn-info" data-id="' + row.id + '" data-id="' + row.id + '" title="UnBlock"><i class="fa fa-check-circle"></i></a>';
						}else{
							btnList += ' <a class="block-row text-white btn btn-warning" data-id="' + row.id + '" data-id="' + row.id + '" title="Block"><i class="fa fa-close"></i></a>';
						}
						if (role == 'Operator') {
							btnList += ' <a class="delete-row btn btn-danger" data-id="' + row.id + '" data-id="' + row.id + '" title="Hapus"><i class="fa fa-trash"></i></a>';
						}
						return btnList;
					},
					"className": "text-center"
				}
			]
		});
			
    };
	
	var reloadDaftarProduk = function() {
        if (oDaftarProduk == null) {
			handleDaftarPekerjaanTable();
        }
		else {
			oDaftarProduk.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarProduk();
			
			$("#produk_table").on("click", ".edit-row", function () {
				var id = $(this).data('id');
				window.location = url_edit + '?id=' + id;
			});
			
			$("#produk_table").on("click", ".delete-row", function () {
				var id = $(this).data('id');
				var hapus = false;
				swal({
					title: "Anda yakin untuk menghapus data tersebut?",
					type: "warning",
					showCancelButton: true,
					closeOnConfirm: false,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "OK",
					cancelButtonText: "Batal",
					showLoaderOnConfirm: true
				},
				function(isConfirm){
					if (isConfirm) {
						$.ajax({
					        headers:{
					            "Authorization":token,
					            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
					        },
							type: 'DELETE',
							dataType: 'JSON', 
							url: url_load_delete+'/'+id,
							success: function(data){
								reloadDaftarProduk();
								swal({
									title: "Data telah dihapus!",
									confirmButtonColor: "#2196F3"
								});
							},
							error: function(data){
								swal({
									title: "Data gagal dihapus!",
									confirmButtonColor: "#2196F3"
								});
							}
						});
					}
				});
			});

			$("#produk_table").on("click", ".block-row", function () {
				var id = $(this).data('id');
				var hapus = false;
				swal({
					title: "Anda yakin untuk mengeblok data tersebut!",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Note ..."
				},
				function(note){
				  if (note === false) return false;
				  	data = {
				  		'id' : id,
				  		'note' : note
				  	}
					$.ajax({
				        headers:{
				            "Authorization":token,
				            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				        },
						data: data,
						type: "POST",
						dataType: 'JSON', 
						url: url_load_blok,
						success: function(data){
							reloadDaftarProduk();
							swal({
								title: "Data telah Blok!",
								confirmButtonColor: "#2196F3"
							});
						},
						error: function(data){
							swal({
								title: "Data gagal Blok!",
								confirmButtonColor: "#2196F3"
							});
						}
					});

				});

				// function(isConfirm){
				// 	if (isConfirm) {
				// 		

				// 	}
				// });
			});

			$("#produk_table").on("click", ".unblock-row", function () {
				var id = $(this).data('id');
				var hapus = false;
				swal({
					title: "Anda yakin untuk membuka data tersebut!",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Note ..."
				},
				function(note){
				  if (note === false) return false;
				  	data = {
				  		'id' : id
				  	}
					$.ajax({
				        headers:{
				            "Authorization":token,
				            "Accept":"text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				        },
						data: data,
						type: "POST",
						dataType: 'JSON', 
						url: url_load_unblok,
						success: function(data){
							reloadDaftarProduk();
							swal({
								title: "Data telah di Buka!",
								confirmButtonColor: "#2196F3"
							});
						},
						error: function(data){
							swal({
								title: "Data gagal di Buka!",
								confirmButtonColor: "#2196F3"
							});
						}
					});

				});

				// function(isConfirm){
				// 	if (isConfirm) {
				// 		

				// 	}
				// });
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarProduk();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();